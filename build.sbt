name := """spark-utils"""

version := "1.0-SNAPSHOT"

fork in Test := true

parallelExecution in Test := false

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
