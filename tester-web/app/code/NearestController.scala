package code

import com.google.inject.Inject
import com.typesafe.scalalogging.StrictLogging
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.Messages
import play.api.mvc.{Action, Controller}
import ws.eiennohito.{MarkedMorpheme, MaskedSentence, SentenceSummarizerClient, SentenceVectorizer}
import ws.kotonoha.akane.analyzers.knp.raw.OldAngUglyKnpTable
import ws.kotonoha.akane.utils.StringUtil
import ws.kotonoha.akane.vectorization.{DictionaryRestorator, DictionaryVectorizer}

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.{ExecutionContext, Future}

/**
  * @author eiennohito
  * @since 2017/01/31
  */

case class AnalyzeRequest(data: String)

case class AnalyzeRequestData(
  content: String,
  ids: Seq[(Long, String)]
)

class NearestController @Inject() (
  knp: KnpAnalyzer,
  impl: SentenceSummarizerClient,
  vec: DictionaryVectorizer,
  rest: DictionaryRestorator,
  repr: VectorRepr
)(implicit ec: ExecutionContext, msg: Messages) extends Controller with StrictLogging {
  val requestForm = Form(
    mapping(
      "query" -> text
    )(AnalyzeRequest.apply)(AnalyzeRequest.unapply)
  )

  def markSentence(res: OldAngUglyKnpTable, idx: Int): MaskedSentence = {
    val idxBld = new ListBuffer[Int]
    val sentBld = new ArrayBuffer[MarkedMorpheme]()

    var charStart = 0
    val morphs = res.lexemeIter
    var midx = 0
    while (morphs.hasNext) {
      val mrph = morphs.next()
      val charEnd = charStart + mrph.surface.length

      if ((charStart <= idx) && (idx < charEnd)) {
        idxBld += midx
      }
      midx += 1

      charStart = charEnd

      sentBld += MarkedMorpheme(mrph, vec.vecOne(mrph))
    }

    MaskedSentence(sentBld, idxBld.result())
  }

  def forDisplay(sent: MaskedSentence): Seq[AnalyzeRequestData] = {
    sent.sentence.map { mrp =>
      val objs = mrp.ids.map { id => (id, rest.restoreOne(id)) }
      AnalyzeRequestData(mrp.data.canonicForm(), objs)
    }
  }

  def index() = Action.async { implicit req =>
    requestForm.bindFromRequest().fold(
      errors => Future.successful(Ok(views.html.nearest(errors, Nil, NearWords(Nil, Nil)))),
      norm => {
        val text = norm.data
        val idx = StringUtil.indexOfAny(text, "$＄")
        val clean = if (idx == -1) text else text.substring(0, idx) + text.substring(idx + 1)
        val res = knp.analyze(clean)
        val marked = markSentence(res, idx)
        logger.debug(s"idx=$idx, ${marked.mask}")
        val reply = impl.summarize(Seq(marked))
        val display = forDisplay(marked)
        reply.map { mat =>
          logger.debug(s"Reply: \n$mat")
          val vec = mat.toDenseVector
          val nearest = repr.nearestNWords(vec, 50)
          Ok(views.html.nearest(requestForm.fill(norm), display, nearest))
        }
      }
    )
  }
}
