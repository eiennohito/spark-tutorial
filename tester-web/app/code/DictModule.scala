package code

import akka.actor.ActorSystem
import breeze.linalg.{DenseMatrix, DenseVector}
import com.google.inject
import com.google.inject.{Binder, Module, Provides}
import io.grpc.{ManagedChannel, ManagedChannelBuilder}
import org.apache.commons.lang3.StringUtils
import org.tensorflow.serving.prediction_service.PredictionServiceGrpc
import org.tensorflow.serving.prediction_service.PredictionServiceGrpc.PredictionService
import play.api.i18n.{Lang, Messages}
import play.api.{Application, Configuration}
import ws.eiennohito.math.VectorOps
import ws.eiennohito.{KnnUtil, SentenceSummarizerClient, TensorflowSnapshot}
import ws.kotonoha.akane.analyzers.knp.raw.OldAngUglyKnpTable
import ws.kotonoha.akane.pipe.knp.KnpTabPipeParser
import ws.kotonoha.akane.utils.XLong
import ws.kotonoha.akane.vectorization.{DictionaryRestorator, DictionaryVectorizer, KnpFeatures}

import scala.concurrent.ExecutionContextExecutor

class Dictionary(val words: IndexedSeq[String])

case class NearWord(id: Long, text: String, similarity: Float)
case class NearWords(normal: Seq[NearWord], cosine: Seq[NearWord])

class VectorRepr(data: DenseMatrix[Float], restorator: DictionaryRestorator) {
  private val l2Normed = KnnUtil.normOf(data)

  def nearestNWords(repr: DenseVector[Float], n: Int): NearWords = {
    val normal = KnnUtil.topN(data, repr, n)
    val normed = KnnUtil.normVec(repr)

    println(s"normed=${VectorOps.norm2(normed.data, normed.offset, normed.length)} ${normed.stride}")
    println(VectorOps.norm2(l2Normed.data, 0, l2Normed.rows))
    println(VectorOps.norm2(l2Normed.data, 100, l2Normed.rows))
    println(VectorOps.norm2(l2Normed.data, 200, l2Normed.rows))
    println(VectorOps.norm2(l2Normed.data, 300, l2Normed.rows))
    println(VectorOps.norm2(l2Normed.data, 900, l2Normed.rows))
    println(VectorOps.norm2(l2Normed.data, 9000, l2Normed.rows))
    println(VectorOps.norm2(l2Normed.data, 1200, l2Normed.rows))
    println(data.majorStride)
    println(l2Normed.majorStride)
    println(s"${l2Normed.rows}, ${l2Normed.cols}, ${l2Normed.offset}, ${l2Normed.majorStride}")
    println(s"${data.rows}, ${data.cols}, ${data.offset}, ${data.majorStride}")
    val veca = l2Normed(::, 2)
    println(s"${VectorOps.norm2(veca.data, veca.offset, veca.length)} ${veca.length} ${veca.offset} ${veca.stride}")

    val normalized = KnnUtil.topN(l2Normed, normed, n)

    val nobj = normal._1.iterator.zip(normal._2.valuesIterator)
        .map { case (idx, score) => NearWord(idx, restorator.restoreOne(idx), score) }

    val zobj = normalized._1.iterator.zip(normalized._2.valuesIterator)
      .map { case (idx, score) => NearWord(idx, restorator.restoreOne(idx), score) }

    NearWords(nobj.toVector.sortBy(-_.similarity), zobj.toVector.sortBy(-_.similarity))
  }
}

/**
  * @author eiennohito
  * @since 2017/01/31
  */
class DictModule extends Module {
  override def configure(binder: Binder) = {}

  import ws.kotonoha.akane.resources.FSPaths._

  @Provides
  @inject.Singleton
  def dictionary(
    conf: Configuration
  ): Dictionary = {
    val dictPath = conf.getString("vectorization.dictionary").get.p
    val maxVectorize = conf.getInt("vectorization.max-words").getOrElse(10000)

    val lines = dictPath.lines().take(maxVectorize).map { s =>
      StringUtils.split(s, '\t') match {
        case Array(XLong(cnt), w) => w
        case Array(w, XLong(cnt)) => w
      }
    }

    val words = lines.toArray

    new Dictionary(words)
  }

  @Provides
  @inject.Singleton
  def vectors(
    conf: Configuration,
    rest: DictionaryRestorator
  ): VectorRepr = {
    val baseFile = conf.getString("vectorization.shapshot").get
    val tensorName = conf.getString("vectorization.tensor-name").get

    val snap = TensorflowSnapshot.read(baseFile)
    val tensor = snap.tensorView(tensorName)
    val matrix = tensor.floats().toMatrix()
    snap.close()

    new VectorRepr(matrix, rest)
  }

  @Provides
  @inject.Singleton
  def dictVectorizer(
    dict: Dictionary
  ): DictionaryVectorizer = {
    new DictionaryVectorizer(dict.words.zipWithIndex.toMap, KnpFeatures.featureExtractor("norm"))
  }

  @Provides
  @inject.Singleton
  def restorer(
    dict: Dictionary
  ): DictionaryRestorator = {
    DictionaryVectorizer.restorator(dict.words)
  }

  @Provides
  def ece(
    aref: ActorSystem
  ): ExecutionContextExecutor = aref.dispatcher

  @Provides
  def messages(app: Application): Messages = {
    Messages.Implicits.applicationMessages(Lang.defaultLang, app)
  }

  @inject.Singleton
  @Provides
  def grpcClient(
    conf: Configuration,
    ec: ExecutionContextExecutor
  ): ManagedChannel = {
    val uri = conf.getString("executor.uri").get

    val bldr = ManagedChannelBuilder.forTarget(uri)
    bldr.executor(ec)
    bldr.usePlaintext(true)
    bldr.directExecutor()
    bldr.build()
  }

  @Provides
  def service(
    chan: ManagedChannel
  ): PredictionService = {
    PredictionServiceGrpc.stub(chan)
  }

  @Provides
  def serviceImpl(
    svc: PredictionService,
    conf: Configuration,
    vect: DictionaryVectorizer
  ): SentenceSummarizerClient = {
    val batchSize = conf.getInt("vectorization.batch-size").get
    new SentenceSummarizerClient(svc, batchSize, 1L)
  }

  @Provides
  @inject.Singleton
  def knp(conf: Configuration): KnpAnalyzer = {
    val knp = KnpTabPipeParser.apply(conf.underlying)
    new KnpAnalyzer(knp)
  }

}

class KnpAnalyzer(parser: KnpTabPipeParser) {
  private val lock = new Object

  def analyze(data: String): OldAngUglyKnpTable = {
    lock.synchronized {
      parser.parse(data).get
    }
  }
}
