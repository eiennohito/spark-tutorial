package code

import com.google.inject.Inject
import play.api.mvc.{Action, Controller}

/**
  * @author eiennohito
  * @since 2017/01/31
  */
@Inject()
class IndexController extends Controller {
  def index() = Action { _ => Ok(views.html.index()) }
}
