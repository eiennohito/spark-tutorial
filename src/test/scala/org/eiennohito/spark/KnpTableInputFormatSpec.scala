package org.eiennohito.spark

import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfterAll, FreeSpec, Matchers}
import ws.kotonoha.akane.checkers.JumanPosCheckers

/**
  * @author eiennohito
  * @since 2016/09/26
  */
abstract class SparkFree extends FreeSpec with Matchers with BeforeAndAfterAll {
  protected var ctx: SparkContext = null

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    val cfg = new SparkConf(true)
    cfg.setMaster("local[1]")
    cfg.setAppName("test")
    cfg.set("spark.ui.enabled", "false")
    ctx = new SparkContext(cfg)
  }

  override protected def afterAll(): Unit = {
    try {
      ctx.stop()
      ctx = null
    } finally {
      super.afterAll()
    }
  }
}

class KnpTableInputFormatSpec extends SparkFree {
  "TableInputFormat" - {
    "works" in {
      val file = EosDelimited.readFrom(ctx, "./src/test/resources/trees.01.knp")
      file.count() shouldBe 2

      val items = file.flatMap { t => CaseFrameProcessor.parseTree(t) }

      items.count() shouldBe 2

      val noun = JumanPosCheckers.default.pos("名詞")

      val nouns = items.flatMap { tree => tree.lexemes.filter(l => noun.check(l)) }
      nouns.count() shouldBe 13

    }

    "produces some frame inputs" in {
      val file = EosDelimited.readFrom(ctx, "./src/test/resources/trees.01.knp")
      file.count() shouldBe 2

      val items = file.flatMap { t =>
        CaseFrameProcessor.parseTree(t).toIterator.flatMap(CaseFrameProcessor.processFramesForTree)
      }

      items.count() should be > 0L
    }
  }
}
