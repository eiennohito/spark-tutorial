package org.eiennohito.spark

import org.scalatest.{FreeSpec, Matchers}
import ws.kotonoha.akane.analyzers.knp.LexemeWrapper
import ws.kotonoha.akane.analyzers.knp.raw.OldAndUglyKnpLexeme
import ws.kotonoha.akane.resources.Classpath

/**
  * @author eiennohito
  * @since 2017/01/17
  */
class WordFullCalcSmTest extends FreeSpec with Matchers {
  "WordFullCalc" - {
    "lex<->lex" in {
      val data = Classpath.lines("trees.01.knp").filter(_.length > 5).filter(s => "#*+".indexOf(s.charAt(0)) < 0)

      for (o <- data) {
        val om = OldAndUglyKnpLexeme.fromTabFormat(o)
        val l1 = WordFullCalculator.fromLexeme(om)
        val s1 = WordFullCalculator.lexemeToString(new LexemeWrapper(l1))
        val o1 = OldAndUglyKnpLexeme.fromTabFormat(s1)
        val l2 = WordFullCalculator.fromLexeme(o1)
        val s2 = WordFullCalculator.lexemeToString(new LexemeWrapper(l2))
        s1 shouldBe s2
      }
    }
  }
}
