package org.eiennohito.spark

import java.io.{BufferedReader, StringReader}

import org.scalatest.{FreeSpec, Matchers}
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.resources.Classpath
import ws.kotonoha.akane.vectorization.DictionaryVectorizer

class KnpSeqVectorizerSpec extends FreeSpec with Matchers {
  "KnpSeqVectorizer" - {
    "works" in {
      val data = Classpath.fileAsString("trees.01.knp")
      val parser = new KnpTabFormatParser
      val reader = new BufferedReader(new StringReader(data))
      val t1 = parser.parse(reader)
      val t2 = parser.parse(reader)

      val lines = Classpath.lines("jap.2k.dic")
      val dic = DictionaryVectorizer.fromWordFreqPairs(DictionaryVectorizer.fromLines(lines, 50000), VectorizeKnp.featureExtractor(""))
      val ksv = new KnpSeqVectorizer(dic, minLen = 10)
      val res1 = ksv.makeExamples(t1.get)
      res1 should not be empty
    }
  }
}
