package org.eiennohito.spark

import java.io.{BufferedReader, StringReader}

import org.scalatest.{FreeSpec, Matchers}
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.resources.Classpath
import ws.kotonoha.akane.vectorization.SentenceToken

class JumanppEncoderSpec extends FreeSpec with Matchers {
  "JumanppEncoder" - {
    "correctly encodes stuff" in {
      val data = Classpath.fileAsString("tree.weird.knp")
      val parser = new KnpTabFormatParser
      val reader = new BufferedReader(new StringReader(data))
      val t1 = parser.parse(reader).get

      for (mrp <- t1.lexemes) {
        val tok = SentenceToken(
          Array(
            mrp.surface,
            mrp.reading,
            mrp.dicForm,
            mrp.pos.partOfSpeech.name,
            mrp.pos.subPart.name,
            mrp.pos.conjType.name,
            mrp.pos.conjForm.name
          )
        )

        val encoded = JumanppTokenEncoder.encode(tok)
        val decoded = JumanppTokenEncoder.decode(encoded)

        decoded.tokens.toIndexedSeq shouldBe tok.tokens.toIndexedSeq
      }
    }
  }
}
