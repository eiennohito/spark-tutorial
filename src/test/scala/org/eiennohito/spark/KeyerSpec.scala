package org.eiennohito.spark

import org.scalatest.{FreeSpec, Matchers}

/**
  * @author eiennohito
  * @since 2017/02/03
  */
class KeyerSpec extends FreeSpec with Matchers {
  "Keyer" - {
    val keyer = new VectorizerKeyer(10, 80)
    "works" in {
      val key1 = keyer.keyFor(15, 80)
      val key2 = keyer.keyFor(60, -3)

      Math.abs(key1._1) should be >= Math.abs(key2._2)
    }
  }
}
