package org.eiennohito.spark

/**
  * @author eiennohito
  * @since 2017/01/04
  */
class KnpFormatCnter extends SparkFree {
  "WordCalculator" - {
    "works" in {
      val input = ctx.textFile("./src/test/resources/trees.01.knp")
      val data = WordCalculator.countKnpFormatMrphs(input).collect()

      data.length should be >= 1
      data.map(_._2).max shouldBe 3
    }

    "works with juman" in {
      val input = ctx.textFile("./src/test/resources/data.01.jmn")
      val data = WordCalculator.countKnpFormatMrphs(input, "juman").collect()

      val objs = data.map(_._1).filter(_.startsWith("@"))
      objs shouldBe empty
    }
  }
}
