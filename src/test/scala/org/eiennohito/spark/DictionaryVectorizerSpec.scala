package org.eiennohito.spark

import java.io.{BufferedReader, StringReader}

import org.scalatest.{FreeSpec, Matchers}
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.resources.Classpath
import ws.kotonoha.akane.vectorization.DictionaryVectorizer

/**
  * @author eiennohito
  * @since 2017/01/17
  */
class DictionaryVectorizerSpec extends FreeSpec with Matchers {
  "DictionaryVectorizer" - {
    "works" in {
      val data = Classpath.fileAsString("trees.01.knp")
      val parser = new KnpTabFormatParser
      val reader = new BufferedReader(new StringReader(data))
      val t1 = parser.parse(reader)
      val t2 = parser.parse(reader)

      val lines = Classpath.lines("jap.2k.dic")
      val dic = DictionaryVectorizer.fromWordFreqPairs(DictionaryVectorizer.fromLines(lines, 50000), VectorizeKnp.featureExtractor(""))

      val w1 = dic.vectorize(t1.get.lexemeIter)
      val w2 = dic.vectorize(t2.get.lexemeIter)
      val x1 = List[Long](2, 354, 97, 5, 101, 1199, 99, 8, 20, 5, 99, 8, 20, 1368, 14, 1763, 156, 101, 116, 22, 3)
      val x2 = List[Long](2, 266, 116, 19, 1495, 93, 191, 103, 1433, 96, 1800, 22, 5, 93, 5, 96, 180, 93, 5, 99, 141, 26, 352, 14, 101, 116, 15, 115, 22, 117, 98, 118, 14, 3)
      x1 shouldBe w1
      x2 shouldBe w2
    }
  }
}
