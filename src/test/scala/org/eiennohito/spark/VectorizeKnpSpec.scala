package org.eiennohito.spark

import org.scalatest.{FreeSpec, Matchers}
import org.tensorflow.example.example.Example
import ws.kotonoha.akane.analyzers.knp.raw.OldAngUglyKnpTable
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.resources.Classpath
import ws.kotonoha.akane.vectorization.DictionaryVectorizer

import scala.util.Random

/**
  * @author eiennohito
  * @since 2017/01/27
  */
class VectorizeKnpSpec extends FreeSpec with Matchers {
  def tree(file: String): OldAngUglyKnpTable = {
    val lines = Classpath.lines(file)
    val parser = new KnpTabFormatParser
    parser.parse(lines)
  }

  def loadDic() = {
    val lines = Classpath.lines("/seiki.20k.dic")
    val words = DictionaryVectorizer.fromLines(lines, 20000)
    val dic = DictionaryVectorizer.fromWordFreqPairs(words, VectorizeKnp.featureExtractor("norm"))
    val rest = DictionaryVectorizer.restorator(words.map(_._1))
    (dic, rest)
  }

  lazy val (dic, rest) = loadDic()

  def printRestored(e: Example): Unit = {
    val (q, a) = restore(e)
    println(a)
    println(q)
  }

  private def restore(e: Example) = {
    val ids = e.features.get.feature("words").getInt64List.value
    val ans = e.features.get.feature("answer").getInt64List.value
    val answer = rest.restore(ans)
    val question = rest.restore(ids)
    (question, answer)
  }

  private def allQA(data: Seq[((Int, Int), Example)]): Seq[(String, String)] = {
    data.map { case (k, v) => restore(v) }
  }

  class FakeRandom extends Random {
    override def nextDouble() = 0.0
  }

  "KnpVectorizer" - {
    val vec = new KnpVectorizer(dic, new FakeRandom())

    "works?" in {
      val tr1 = tree("tree.04.knp")
      val ex = vec.convertOne2(tr1)
      for ((id, e) <- ex) {
        println(id)
        printRestored(e)
      }
    }

    "restore verb -> -masu stem from nounification" in {
      val tr1 = tree("tree.03.knp")
      val converted = vec.convertOne2(tr1)
      val data = allQA(converted)
      val o = data.find { case (quest, answ) =>
        answ.contains("付く/つく?就く/つく?搗く/つく?点く/つく?着く/つく?突く/つく") &&
        quest.contains("$$$ 基本連用形")}
      o should not be empty
    }
  }
}
