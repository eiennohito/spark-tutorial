package org.eiennohito.spark

import java.nio.file.Files

import org.apache.hadoop.io.compress.GzipCodec
import ws.kotonoha.spark.tensorflow.{TFExample, TFRecords}

/**
  * @author eiennohito
  * @since 2017/01/20
  */
class TFRecordSpec extends SparkFree {

  def someExamples(num: Int) = {
    (0 until num).map { i =>
      TFExample(
        "ints" -> TFExample.i64(i, i, i, i),
        "floats" -> TFExample.f32(i, i, i, i)
      )
    }
  }

  "TFRecordSaver" - {
    "works" in {
      val inp = someExamples(1000)
      val input = ctx.parallelize(inp, 2)
      val file = Files.createTempFile("tflow", "test")
      Files.delete(file)
      val path = file.toString
      TFRecords.writeToFile(input, path, Some(classOf[GzipCodec]))
      val output = TFRecords.readFromFile(ctx, path)
      val result = output.collect()
      result shouldBe inp
    }
  }
}
