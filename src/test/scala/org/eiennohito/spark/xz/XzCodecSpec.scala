package org.eiennohito.spark.xz

import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.compress.CompressionCodecFactory
import org.eiennohito.spark.SparkFree
import org.eiennohito.spark.codecs.XzCompressionCodec
import ws.kotonoha.akane.resources.Classpath

/**
  * @author eiennohito
  * @since 2017/01/26
  */
class XzCodecSpec extends SparkFree {
  "XZCodec" - {
    "can be found in codec collection" in {
      val codecs = new CompressionCodecFactory(ctx.hadoopConfiguration)
      val codec = codecs.getCodec(new Path("test.xz"))
      codec.getClass shouldBe classOf[XzCompressionCodec]
    }

    "decompresses a file" in {
      val file = Classpath.lines("/trees.01.knp")
      val decompressed = ctx.textFile("src/test/resources/trees.01.knp.xz").collect()
      file shouldBe decompressed.toSeq
    }
  }
}
