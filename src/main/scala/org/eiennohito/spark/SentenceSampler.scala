package org.eiennohito.spark

import java.nio.file.Path

import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{HashPartitioner, SparkConf, SparkContext}
import org.rogach.scallop.ScallopConf

object SentenceSampler {
  private class DataReshufflerArgs(args: Seq[String]) extends ScallopConf(args) {
    val input = opt[Path](required = true)
    val output = opt[String](required = true)
    val outStat = opt[String]()
    val numFiles = opt[Int](required = true)
    val percentage = opt[Double](default = Some(0.01)).map(_ / 100.0)
    val seed = opt[Long](default = Some(0xdeadbeef))
    val shuffle = toggle(default = Some(true))
  }

  import ws.kotonoha.akane.resources.FSPaths._

  def main(args: Array[String]): Unit = {
    val a = new DataReshufflerArgs(args)
    a.verify()

    val filenames = a.input().lines().filter(_.length > 5).mkString(",")

    val conf = new SparkConf().setAppName("SentenceSampler")
    conf.registerKryoClasses(Array(

    ))

    val sc = new SparkContext(conf)

    try {
      doWork(sc, filenames, a)
    } finally {
      sc.stop()
    }
  }

  def doWork(sc: SparkContext, files: String, args: DataReshufflerArgs): Unit = {
    val lines = sc.textFile(files)

    val totalCntr = sc.longAccumulator("totals")
    val sampledCntr = sc.longAccumulator("sampled")

    val pairs = lines.mapPartitions(iter => {
      iter.grouped(2).map { case Seq(k, v) => totalCntr.add(1); k -> v }
    }, preservesPartitioning = true)

    val perc = args.percentage()
    val sampled = if (perc >= 1) pairs else pairs.sample(withReplacement = false, perc, seed = args.seed())
    val shuffled = if (args.shuffle()) {
      sampled.partitionBy(new HashPartitioner(args.numFiles()))
    } else {
      val numParts = sampled.getNumPartitions
      if (args.numFiles() < numParts) {
        sampled.coalesce(args.numFiles())
      } else sampled
    }

    shuffled.map {case (a, b) => sampledCntr.add(1); s"$a\n$b"}.saveAsTextFile(args.output(), classOf[GzipCodec])

    val outstat = args.outStat.getOrElse(args.output())

    s"$outstat.stats".p.writeLines(Seq(
      s"Total: ${totalCntr.value}",
      s"Sampled: ${sampledCntr.value}",
      f"Percentage: ${sampledCntr.value / totalCntr.value.toDouble}%.5f"
    ))
  }
}
