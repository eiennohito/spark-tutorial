package org.eiennohito.spark


import java.io.{BufferedReader, ByteArrayInputStream, InputStreamReader, StringReader}
import java.nio.{ByteBuffer, CharBuffer}

import io.netty.buffer.ByteBuf
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.{InputSplit, TaskAttemptContext}
import org.apache.hadoop.mapreduce.lib.input.{LineRecordReader, TextInputFormat}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import ws.kotonoha.akane.analyzers.knp.raw.OldAngUglyKnpTable
import ws.kotonoha.akane.analyzers.knp.{KihonkuApi, TableApi}
import ws.kotonoha.akane.io.Charsets
import ws.kotonoha.akane.parser.KnpTabFormatParser

//This class will be a key for predicate-argument-relation triple
case class PredicateArgument(
  cftype: String,
  predicate: String,
  argument: String
)

//This is a result class for argument information over a predicate
case class PredicateInfo(
  prediate: String,
  total: Long,
  //keys in this map are relations
  arguments: Map[String, Seq[(String, Long)]]
) {
  def stringRepr: String = {
    val bldr = new StringBuilder
    bldr.append(prediate).append(" ").append(total).append("\n")
    for ((kind, args) <- arguments) {
      bldr.append(kind).append(" - ")
      for ((arg, cnt) <- args) {
        bldr.append(arg).append(":").append(cnt).append(" ")
      }
      bldr.append("\n")
    }
    bldr.append("EOA\n")
    bldr.result()
  }
}

//this object is entry point for Spark
object PredicateArgumentCalculator {
  //this imports oprtations for operating on files
  //this is not a scala standard library, but my own
  import ws.kotonoha.akane.resources.FSPaths._

  //entry point of scala applications are main functions
  //inside objects
  def main(args: Array[String]): Unit = {
    //first thing to do is to create a Spark context
    //it is available by default in a shell as sc
    val conf = new SparkConf().setAppName("PredArgCalc")
    val ctx = new SparkContext(conf)

    //giving names for input arguments
    val inputFilesHere = args(0)
    val output = args(1)

    //read input filenames from file and put them to a List collection
    val inputFiles = inputFilesHere.p.lines().toList

    //some debungging information
    //you can use variable substitution in Scala by prefixing a
    //string literal with s
    println(s"using ${inputFiles.length} input files: ${inputFiles.headOption}")

    //create inputs
    //mkString method joins all strings in a collection
    //with a provided separator
    val inputs = EosDelimited.readFrom(ctx, inputFiles.mkString(","))

    //two flatmaps create basic data for the further pipeline
    val predArgs = inputs
      //case syntax here is needed to use extraction of variables from a tuple
      //first we need to parse KNP trees from their table representation
      .flatMap { txt => CaseFrameProcessor.parseTree(txt) }
      //then we need to extract triples from those trees
      .flatMap(tree => CaseFrameProcessor.processFramesForTree(tree))

    //after triples are extracted, the only thing left is doing the usual count aggregation
    val counted = predArgs.map(x => x -> 1L).reduceByKey(_ + _)

    //tuples with count can be grouped using predicates as keys,
    //collecting all arguments into one place
    //take minimum of 5 instances of an argument
    val groups = counted.filter(x => x._2 > 4).groupBy(_._1.predicate)

    //this transforms intermediate data representation to the final one
    val objects = groups.map { case (predicate, data) =>
      val arguments = data.groupBy(_._1.cftype).map { case (cftype, args) =>
        cftype -> args.map { case (pa, cnt) => (pa.argument, cnt) }.toVector.sortBy(-_._2)
      }.toMap

      val total = arguments.valuesIterator.flatMap(_.iterator).map(_._2).sum
      PredicateInfo(predicate, total, arguments)
    }

    //and finally, we save all data to the output directory, creating at most 50 files
    val numParts = 50 min objects.getNumPartitions
    objects.sortBy(_.total, ascending = false, numPartitions = numParts).map(_.stringRepr).saveAsTextFile(output)

    ctx.stop()
  }
}

object CaseFrameProcessor {
  private val treeParser = new KnpTabFormatParser

  def parseTree(t: Text): Option[OldAngUglyKnpTable] = {
    val inp = new ByteArrayInputStream(t.getBytes, 0, t.getLength)
    val rdr = new BufferedReader(new InputStreamReader(inp, Charsets.utf8))
    parseTree(rdr)
  }

  def parseTree(t: String): Option[OldAngUglyKnpTable] = {
    val rdr = new BufferedReader(new StringReader(t))
    parseTree(rdr)
  }

  private def parseTree(rdr: BufferedReader): Option[OldAngUglyKnpTable] = {
    try {
      treeParser.parse(rdr)
    } catch {
      case e: Exception => None
    }
  }

  def repr(k: KihonkuApi) = {
    k.valueOfFeature("正規化代表表記")
  }

  //this function creates P-A-R triples for an input KNP tree
  def processFramesForTree(input: TableApi): Iterator[PredicateArgument] = {
    //first we take all predicates, treating kihonku that contain yougens as predicates
    val yougens = input.kihonkuIter.filter(_.valueOfFeature("用言").isDefined)
    //then comes the hardest Scala part
    //for each such yougen we want to output from 0 to n triples.
    //For comprehensions feature of Scala allows to write the logic of checking
    //possibly missing values very concisely.
    //Basically you can think of it as that execution will come to bottom
    //only if the value of something is present
    yougens.flatMap { k =>
      for {
        //normalized writing of the predicate itself should be present
        //toIterator here needs to be present to satifsfy typechecker
        predRepr <- repr(k).toIterator
        //then we find all children of current kihonku
        child <- input.kihonkuIter.filter(_.depNumber == k.number)
        //check if they have kakari feature present
        //and use its value as relation type
        relation <- child.valueOfFeature("係")
        //finally, we check that the child has normalized writing present
        childRepr <- repr(child)
        //and with all these values we create a single triple
      } yield PredicateArgument(relation, predRepr, childRepr)
    }
  }
}

