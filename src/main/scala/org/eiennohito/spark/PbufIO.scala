package org.eiennohito.spark

import com.google.protobuf.CodedOutputStream
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.mapred._
import org.apache.hadoop.util.Progressable
import scalapb.{GeneratedMessage, GeneratedMessageCompanion, Message}

/**
  * @author eiennohito
  * @since 2017/01/17
  */
class PbufOutFormat[T <: GeneratedMessage] extends FileOutputFormat[Long, T] {
  override def getRecordWriter(ignored: FileSystem, job: JobConf, name: String, progress: Progressable) = {

    val smt = ignored.create(FileOutputFormat.getTaskOutputPath(job, name))
    val outStream = CodedOutputStream.newInstance(smt)

    new RecordWriter[Long, T] {
      override def write(key: Long, value: T): Unit = {
        outStream.writeInt32NoTag(value.serializedSize)
        value.writeTo(outStream)
        progress.progress()
      }

      override def close(reporter: Reporter): Unit = {
        outStream.flush()
        smt.close()
      }
    }
  }
}

class PbufInFormat[T <: GeneratedMessage with Message[T]](implicit meta: GeneratedMessageCompanion[T]) extends FileInputFormat[Long, T] {
  override def getRecordReader(split: InputSplit, job: JobConf, reporter: Reporter) = {

    new RecordReader[Long, T] {
      override def next(key: Long, value: T): Boolean = ???

      override def getProgress = ???

      override def getPos = ???

      override def createKey() = ???

      override def close() = ???

      override def createValue() = ???
    }
  }

  override def isSplitable(fs: FileSystem, filename: Path): Boolean = false
}
