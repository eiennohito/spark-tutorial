package org.eiennohito.spark

import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import ws.kotonoha.akane.analyzers.juman.{JumanMorpheme, JumanPos, JumanText}
import ws.kotonoha.akane.analyzers.knp.raw.OldAndUglyKnpLexeme
import ws.kotonoha.akane.analyzers.knp.{LexemeApi, LexemeWrapper, TableConverter}
import ws.kotonoha.akane.parser.JumanPosSet

/**
  * @author eiennohito
  * @since 2017/01/04
  */
object WordFullCalculator {
  case class MyKey(cnt: Long, repr: String)

  implicit object mykeyOrdering extends Ordering[MyKey] {
    override def compare(x: MyKey, y: MyKey): Int = {
      val c1 = x.cnt.compareTo(y.cnt)
      c1 match {
        case 0 => x.repr.compareTo(y.repr)
        case _ => c1
      }
    }
  }

  def fromLexeme(lexeme: OldAndUglyKnpLexeme): JumanMorpheme = {
    JumanMorpheme(
      surface = lexeme.surface,
      reading = lexeme.reading,
      baseform = lexeme.dicForm,
      posInfo = JumanPos(
        pos = lexeme.pos.pos,
        subpos = lexeme.pos.subpos,
        category = lexeme.pos.category,
        conjugation = lexeme.pos.conjugation
      ),
      features = TableConverter.transformFeatures(lexeme.tags, Set.empty)
    )
  }

  import ws.kotonoha.akane.resources.FSPaths._

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Morpheme Counter")

    conf.registerKryoClasses(Array(
      classOf[JumanMorpheme],
      classOf[MyKey]
    ))

    val input = args(0)
    val output = args(1)
    val border = args(2).toInt

    val filenames = input.p.lines().filter(_.length > 5).mkString(",")

    println(s"using $filenames")

    val sc = new SparkContext(conf)

    try {
      doWork(output, filenames, sc, border)
    } finally {
      sc.stop()
    }
  }

  private val posset = JumanPosSet.default

  def lexemeToString(l: LexemeApi): String = {
    val bldr = new StringBuilder
    bldr.append(l.surface).append(" ")
    bldr.append(l.reading).append(" ")
    bldr.append(l.dicForm).append(" ")
    val p = l.pos
    val po = posset.pos(p.pos)
    bldr.append(po.name).append(" ").append(po.num).append(" ")
    val so = po.subtypes(p.subpos)
    bldr.append(so.name).append(" ").append(so.num).append(" ")
    val ct = posset.conjugatons(p.category)
    bldr.append(ct.name).append(" ").append(ct.num).append(" ")
    val cf = ct.conjugations(p.conjugation)
    bldr.append(cf.name).append(" ").append(cf.num).append(" ")

    bldr.append("NIL ")

    for (k <- l.featureKeys) {
      bldr.append("<")
      bldr.append(k)
      l.valueOfFeature(k) match {
        case Some(v) =>
          bldr.append(":")
          bldr.append(v)
        case _ =>
      }
      bldr.append(">")
    }

    bldr.replaceAllLiterally("\"", "\'")
  }

  private def canonicalWrit(k: JumanMorpheme) = {
    k.features.find(_.key == "代表表記").flatMap(_.value).getOrElse(s"${k.baseform}/${k.reading}")
  }

  private def doWork(output: String, filenames: String, sc: SparkContext, border: Int) = {
    val inp = sc.textFile(filenames)

    val counts: RDD[(JumanMorpheme, Long)] = countKnpFormatMrphs(inp).filter(_._2 > border)

    val c2 = counts.map {
      case (k, v) => (MyKey(v, canonicalWrit(k)), k)
    }.persist(StorageLevel.MEMORY_AND_DISK_SER)

    c2.sortByKey(numPartitions = partNum(1000, c2.getNumPartitions))
      .map { case (a, b) => s"${a.cnt}\t${lexemeToString(new LexemeWrapper(b))}" }
      .saveAsTextFile(output, classOf[GzipCodec])
  }

  private def partNum(wanted: Int, current: Int): Int = {
    if (current > wanted) wanted else current
  }

  def countKnpFormatMrphs(inp: RDD[String], syntax: String = "knp"): RDD[(JumanMorpheme, Long)] = {
    val jumanMorphemes = inp.filter(s => s.length > 10 && "+*#".indexOf(s.charAt(0)) < 0)
    val processed = syntax match {
      case "knp" => jumanMorphemes.flatMap(s => try {
        val om = OldAndUglyKnpLexeme.fromTabFormat(s)
        val jl = fromLexeme(om)
        jl -> 1L :: Nil
      } catch {
        case e: Exception =>
          System.err.print(s"could not parse $s")
          e.printStackTrace(System.err)
          Nil
      })
      case "juman" => jumanMorphemes.flatMap { s =>
        val start = if (s.charAt(0) == '@') 2 else 0
        try {
          val line = JumanText.parseLine(s, start, s.length)
          line -> 1L :: Nil
        } catch {
          case e: Exception =>
            System.err.print(s"could not parse $s")
            e.printStackTrace(System.err)
            Nil
        }
      }
      case _ => throw new Exception(s"unknow syntax: $syntax")
    }
    val sorted = processed.reduceByKey(_ + _, partNum(5000, processed.getNumPartitions))
    sorted
  }
}
