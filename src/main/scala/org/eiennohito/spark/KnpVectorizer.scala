package org.eiennohito.spark

import org.apache.commons.lang3.StringUtils
import org.tensorflow.example.example.Example
import ws.kotonoha.akane.analyzers.juman.{JumanPos, JumanStylePos}
import ws.kotonoha.akane.analyzers.knp.LexemeApi
import ws.kotonoha.akane.analyzers.knp.raw.{OldAndUglyKnpLexeme, OldAngUglyKnpTable}
import ws.kotonoha.akane.utils.XInt
import ws.kotonoha.akane.vectorization.DictionaryVectorizer
import ws.kotonoha.spark.tensorflow.TFExample

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.Random
import scala.util.hashing.MurmurHash3

/**
  * @author eiennohito
  * @since 2017/01/27
  */
class KnpVectorizer(
  dic: DictionaryVectorizer,
  random: Random = new Random(),
  minLen: Int = 10,
  maxLen: Int = 80
) {

  private var subsampled = 0L
  private var lastSubsampled = 0L
  def subsampleCnt: Long = {
    val res = subsampled - lastSubsampled
    lastSubsampled = subsampled
    res
  }

  import KnpVectorizer._

  def prob(border: Double): Boolean = random.nextDouble() <= border

  private val wordStemId = DictionaryVectorizer.conjOf(JumanPos(0, 0, 21, 1))

  private def makeTargets(table: OldAngUglyKnpTable, self: Replacement, others: mutable.Buffer[Replacement], idx: Int): Seq[Target] = {

    val headId = self.ids.head

    //drop frequent words
    val toDrop = dic.shouldKeep(headId)
    if (!prob(toDrop)) {
      subsampled += 1
      return Nil
    }

    val res = Seq.newBuilder[Target]

    val question = 1L

    val lex = self.lex
    val cf = lex.canonicForm()

    //for things that have changed POS
    if ("va".indexOf(cf.charAt(cf.length - 1)) != -1) {
      //cf ends with a or v
      val alts = collectAlts(table.lexemes(idx))
      val altPos = alts.flatMap(_.origPos).distinct
      if (alts.nonEmpty && altPos.length == 1) {
        //there is only one
        val thepos = altPos.head
        val writ = alts.flatMap(_.origRepr).sorted.mkString("?")
        val wid = dic.idFor(writ)
        if (wid.isDefined) {
          val id = wid.get.toLong
          if (prob(0.5)) {
            res += Target(Seq(question), 0, id)
          }
          if (thepos.category != 0) {
            val posId = DictionaryVectorizer.conjOf(thepos).toLong
            res += Target(Seq(question, posId), 0, id)
          }
        }
      } else if (alts.isEmpty) {
        val posChange = parsePosChange(lex)
        posChange.flatMap(_.origRepr).flatMap(dic.idFor) match {
          case Some(id) =>
            if (prob(0.5)) {
              res += Target(Seq(question), 0, id)
            }
            val origPos = posChange.flatMap(_.origPos)
            if (origPos.isDefined) {
              val posId = DictionaryVectorizer.conjOf(origPos.get).toLong
              res += Target(Seq(question, posId), 0, id)
            }
          case _ =>
            val df = lex.valueOfFeature("代表表記変更").flatMap(dic.idFor).foreach { id =>
              val basic = Seq(question)
              res += Target(basic, 0, id)
              if (self.ids.length > 1 && prob(0.3)) {
                res += Target(basic ++ self.ids.drop(1), 0, id)
              }
            }
        }
      }
    }

    //Basic question -- all parts. This comes always.
    res += Target(self.ids.updated(0, question), 0, headId)


    if (lex.pos.category != 0) {
      //Replace conjugation tags sometimes (30% of cases) with stems
      if (prob(0.3) && lex.pos.conjugation != 1) {
        res += Target(Seq(question, wordStemId), 0, headId)
      }

      //Drop conjugation tags in 5% of cases
      if (prob(0.05)) {
        res += Target(Seq(question), 0, headId)
      }
    }

    //Suru-verbs and adverbs directly followed by suru
    //drop suru in 20% cases and drop suru conjugation in 5%
    if (
      ((lex.pos.pos == 6 && lex.pos.subpos == 2) || //suru-verb-nouns
        (lex.pos.pos == 8)) && //adjectives
      ( (idx + 1) < others.length && { //has next word
        val next = others(idx + 1) //and it is suru
        next.lex.dicForm == "する"
      })
    ) {
      if (prob(0.2)) {
        res += Target(Seq(question) ++ others(idx + 1).ids.drop(1), 1, headId) //drop suru only, copy its content here
      }

      if (prob(0.05)) {
        res += Target(Seq(question), 1, headId) //completely drop suru node
      }
    }

    res.result().filter(tgt => !KnpVectorizer.isUnk(tgt.target))
  }

  private val keyer = new VectorizerKeyer(minLen, maxLen)

  def convertOne2(table: OldAngUglyKnpTable): Seq[((Int, Int), Example)] = {
    val preprocess = table.lexemeIter.map { lex =>
      val id = dic.vecOne(lex)
      Replacement(lex, id, shouldTarget(lex, id))
    }.toBuffer

    val unks = preprocess.count(x => isUnk(x.ids))

    //skip sentence if there more than 10% morphemes containing unks
    if ((unks.toFloat / preprocess.length) > 0.1f) {
      return Nil
    }

    val expectedSize = preprocess.map(_.ids.size).sum

    if (!keyer.inRange(expectedSize)) return Nil

    val sentenceHash = KnpVectorizer.hashSentence(table)
    val key = keyer.keyFor(expectedSize, sentenceHash)

    val results = new ArrayBuffer[((Int, Int), Example)]()

    var i = 0
    val end = preprocess.length
    while (i < end) {
      val item = preprocess(i)
      if (item.target) {
        val start = Vector(2L) ++ preprocess.view(0, i).flatMap(_.ids).toVector
        val targets = makeTargets(table, item, preprocess, i)
        results ++= targets.groupBy(_.length).toSeq.flatMap { case (len, tgts) =>
          val end = preprocess.view(i + len + 1, preprocess.length).flatMap(_.ids).toVector ++ Vector(3L)
          tgts.map { t =>
            val seq = start ++ t.ids ++ end
            key -> TFExample(
              "words" -> TFExample.i64seq(seq),
              "answer" -> TFExample.i64(t.target),
              "len" -> TFExample.i64(seq.length)
            )
          }
        }
      }
      i += 1
    }

    results
  }
}

object KnpVectorizer {
  def hashSentence(table: OldAngUglyKnpTable): Int = {
    var start = 0xdeadbeef
    val iter = table.lexemeIter
    while (iter.hasNext) {
      val i = iter.next()
      start = MurmurHash3.stringHash(i.surface, start)
    }
    MurmurHash3.finalizeHash(start, table.lexemeCnt)
  }


  private val unkStart = DictionaryVectorizer.hardcodedSize - 1
  private val unkEnd = unkStart + DictionaryVectorizer.unkWordCheckers.length + 1

  def isUnk(id: Long): Boolean = {
    unkStart <= id && id < unkEnd
  }

  def isUnk(ids: Seq[Long]): Boolean = {
    val headId = ids.head
    isUnk(headId)
  }

  def shouldTarget(lex: LexemeApi, id: Seq[Long]): Boolean = {
    (lex.featureExists("内容語") || lex.featureExists("準内容語")) && (!lex.featureExists("記号"))
  }

  case class Replacement(lex: LexemeApi, ids: Seq[Long], target: Boolean)

  case class Target(ids: Seq[Long], length: Int, target: Long)

  case class Alt(pos: JumanPos, repr: String, origRepr: Option[String], origPos: Option[JumanPos])

  private val separators = "\"\'"

  //ALT-い-い-い-6-1-0-0-'代表表記:居/いv 代表表記変更:居る/いる 品詞変更:い-い-いる-2-0-1-8'
  def parseAlt(str: String): Seq[Alt] = {
    val start = StringUtils.indexOfAny(str, separators)
    if (start == -1) {
      return Nil
    }

    val firstPart = str.substring(0, start - 1)
    val pos = StringUtils.split(firstPart, '-') match {
      case Array(a, b, c, XInt(p1), XInt(p2), XInt(p3), XInt(p4)) => JumanPos(p1, p2, p3, p4)
      case _ => return Nil
    }

    val features = str.substring(start + 1, str.length - 1)
    val splitted = StringUtils.split(features, ' ')

    var origRepr: Option[String] = None
    var origPos: Option[JumanPos] = None
    var repr = ""

    for (spl <- splitted) {
      StringUtils.split(spl, ":", 2) match {
        case Array(k, v) =>
          k match {
            case "代表表記変更" => origRepr = Some(v)
            case "代表表記" => repr = v
            case "品詞変更" =>
              StringUtils.split(v, '-') match {
                case Array(a, b, c, XInt(p1), XInt(p2), XInt(p3), XInt(p4)) =>
                  origPos = Some(JumanPos(p1, p2, p3, p4))
                case _ =>
              }
            case _ =>
          }
        case _ =>
      }
    }

    List(Alt(pos, repr, origRepr, origPos))
  }

  def collectAlts(lexeme: OldAndUglyKnpLexeme): Seq[Alt] = {
    val altStrings = lexeme.tags.filter(_.startsWith("ALT-")).map(_.substring(4))
    altStrings.flatMap(parseAlt) ++ parsePosChange(lexeme)
  }

  def wrapPos(jp: JumanStylePos): JumanPos = {
    jp match {
      case j: JumanPos => j
      case _ => JumanPos(jp.pos, jp.subpos, jp.category, jp.conjugation)
    }
  }

  def parsePosChange(lex: LexemeApi): Option[Alt] = {
    lex.valueOfFeature("品詞変更").flatMap { v =>
      val feats = StringUtils.indexOfAny(v, separators)
      if (feats < 0) None
      else {
        val part = v.substring(0, feats - 1)
        val pos = StringUtils.split(part, '-') match {
          case Array(a, b, c, XInt(p1), XInt(p2), XInt(p3), XInt(p4)) =>
            Some(JumanPos(p1, p2, p3, p4))
          case _ => None
        }
        val rest = v.substring(feats + 1, v.length - 1)
        val repr = rest.split(' ').find(_.startsWith("代表表記")).map(_.substring(5))
        Some(Alt(wrapPos(lex.pos), lex.canonicForm(), repr, pos))
      }
    }
  }

}
