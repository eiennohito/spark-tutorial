package org.eiennohito.spark

import java.io.{BufferedReader, ByteArrayInputStream, InputStreamReader}

import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.rogach.scallop.ScallopConf
import ws.kotonoha.akane.io.Charsets
import ws.kotonoha.akane.vectorization.JumanppImplicits

import scala.collection.mutable


object JumanppTokenFreq {

  private class JtfConfig(args: Seq[String]) extends ScallopConf(args) {
    val input = opt[String](required = true)
    val output = opt[String](required = true)
    val minFreq = opt[Int](default = Some(5))
    val numFiles = opt[Int](default = Some(100))
  }

  import ws.kotonoha.akane.resources.FSPaths._

  def resolveInput(in: String): String = {
    val char = in.charAt(0)
    if (char == '@') {
      val lines = in.p.lines().filter(l => !l.startsWith("#") && l.length > 4)
      lines.mkString(",")
    } else {
      in
    }
  }

  def main(args: Array[String]): Unit = {
    val a = new JtfConfig(args)
    a.verify()

    val filenames = resolveInput(a.input())

    val conf = new SparkConf().setAppName("JumanppTokenFreq")
    conf.registerKryoClasses(Array(
    ))

    val sc = new SparkContext(conf)

    try {
      doWork(sc, filenames, a)
    } finally {
      sc.stop()
    }
  }

  def doWork(sc: SparkContext, filenames: String, cfg: JtfConfig): Unit = {
    val inputs = EosDelimited.readFrom(sc, filenames)
    val nodes = inputs.mapPartitions { it =>
      val parser = new XJppLatticeParser()
      it.flatMap { txt =>
        val tree = parser.parse(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(txt.getBytes), Charsets.utf8)))
        val topNodes = tree.nodes.filter(_.ranks.contains(1))
        topNodes.map(x => JumanppImplicits.JumanppTokenProjector.project(x).mkString("\u0001") -> 1L)
      }
    }

    val minFreq = cfg.minFreq()

    nodes.reduceByKey(_ + _, numPartitions = cfg.numFiles())
      .filter { case (_, cnt) => cnt >= minFreq }
      .localCheckpoint()
      .sortBy({ case (k, v) => (v, k) }, ascending = false)
      .map { case (t, v) => s"$t\t$v" }
      .saveAsTextFile(cfg.output())
  }
}

object JumanppToNgrams {

  private class JngConfig(args: Seq[String]) extends ScallopConf(args) {
    val input = opt[String](required = true)
    val output = opt[String](required = true)
    val minFreq = opt[Int](default = Some(5))
    val perFile = opt[Long](default = Some(1000000))
    val ngramStart = opt[Int](default = Some(1))
    val ngramEnd = opt[Int](default = Some(1))
    val procCount = opt[Int](default = Some(5000))
  }

  def doWork0(sc: SparkContext, cfg: JngConfig, filenames: String, order: Int): Unit = {
    val inputs = EosDelimited.readFrom(sc, filenames)
    val nodes = inputs.mapPartitions { it =>
      val parser = new XJppLatticeParser()
      it.flatMap { txt =>
        val tree = parser.parse(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(txt.getBytes), Charsets.utf8)))
        val visited = new mutable.BitSet(128)
        val topNodes = tree.nodes.filter(n => n.ranks.contains(1) && visited.add(n.nodeId))
        topNodes.map(x => JumanppTokenEncoder.encode(JumanppImplicits.JumanppTokenProjector.project(x)))
      }.sliding(order, 1).map { tokens => tokens.mkString("\u0000") -> 1L }
    }

    val minFreq = (cfg.minFreq() / order.toFloat).toInt

    val trimmedNodes = nodes.coalesce(cfg.procCount()).reduceByKey(_ + _)
      .filter { case (_, cnt) => cnt >= minFreq }
      .persist(StorageLevel.MEMORY_AND_DISK_SER)

    val count = trimmedNodes.countApprox(1000, 0.9)
    val numFiles = count.getFinalValue().mean / cfg.perFile()

    trimmedNodes
      .sortBy({ case (k, v) => (v, k) }, ascending = false, numPartitions = numFiles.toInt)
      .map { case (t, v) =>
        val parts = t.split('\u0000')
        val repr = parts.map(p => JumanppTokenEncoder.decode(p).mkString("\u0001")).mkString("\u0002")
        s"$repr\t$v"
      }
      .saveAsTextFile(s"${cfg.output()}/ngram-$order", classOf[GzipCodec])
  }

  def doWork(sc: SparkContext, cfg: JngConfig, filenames: String): Unit = {
    for (order <- cfg.ngramStart() to cfg.ngramEnd()) {
      doWork0(sc, cfg, filenames, order)
    }
  }

  def main(args: Array[String]): Unit = {
    val a = new JngConfig(args)
    a.verify()

    val filenames = JumanppTokenFreq.resolveInput(a.input())

    val conf = new SparkConf().setAppName("JumanppNgrams")
    val sc = new SparkContext(conf)
    try {
      doWork(sc, a, filenames)
    } finally {
      sc.stop()
    }
  }
}

object CountDelimitedApprox {

  private class CntConfig(args: Seq[String]) extends ScallopConf(args) {
    val input = opt[String]()
    val timeout = opt[Long](default = Some(1000L))
    val confidence = opt[Double](default = Some(0.95))
    val separator = opt[String](default = Some("EOS\n"))
  }


  def main(args: Array[String]): Unit = {
    val a = new CntConfig(args)
    a.verify()

    val conf = new SparkConf().setAppName("Counter")
    val sc = new SparkContext(conf)
    sc.hadoopConfiguration.set(
      "textinputformat.record.delimiter",
      a.separator()
    )

    val filenames = JumanppTokenFreq.resolveInput(a.input())

    println(s"waiting for ${a.timeout()} and ${a.confidence()}")
    val res = sc.textFile(filenames).countApprox(a.timeout(), a.confidence())
    val value = res.getFinalValue()
    println(s"${value.mean} conf=${value.confidence} [${value.low}, ${value.high}]")
  }
}