package org.eiennohito.spark

import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import ws.kotonoha.akane.analyzers.knp.{LexemeApi, TableApi}
import ws.kotonoha.akane.parser.JumanPosSet

object TariCalculator {
  //this imports oprtations for operating on files
  //this is not a scala standard library, but my own
  import ws.kotonoha.akane.resources.FSPaths._

  //entry point of scala applications are main functions
  //inside objects
  def main(args: Array[String]): Unit = {
    //first thing to do is to create a Spark context
    //it is available by default in a shell as sc
    val conf = new SparkConf().setAppName("TariCalc")
    val ctx = new SparkContext(conf)

    //giving names for input arguments
    val inputFilesHere = args(0)
    val output = args(1)

    //read input filenames from file and put them to a List collection
    val inputFiles = inputFilesHere.p.lines().toList

    //some debungging information
    //you can use variable substitution in Scala by prefixing a
    //string literal with s
    println(s"using ${inputFiles.length} input files: ${inputFiles.headOption}")

    //create inputs
    //mkString method joins all strings in a collection
    //with a provided separator
    val inputs = EosDelimited.readFrom(ctx, inputFiles.mkString(","))

    //two flatmaps create basic data for the further pipeline
    val predArgs = inputs
      //case syntax here is needed to use extraction of variables from a tuple
      //first we need to parse KNP trees from their table representation
      .flatMap { txt => CaseFrameProcessor.parseTree(txt) }
      //then we need to extract triples from those trees
      .flatMap(tree => extractTariInfo(tree))

    //after triples are extracted, the only thing left is doing the usual count aggregation
    val counted = predArgs.map(x => x -> 1L).reduceByKey(_ + _)


    //and finally, we save all data to the output directory, creating at most 50 files
    val numParts = 50 min counted.getNumPartitions
    counted.sortBy({case (a, b) => (-b, a)}, numPartitions = numParts)
      .map{case (a,b) => s"$a\t$b"}
      .saveAsTextFile(output)

    ctx.stop()
  }

  val allTariForms = {
    val conjs = JumanPosSet.default.conjugatons
    val allForms = conjs.flatMap(conjType => conjType.conjugations.flatMap{conjForm  =>
      if (conjForm.name.contains("タリ形")) {
        (conjType.num, conjForm.num) :: Nil
      } else Nil
    })
    allForms.toSet
  }

  def isTari(l: LexemeApi): Boolean = {
    val pos = l.pos
    val form = (pos.category, pos.conjugation)
    allTariForms.contains(form)
  }

  def extractTariInfo(table: TableApi): Iterator[String] = {
    table.bunsetsuIter.filter(_.depType == "P").flatMap { b =>
      val target = table.bunsetsu(b.depNumber)
      val depIsTari = target.lexemeIter.find(l => isTari(l))
      val selfIsTari = b.lexemeIter.find(l => isTari(l))
      (depIsTari, selfIsTari) match {
        case (Some(dep), Some(slf)) =>
          val v1 = b.valueOfFeature("正規化代表表記").getOrElse("")
          val v2 = target.valueOfFeature("正規化代表表記").getOrElse("")
          if (v1.compareTo(v2) > 0)  {
            List(s"$v2\t$v1")
          } else {
            List(s"$v1\t$v2")
          }
        case _ => Nil
      }
    }
  }
}
