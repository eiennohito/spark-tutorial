package org.eiennohito.spark

import java.io.{DataInputStream, InputStream}

import com.google.protobuf.{ByteString, CodedInputStream}
import org.tensorflow.example.example.Example
import org.tensorflow.example.feature.Feature
import ws.kotonoha.akane.vectorization.{CompressionSupport, DictionaryVectorizer}
import ws.kotonoha.tensorflow.hadoop.util

/**
  * @author eiennohito
  * @since 2017/02/03
  */
object TrainingDataReader {
  import ws.kotonoha.akane.resources.FSPaths._

  def main(args: Array[String]): Unit = {
    val filename = args(0).p
    val dictionary = args(1).p

    val words = DictionaryVectorizer.readPairsFromTsv(dictionary, 1000000)
    val restorator = DictionaryVectorizer.restorator(words.map(_._1))

    for (str <- filename.inputStream) {
      val rdr = TFExampleReader.fromStream(CompressionSupport.wrapStream(str, filename))

      for (ex <- rdr) {

        val features = ex.features.get
        val words = features.feature("words").getInt64List.value
        val target = features.feature("answer").getInt64List.value.head

        val tgt = restorator.restoreOne(target)
        val data = restorator.restore(words)
        println(s"$tgt $data")
      }
    }
  }
}


object TFExampleReader {

  def fromStream(stream: InputStream): Iterator[Example] = {
    val input = new DataInputStream(stream)
    val rdr = new util.TFRecordReader(input, true)
    val iter = new TFExampleStream(rdr)

    iter.flatMap { ex =>
      ex.features match {
        case Some(f) =>
          f.feature.get("packed") match {
            case Some(Feature(Feature.Kind.BytesList(bytes))) => new TFExampleIterator(bytes.value)
            case _ => List(ex)
          }
        case None => Nil
      }
    }
  }


  class TFExampleStream(reader: util.TFRecordReader) extends Iterator[Example] {
    private var nextBytes: Array[Byte] = reader.read()

    override def hasNext: Boolean = nextBytes != null

    override def next(): Example = {
      val ex = Example.parseFrom(CodedInputStream.newInstance(nextBytes))
      nextBytes = reader.read()
      ex
    }
  }

  class TFExampleIterator(objs: TraversableOnce[ByteString]) extends Iterator[Example] {
    private val iter = objs.toIterator

    override def hasNext: Boolean = iter.hasNext

    override def next(): Example = {
      Example.parseFrom(CodedInputStream.newInstance(iter.next().newInput()))
    }
  }

}


