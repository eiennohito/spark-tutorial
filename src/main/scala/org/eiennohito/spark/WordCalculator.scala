package org.eiennohito.spark

import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{RangePartitioner, SparkConf, SparkContext}
import ws.kotonoha.akane.analyzers.juman.JumanText
import ws.kotonoha.akane.analyzers.knp.raw.OldAndUglyKnpLexeme

/**
  * @author eiennohito
  * @since 2017/01/04
  */
object WordCalculator {
  import ws.kotonoha.akane.resources.FSPaths._

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Morpheme Counter")

    conf.registerKryoClasses(Array(classOf[String]))

    val input = args(0)
    val output = args(1)

    val syntax = if (args.length < 3) {
      "knp"
    } else args(2)

    val filenames = input.p.lines().filter(_.length > 5).mkString(",")

    println(s"using $filenames as input from $syntax")

    val sc = new SparkContext(conf)

    try {
      doWork(output, filenames, sc, syntax)
    } finally {
      sc.stop()
    }
  }

  private def doWork(output: String, filenames: String, sc: SparkContext, syntax: String) = {
    val inp = sc.textFile(filenames)

    val counts: RDD[(String, Long)] = countKnpFormatMrphs(inp, syntax).filter(_._2 > 5L)

    val c2 = counts.map {case (k, v) => ((v, k(0)), k)}.persist(StorageLevel.MEMORY_AND_DISK_SER)

    c2.sortByKey(ascending = false, numPartitions = 100)
      .map { case ((k, _), v) => s"$k\t$v" }
      .saveAsTextFile(output, classOf[GzipCodec])
  }

  private def partNum(wanted: Int, current: Int): Int = {
    if (current > wanted) wanted else current
  }

  def countKnpFormatMrphs(inp: RDD[String], syntax: String = "knp"): RDD[(String, Long)] = {
    val jumanMorphemes = inp.filter(s => s.length > 10 && "+*#".indexOf(s.charAt(0)) < 0)
    val processed = syntax match {
      case "knp" => jumanMorphemes.flatMap(s => try {
        OldAndUglyKnpLexeme.fromTabFormat(s).canonicForm() -> 1L :: Nil
      } catch {
        case e: Exception =>
          System.err.print(s"could not parse $s")
          e.printStackTrace(System.err)
          Nil
      })
      case "norm" => jumanMorphemes.flatMap(s => try {
        val morph = OldAndUglyKnpLexeme.fromTabFormat(s)
        val form = morph.valueOfFeature("正規化代表表記").getOrElse(morph.canonicForm())
        form -> 1L :: Nil
      } catch {
        case e: Exception =>
          System.err.print(s"could not parse $s")
          e.printStackTrace(System.err)
          Nil
      })
      case "juman" => jumanMorphemes.flatMap { s =>
        val start = if (s.charAt(0) == '@') 2 else 0
        try {
          val line = JumanText.parseLine(s, start, s.length)
          val x = line.features.find(_.key == "代表表記").flatMap(_.value).getOrElse(s"${line.surface}/${line.reading}")
          x -> 1L :: Nil
        } catch {
          case e: Exception =>
            System.err.print(s"could not parse $s")
            e.printStackTrace(System.err)
            Nil
        }
      }
      case _ => throw new Exception(s"unknow syntax: $syntax")
    }
    val sorted = processed.reduceByKey(_ + _, partNum(5000, processed.getNumPartitions))
    sorted
  }
}
