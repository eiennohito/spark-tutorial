package org.eiennohito.spark

import java.io.{BufferedReader, StringReader}
import java.nio.file.Path

import com.google.protobuf.ByteString
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{SparkConf, SparkContext}
import org.rogach.scallop.ScallopConf
import org.tensorflow.example.example.Example
import ws.kotonoha.akane.analyzers.knp.LexemeApi
import ws.kotonoha.akane.analyzers.knp.wire.KnpTable
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.resources.FSPaths
import ws.kotonoha.akane.utils.CalculatingIterator
import ws.kotonoha.akane.vectorization.{DictionaryVectorizer, VectKnpTree}
import ws.kotonoha.spark.tensorflow.{TFExample, TFRecords}

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
  * @author eiennohito
  * @since 2017/01/17
  */
object VectorizeKnp {

  private class VectorizeKnpConf(args: Seq[String]) extends ScallopConf(args) {
    val dictionary = opt[Path](required = true)
    val input = opt[Path](required = true)
    val output = opt[String](required = true)
    val vocabSize = opt[Int](default=Some(10000))
    val feature = opt[String](default=Some("repr"))
    val packSize = opt[Int](default=Some(100))
    val minLength = opt[Int](default=Some(10))
    val maxLength = opt[Int](default=Some(80))
    val maxOutputFiles = opt[Int](default=Some(1000))
    val sortByLength = toggle(default=Some(true))


    private val defHandler = errorMessageHandler

    errorMessageHandler = { msg =>
      this.printHelp()
      defHandler(msg)
    }
  }


  def featureExtractor(s: String): LexemeApi => String = {
    VectKnpTree.feature(s)
  }

  def loadDictionary(dictionary: Path, feature: String, output: String, max: Int = 50000) = {
    import FSPaths._
    val data = DictionaryVectorizer.readPairsFromTsv(dictionary, max)
    val outName = (output + ".dict.csv").p
    outName.ensureParent()

    DictionaryVectorizer.restorator(data.map(_._1)).dump(outName)

    DictionaryVectorizer.fromWordFreqPairs(data, featureExtractor(feature))
  }

  def convert(sc: SparkContext, input: String, output: String, dic: DictionaryVectorizer, args: VectorizeKnpConf) = {
    val dicbcast = sc.broadcast(dic)

    val minLen = args.minLength()
    val maxLen = args.maxLength()

    val empty = sc.longAccumulator("empty")
    val examples = sc.longAccumulator("examples")
    val subsampled = sc.longAccumulator("subsampled")

    val inp = EosDelimited.readFrom(sc, input)
    val converted = inp.mapPartitions({ iter =>
      val parser = new KnpTabFormatParser
      val dic = dicbcast.value
      val vec2 = new KnpVectorizer(dic, new Random(), minLen, maxLen)
      iter.flatMap { b =>
        var s: String = null
        try {
          s = b.toString
          val tree = parser.parse(new BufferedReader(new StringReader(s)))
          val table = tree.get
          val res = vec2.convertOne2(table)
          if (res.isEmpty) {
            empty.add(1)
          }
          examples.add(res.length)
          subsampled.add(vec2.subsampleCnt)
          res
        } catch {
          case e: Exception =>
            System.err.println(s)
            e.printStackTrace()
            Nil
        }
      }
    }, preservesPartitioning = true)

    val numParts = partNum(converted.getNumPartitions, args.maxOutputFiles())

    val partitioned = if (args.sortByLength()) {
      converted.sortByKey(numPartitions = numParts)
    } else {
      converted.coalesce(numParts, shuffle = true)
    }

    val result = partitioned.values

    val pack = args.packSize()

    if (pack > 1) {
      val packed = result.mapPartitions({ iter =>
        new CalculatingIterator[Example] {
          override protected def calculate(): Option[Example] = {
            val buffer = new ArrayBuffer[ByteString](pack)
            var i = 0
            while (iter.hasNext && i < pack) {
              val byte = iter.next().toByteArray
              buffer += ByteString.copyFrom(byte)
              i += 1
            }
            if (buffer.nonEmpty) {
              Some(TFExample("packed" -> TFExample.byteSeq(buffer)))
            } else None
          }
        }
      }, preservesPartitioning = true)
      TFRecords.writeToFile(packed, output, Some(classOf[GzipCodec]))
    } else {
      TFRecords.writeToFile(result, output, Some(classOf[GzipCodec]))
    }

    println(s"total ${examples.count} sentences, ${empty.sum} empty, ${examples.sum} examples, ${subsampled.sum} subsampled")
  }

  def partNum(current: Int, expected: Int): Int = {
    if (current < expected) current else expected
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Knp2Id")
    conf.registerKryoClasses(Array(
      classOf[Example],
      classOf[KnpTable],
      classOf[DictionaryVectorizer],
      classOf[VectorizeKnpConf]
    ))

    val argobj = new VectorizeKnpConf(args)
    argobj.verify()

    val dictionary = argobj.dictionary()
    val input = argobj.input()
    val output = argobj.output()
    val cnt = argobj.vocabSize()
    val feature = argobj.feature()

    import FSPaths._
    val filenames = input.p.lines().filter(_.length > 5).mkString(",")

    val dic = loadDictionary(dictionary, feature, output, cnt)

    println(s"converting $input to indices with feature=$feature, packby=${argobj.packSize}, sort=${argobj.sortByLength}")

    val sc = new SparkContext(conf)

    try {
      convert(sc, filenames, output, dic, argobj)
    } finally {
      sc.stop()
    }
  }
}
