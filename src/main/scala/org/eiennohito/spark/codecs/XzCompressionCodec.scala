package org.eiennohito.spark.codecs

import java.io.InputStream

import org.apache.hadoop.io.compress.{CompressionInputStream, Decompressor, DefaultCodec}
import org.tukaani.xz.XZInputStream

/**
  * @author eiennohito
  * @since 2017/01/26
  */
class XzCompressionCodec extends DefaultCodec {
  override def getDefaultExtension = ".xz"
  override def createInputStream(in: InputStream) = new XzComprInputStream(in)
  override def createInputStream(in: InputStream, decompressor: Decompressor) = new XzComprInputStream(in)
}


class XzComprInputStream(str: InputStream) extends CompressionInputStream(str) {
  private val xzStream = new XZInputStream(str)

  override def resetState() = throw new UnsupportedOperationException

  override def read() = xzStream.read()

  override def read(b: Array[Byte]) = xzStream.read(b)

  override def read(b: Array[Byte], off: Int, len: Int) = xzStream.read(b, off, len)

  override def close() = {
    xzStream.close()
  }

  override def available() = xzStream.available()

  override def mark(readlimit: Int) = xzStream.mark(readlimit)

  override def skip(n: Long) = xzStream.skip(n)

  override def markSupported() = xzStream.markSupported()

  override def reset() = xzStream.reset()
}
