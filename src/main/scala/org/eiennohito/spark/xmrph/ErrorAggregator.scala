package org.eiennohito.spark.xmrph

import java.util
import java.util.Comparator

import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.eiennohito.spark._
import org.rogach.scallop.ScallopConf
import ws.kotonoha.akane.parser.JumanPosSet

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object ErrorAggregator {

  case class ErrorLocation(count: Long, examples: mutable.HashMap[String, Int]) {
    def merge(other: ErrorLocation, maxEx: Int): ErrorLocation = {
      val iter = other.examples.iterator
      while (iter.hasNext) {
        val (key, cnt) = iter.next()
        examples.update(key, examples.getOrElse(key, 0) + cnt)
      }
      ErrorLocation(count + other.count, examples).finish(maxEx * 2)
    }

    def finish(maxEx: Int): ErrorLocation = {
      if (examples.size <= maxEx) {
        return this
      }

      val items = examples.toArray
      util.Arrays.sort(items, new Comparator[(String, Int)] {
        override def compare(o1: (String, Int), o2: (String, Int)): Int = o2._2 - o1._2
      })
      val toTake = items.length - maxEx
      var i = 0
      while (i < toTake) {
        examples.remove(items(i)._1)
        i += 1
      }
      this
    }
  }

  trait Processor {
    def process(item: XMorphDebugItem): Traversable[(String, ErrorLocation)]
  }

  class InconsistentTagProcessor(args: ProcArgs) extends Processor {
    private val posset = JumanPosSet.default
    private val posMap = {
      posset.pos.map { p => p.name -> p.subtypes.map(s => s.name -> s.possibleConjs).toMap}.toMap
    }
    private val conjMap = {
      posset.conjugatons.map { c => c.name -> (c.num, c.conjugations.map(_.name).toSet) }.toMap
    }

    private def tagsEquals(tags1: Array[(String, Float)], tags2: Array[(String, Float)]): Boolean = {
      if (tags1.length != tags2.length) {
        return false
      }
      var i = 1
      while (i < tags1.length) {
        val t1 = tags1(i)
        val t2 = tags2(i)
        if (t1._1 != t2._1) {
          return false
        }
        i += 1
      }
      true
    }

    private def invalidTagCombo(tags: Array[(String, Float)]): Boolean = {
      val (pos, _) = tags(1)
      val (subpos, _) = tags(2)
      val (cType, _) = tags(3)
      val (cForm, _) = tags(4)

      val cidxes = posMap.getOrElse(pos, Map.empty).getOrElse(subpos, null)
      if (cidxes == null) {
        return true
      }

      if (cidxes.isEmpty && cType == "*" && cForm == "*") {
        return false
      }

      conjMap.get(cType) match {
        case None => true
        case Some((idx, subnames)) =>
          !(cidxes.contains(idx) && subnames.contains(cForm))
      }
    }

    def process(item: XMorphDebugItem): Traversable[(String, ErrorLocation)] = {
      val tokens = new ArrayBuffer[String]()
      val errors = new ArrayBuffer[Int]()

      val buffer = new mutable.StringBuilder()

      var curTags = item.chars.head.tags

      var tokIdx = 0

      var chidx = 0
      while (chidx < item.chars.size) {
        val ch = item.chars(chidx)
        val (seg, _) = ch.tags(0)

        if (seg == "B" && buffer.nonEmpty) {
          tokIdx += 1
          tokens += buffer.result()
          curTags = ch.tags
          buffer.clear()
        }

        buffer.append(ch.char)

        if (!tagsEquals(curTags, ch.tags) && (errors.isEmpty || errors.last != tokIdx)) {
          errors += tokIdx
        }

        if (invalidTagCombo(ch.tags) && (errors.isEmpty || errors.last != tokIdx)) {
          errors += tokIdx
        }

        chidx += 1
      }

      if (buffer.nonEmpty) {
        tokens += buffer.result()
        tokIdx += 1
      }

      for (err <- errors) yield {
        val start = (err - 1) max 0
        val end = (err + 2) min tokIdx
        val slice = tokens.slice(start, end)
        val target = tokens(err)
        val sliceStr = slice.mkString("|")
        target -> ErrorLocation(1, mutable.HashMap(sliceStr -> 1))
      }
    }
  }

  class SegProbProcessor(args: ProcArgs) extends Processor {

    def makeTokens(chars: IndexedSeq[XMorphChar]) = {
      val buffer = new ArrayBuffer[(String, Float)]()

      buffer += "<b>" -> 100f

      val surf = new StringBuilder()
      var prob = 100f

      var i = 0
      while (i < chars.length) {
        val ch = chars(i)
        val (tag, tagProb) = ch.tags(0)
        if (tag == "B") {
          if (surf.nonEmpty) {
            buffer += surf.result() -> prob
          }
          surf.clear()
          prob = tagProb
        }
        surf.append(ch.char)
        prob = prob min tagProb
        i += 1
      }
      if (surf.nonEmpty) {
        buffer += surf.result() -> prob
      }

      buffer += "<e>" -> 100f

      buffer
    }

    def process(item: XMorphDebugItem): Traversable[(String, ErrorLocation)] = {
      if (!check(item)) {
        return Nil
      }

      val tokens = makeTokens(item.chars)

      var idx = 1
      val limit = tokens.length - 1
      val result = new ArrayBuffer[(String, ErrorLocation)]()

      while (idx < limit) {
        val (t, tp) = tokens(idx)
        if (tp < args.threshold) {
          val (prev, _) = tokens(idx - 1)
          val (next, _) = tokens(idx + 1)
          result += t -> ErrorLocation(1, mutable.HashMap[String, Int](s"$prev|$t|$next" -> 1))
        }
        idx += 1
      }
      result
    }

    private def check(item: XMorphDebugItem): Boolean = {
      val nchars = item.chars.length
      var i = 0
      while (i < nchars) {
        val ch = item.chars(i)
        val tag = ch.tags(0)
        if (tag._2 < args.threshold) {
          return true
        }
        i += 1
      }
      false
    }
  }

  object Processors {
    def byName(args: ProcArgs) = args.name match {
      case "seg-prob" => new SegProbProcessor(args)
      case "inc" => new InconsistentTagProcessor(args)
    }
  }

  case class ProcArgs(name: String, threshold: Float, maxExamples: Int)

  private class ErrorAggregatorArgs(args: Seq[String]) extends ScallopConf(args) {
    val input = opt[String](required = true)
    val output = opt[String](required = true)
    val numFiles = opt[Int](default = Some(10))
    val maxEntries = opt[Int](default = Some(500))
    val kind = opt[String](default = Some("seg-prob"))
    val probTreshold = opt[Float](default = Some(60f))
    val maxExamples = opt[Int](default = Some(50))

    def procArgs = ProcArgs(kind(), probTreshold(), maxExamples())
  }

  def doWork(sc: SparkContext, args: ErrorAggregatorArgs): Unit = {
    val data = EosDelimited.readFrom(sc, JumanppTokenFreq.resolveInput(args.input()))

    val pargs = args.procArgs

    val maxExs = pargs.maxExamples

    data.mapPartitions { iter =>
      val proc = Processors.byName(pargs)
      iter.flatMap { sent =>
        val data = XMorphDebugParser.parse(sent)
        proc.process(data)
      }
    }.reduceByKey { (a, b) => a.merge(b, maxExs) }
      .map { case (a, b) => ((b.count, a), b) }.persist(StorageLevel.DISK_ONLY)
      .sortByKey(ascending = false, numPartitions = args.numFiles())
      .map { case ((cnt, key), d) => s"$key\t$cnt\t${d.examples.toSeq.sortBy(-_._2).map(_._1).mkString(" ")}" }
      .saveAsTextFile(args.output(), classOf[GzipCodec])
  }

  def main(args: Array[String]): Unit = {
    val cfg = new ErrorAggregatorArgs(args)
    cfg.verify()

    val conf = new SparkConf()
    conf.setAppName("XmrphErrorAggregator")
    conf.registerKryoClasses(Array(
      classOf[ProcArgs],
      classOf[ErrorLocation]
    ))

    val sc = new SparkContext(conf)

    doWork(sc, cfg)
  }
}
