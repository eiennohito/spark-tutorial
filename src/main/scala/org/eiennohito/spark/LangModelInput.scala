package org.eiennohito.spark

import java.io.{BufferedReader, StringReader}
import java.nio.file.Path

import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.{SparkConf, SparkContext}
import org.rogach.scallop.ScallopConf
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.vectorization.DictionaryVectorizer

/**
  * @author eiennohito
  * @since 2017/07/19
  */
object LangModelInput {

  import ws.kotonoha.akane.resources.FSPaths._

  private class LangModelInputConf(args: Seq[String]) extends ScallopConf(args) {
    val dictionary = opt[Path](required = true)
    val input = opt[Path](required = true)
    val output = opt[String](required = true)
    val vocabSize = opt[Int](default=Some(10000))
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("LangModelInput")
    val argobj = new LangModelInputConf(args)
    argobj.verify()

    conf.registerKryoClasses(Array(
      classOf[DictionaryVectorizer]
    ))

    val dic = VectorizeKnp.loadDictionary(
      argobj.dictionary(),
      "surf",
      argobj.output(),
      argobj.vocabSize()
    )

    val sc = new SparkContext(conf)
    val input = argobj.input()
    val filenames = input.p.lines().filter(_.length > 5).mkString(",")
    val inp = EosDelimited.readFrom(sc, filenames)

    val dicbcast = sc.broadcast(dic)

    val data = inp.mapPartitions { it =>
      val parser = new KnpTabFormatParser
      val localDic = dicbcast.value
      it.flatMap { txt =>
        parser.parse(new BufferedReader(new StringReader(txt.toString)))
      }.map { tbl =>
        tbl.lexemeIter.map { l =>
          if (localDic.idFor(l.surface).isDefined) {
            l.surface
          } else {
            "JPP_UNK"
          }
        }.mkString(" ")
      }
    }

    val numParts = VectorizeKnp.partNum(data.getNumPartitions, 200)

    data.coalesce(numParts, shuffle = false).saveAsTextFile(argobj.output(), classOf[GzipCodec])
  }
}
