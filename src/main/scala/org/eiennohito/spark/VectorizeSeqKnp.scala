package org.eiennohito.spark

import java.io.{BufferedReader, StringReader}
import java.nio.file.Path

import org.apache.hadoop.io.compress.GzipCodec
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{SparkConf, SparkContext}
import org.rogach.scallop.ScallopConf
import org.tensorflow.example.example.Example
import ws.kotonoha.akane.analyzers.knp.LexemeApi
import ws.kotonoha.akane.analyzers.knp.wire.KnpTable
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.resources.FSPaths
import ws.kotonoha.akane.vectorization.{DictionaryVectorizer, VectKnpTree}
import ws.kotonoha.spark.tensorflow.TFRecords

import scala.util.Random

/**
  * @author eiennohito
  * @since 2017/01/17
  */
object VectorizeSeqKnp {

  private class VectorizeKnpConf(args: Seq[String]) extends ScallopConf(args) {
    val dictionary = opt[Path](required = true)
    val input = opt[Path](required = true)
    val output = opt[String](required = true)
    val fullVocab = opt[Int](default=Some(10000))
    val vocabSize = opt[Int](default=Some(10000))
    val feature = opt[String](default=Some("repr"))
    val sampleSize = opt[Long](default=Some(-1))
    val minLength = opt[Int](default=Some(15))
    val maxLength = opt[Int](default=Some(70))
    val maxOutputFiles = opt[Int](default=Some(1000))
    val sortByLength = toggle(default=Some(true))


    private val defHandler = errorMessageHandler

    errorMessageHandler = { msg =>
      this.printHelp()
      defHandler(msg)
    }
  }


  def featureExtractor(s: String): LexemeApi => String = {
    VectKnpTree.feature(s)
  }

  def loadDictionary(dictionary: Path, feature: String, output: String, maxAny: Int = 10000, max: Int = 50000, save: Boolean = true) = {
    import FSPaths._
    val data = DictionaryVectorizer.readTsv2(dictionary, maxAny, max)
    val outName = (output + ".dict.csv").p
    outName.ensureParent()

    if (save) {
      DictionaryVectorizer.restorator(data.map(_._1)).dump(outName)
    }

    DictionaryVectorizer.fromWordFreqPairs(data, featureExtractor(feature))
  }

  def convert(sc: SparkContext, input: String, output: String, dic: DictionaryVectorizer, args: VectorizeKnpConf) = {
    val dicbcast = sc.broadcast(dic)

    val minLen = args.minLength()
    val maxLen = args.maxLength()

    val empty = sc.longAccumulator("empty")
    val examples = sc.longAccumulator("examples")
    val subsampled = sc.longAccumulator("subsampled")

    val inp = EosDelimited.readFrom(sc, input)
    val useLength = args.sortByLength()
    val converted = inp.mapPartitions({ iter =>
      val parser = new KnpTabFormatParser
      val dic = dicbcast.value
      val vec2 = new KnpSeqVectorizer(dic, minLen, maxLen, new Random(0xDEADBEEF), 10)
      iter.flatMap { b =>
        var s: String = null
        try {
          s = b.toString
          val tree = parser.parse(new BufferedReader(new StringReader(s)))
          val table = tree.get
          val exs = vec2.makeExamples(table)
          val res = vec2.makeTfExamples(exs, useLength)
          if (res.isEmpty) {
            empty.add(1)
          }
          examples.add(res.length)
          subsampled.add(vec2.subsampled)
          res
        } catch {
          case e: Exception =>
            System.err.println(s)
            e.printStackTrace()
            Nil
        }
      }
    }, preservesPartitioning = true)

    val numParts = partNum(converted.getNumPartitions, args.maxOutputFiles())

    val partitioned = converted
      .persist(StorageLevel.MEMORY_AND_DISK)
      .sortByKey(numPartitions = numParts)

    val result = partitioned.values

    val sample = args.sampleSize()

    if (sample > 1) {
      val count = result.countApprox(10000)
      val ratio = sample.toDouble / count.getFinalValue().mean
      print(s"Sampling ${ratio*100}%: $sample of $count (${count.getFinalValue().confidence} conf)")
      val packed = result.sample(withReplacement = false, ratio)
      TFRecords.writeToFile(packed, output, Some(classOf[GzipCodec]))
    } else {
      TFRecords.writeToFile(result, output, Some(classOf[GzipCodec]))
    }

    val status = s"total ${examples.count} sentences, ${empty.sum} empty, ${examples.sum} examples, ${subsampled.sum} subsampled"
    println(status)
    status
  }

  def partNum(current: Int, expected: Int): Int = {
    if (current < expected) current else expected
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("Knp2Id")
    conf.registerKryoClasses(Array(
      classOf[Example],
      classOf[KnpTable],
      classOf[DictionaryVectorizer],
      classOf[VectorizeKnpConf]
    ))

    val argobj = new VectorizeKnpConf(args)
    argobj.verify()

    val dictionary = argobj.dictionary()
    val input = argobj.input()
    val output = argobj.output()
    val cnt = argobj.vocabSize()
    val cnt0 = argobj.fullVocab() min cnt
    val feature = argobj.feature()

    import FSPaths._
    val filenames = input.p.lines().filter(_.length > 5).mkString(",")

    val dic = loadDictionary(dictionary, feature, output, cnt0, cnt)

    println(s"converting $input to indices with feature=$feature, packby=${argobj.sampleSize}, sort=${argobj.sortByLength}")

    val sc = new SparkContext(conf)

    try {
      val status = convert(sc, filenames, output, dic, argobj)
      (output + ".stats").p.writeStrings(Seq(status), "\n")
    } finally {
      sc.stop()
    }
  }
}

