package org.eiennohito.spark

import org.tensorflow.example.feature.Feature.Kind
import ws.kotonoha.akane.vectorization.CompressionSupport

import scala.collection.mutable


/**
  * @author eiennohito
  * @since 2017/01/25
  */
object LengthChecker {

  import ws.kotonoha.akane.resources.FSPaths._

  private def checkLength(f: String) = {
    val path = f.p
    for {
      is <- path.inputStream
    } {
      val cnts = new mutable.HashMap[Int, Int]()
      val input = TFExampleReader.fromStream(CompressionSupport.wrapStream(is, path))

      while (input.hasNext) {
        val ex = input.next()

        val fmap = ex.features.get.feature

        fmap.get("words").map(_.kind) match {
          case Some(Kind.Int64List(v)) =>
            val len = v.value.length
            cnts.put(len, cnts.getOrElse(len, 0) + 1)
          case x => throw new Exception(s"unknown elem of words: $x")
        }
      }

      val sorted = cnts.toSeq.sortBy(-_._2)
      println(s"stats of $f")
      for ((num, cnt) <- sorted) {
        println(s"$num\t$cnt")
      }
    }
  }

  def main(args: Array[String]): Unit = {
    for (f <- args) {
      try {
        checkLength(f)
      } catch {
        case e: Exception => e.printStackTrace()
      }
    }
  }
}
