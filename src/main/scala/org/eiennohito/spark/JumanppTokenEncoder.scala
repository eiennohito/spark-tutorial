package org.eiennohito.spark

import java.lang

import ws.kotonoha.akane.parser.JumanPosSet
import ws.kotonoha.akane.vectorization.SentenceToken

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object JumanppTokenEncoder {

  private val posset = JumanPosSet.default

  private val posById = posset.pos.map(x => x.name)
  private val posByName = posById.zipWithIndex.toMap
  private val subposById = posset.pos.flatMap(_.subtypes.map(_.name)).distinct
  private val subposByName = subposById.zipWithIndex.toMap
  private val ctypeById = posset.conjugatons.map(_.name)
  private val ctypeByName = ctypeById.zipWithIndex.toMap
  private val cformById = posset.conjugatons.flatMap(_.conjugations.map(_.name)).distinct
  private val cformByName = cformById.zipWithIndex.toMap

  private val (posByTpl, posTpls) = {
    val to = new mutable.HashMap[(String, String, String, String), Int]()
    val from = new mutable.HashMap[Int, (String, String, String, String)]()

    val items = new ArrayBuffer[(String, String, String, String)]()

    for {
      pos <- posset.pos
      subpos <- pos.subtypes
      ctid <- if (subpos.possibleConjs.length > 0) subpos.possibleConjs else Array(0)
      ctype = posset.conjugatons(ctid)
      cform <- ctype.conjugations
    } {
      items += ((ctype.name, pos.name, subpos.name, cform.name))
    }

    val sortedItems = items.sorted
    for ((x, idx) <- sortedItems.zipWithIndex) {
      to += x -> idx
    }
    (to, sortedItems)
  }

  private def encodeTag(str: String, stringToInt: Map[String, Int], builder: java.lang.StringBuilder): Unit = {
    stringToInt.get(str) match {
      case Some(i) if i < 123 =>
        builder.append((0x3 + i).toChar)
      case _ =>
        builder.append(0x2.toChar)
        builder.append(str)
    }
  }

  private def encodeTags(tuple: (String, String, String, String), bldr: lang.StringBuilder) = {
    posByTpl.get(tuple) match {
      case Some(id) => bldr.append((id + 0x3).toChar)
      case _ =>
        val (a, b, c, d) = tuple
        bldr.append(0x2.toChar)
        bldr.append(a)
        bldr.append(0x2.toChar)
        bldr.append(b)
        bldr.append(0x2.toChar)
        bldr.append(c)
        bldr.append(0x2.toChar)
        bldr.append(d)
    }
  }

  def encode(t: SentenceToken): String = {
    val surf = t.tokens(0)
    val read = t.tokens(1)
    val dicForm = t.tokens(2)
    val pos = t.tokens(3)
    val subpos = t.tokens(4)
    val ctype = t.tokens(5)
    val cform = t.tokens(6)

    val bldr = new java.lang.StringBuilder()
    bldr.append(surf)
    if (read.equals(surf)) {
      bldr.append(0x1.toChar)
    } else {
      bldr.append(0x2.toChar)
      bldr.append(read)
    }
    if (dicForm.equals(surf)) {
      bldr.append(0x1.toChar)
    } else {
      bldr.append(0x2.toChar)
      bldr.append(dicForm)
    }

    //    encodeTag(pos, posByName, bldr)
    //    encodeTag(subpos, subposByName, bldr)
    //    encodeTag(ctype, ctypeByName, bldr)
    //    encodeTag(cform, cformByName, bldr)

    encodeTags((ctype, pos, subpos, cform), bldr)

    bldr.toString
  }

  private def decodeTags(data: String, from: Int, target: Array[String], pos: Int) = {
    data.charAt(from) match {
      case 0x2 =>
        var idx = decodeOne(data, from, null, "", target, pos + 2)
        idx = decodeOne(data, idx, null, "", target, pos + 0)
        idx = decodeOne(data, idx, null, "", target, pos + 1)
        decodeOne(data, idx, null, "", target, pos + 3, 0)
      case code =>
        val (a, b, c, d) = posTpls(code - 3)
        target(pos + 2) = a
        target(pos + 0) = b
        target(pos + 1) = c
        target(pos + 3) = d
    }
  }

  private def decodeOne(data: String, from: Int, codes: Array[String], surf: String, target: Array[String], pos: Int, deadzone: Int = 1): Int = {
    data.charAt(from) match {
      case 0x1 =>
        target(pos) = surf
        from + 1
      case 0x2 =>
        var idx = from + 1
        val maxLen = data.length - deadzone
        while (idx < maxLen && data.charAt(idx) > 0x2) {
          idx += 1
        }
        target(pos) = data.substring(from + 1, idx)
        idx
      case code =>
        target(pos) = codes(code - 3)
        from + 1
    }
  }

  def decode(str: String): SentenceToken = {
    var idx = 0
    while (idx < str.length && str.charAt(idx) > 0x2) {
      idx += 1
    }
    val surf = str.substring(0, idx)
    val result = new Array[String](7)
    result(0) = surf
    idx = decodeOne(str, idx, null, surf, result, 1)
    idx = decodeOne(str, idx, null, surf, result, 2)
    //    idx = decodeOne(str, idx, posById, surf, result, 3)
    //    idx = decodeOne(str, idx, subposById, surf, result, 4)
    //    idx = decodeOne(str, idx, ctypeById, surf, result, 5)
    //    idx = decodeOne(str, idx, cformById, surf, result, 6)
    decodeTags(str, idx, result, 3)
    SentenceToken(result)
  }
}
