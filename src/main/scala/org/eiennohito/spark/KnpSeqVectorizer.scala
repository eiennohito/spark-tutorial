package org.eiennohito.spark

import org.tensorflow.example.example.Example
import ws.kotonoha.akane.analyzers.knp.raw.{OldAndUglyBunsetsu, OldAngUglyKnpTable}
import ws.kotonoha.akane.checkers.JumanPosCheckers
import ws.kotonoha.akane.vectorization.DictionaryVectorizer
import ws.kotonoha.spark.tensorflow.TFExample

import scala.collection.mutable.ArrayBuffer
import scala.util.Random
import scala.util.hashing.MurmurHash3

case class SeqClozeExample(
  ctxCharLen: Int,
  context: Seq[Long],
  cloze: Seq[Long]
)

case class AnnotatedBunsetsu(
  table: OldAngUglyKnpTable,
  bstIdx: Int,
  vec: DictionaryVectorizer
) {
  def bunsetsu: OldAndUglyBunsetsu = table.bunsetsu(bstIdx)

  val shouldKeep: Float = {
    var sum = 0.0f
    var cnt = 0
    var hadUnk = false
    for (l <- bunsetsu.lexemeIter) {
      val lid = vec.id(l)
      if (lid < DictionaryVectorizer.offset) {
        hadUnk = true
      }
      sum += vec.shouldKeep(lid)
      cnt += 1
    }
    if (hadUnk) {
      0.01f // keep 1% of unks
    } else if (cnt == 0) {
      0
    } else {
      sum / cnt
    }
  }

  val ids = bunsetsu.lexemeIter.flatMap(l => vec.vecOne(l)).toVector

  val charLen = bunsetsu.lexemeIter.map(_.surface.length).sum

  val hasPunct = bunsetsu.lexemeIter.exists(AnnotatedBunsetsu.punct.check)
}

object AnnotatedBunsetsu {
  private val checkers = JumanPosCheckers.default
  val punct = checkers.posSubPos("特殊","記号")
}

class KnpSeqVectorizer(
  dic: DictionaryVectorizer,
  minLen: Int = 15,
  maxLen: Int = 80,
  rng: Random = new Random(0xDEADBEEF),
  maxClozeSize: Int = 12
) {

  private val keyer = new VectorizerKeyer(minLen, maxLen * 10 + 5)
  var subsampled = 0

  def make(buffer: ArrayBuffer[AnnotatedBunsetsu], from: Int, cnt: Int): SeqClozeExample = {
    val ctx = ArrayBuffer[Long]()
    val cloze = ArrayBuffer[Long]()

    val to = from + cnt

    ctx += DictionaryVectorizer.bos
    cloze += DictionaryVectorizer.bos
    var ctxCharLen = 0

    var idx = 0
    while (idx < buffer.length) {
      if (idx >= from && idx < to) {
        if (ctx.isEmpty || ctx.last != DictionaryVectorizer.question) {
          ctx += DictionaryVectorizer.question
        }
        val bnst = buffer(idx)
        val lastLex = bnst.bunsetsu.lexeme(bnst.bunsetsu.lexemeStart + bnst.bunsetsu.lexemeCnt - 1)
        if (AnnotatedBunsetsu.punct.check(lastLex)) {
          cloze ++= bnst.ids.slice(0, bnst.ids.length - 1)
          ctx += bnst.ids.last
        } else {
          cloze ++= bnst.ids
        }
      } else {
        ctx ++= buffer(idx).ids
        ctxCharLen += buffer(idx).charLen
      }
      idx += 1
    }

    ctx += DictionaryVectorizer.eos
    cloze += DictionaryVectorizer.eos

    SeqClozeExample(ctxCharLen, ctx, cloze)
  }

  def makeExamples(ex: OldAngUglyKnpTable): Seq[SeqClozeExample] = {
    subsampled = 0

    var i = 0
    val buffer = new ArrayBuffer[AnnotatedBunsetsu]()
    while (i < ex.bunsetsuCnt) {
      buffer += AnnotatedBunsetsu(ex, i, dic)
      i += 1
    }

    val result = new ArrayBuffer[SeqClozeExample]()

    i = 0
    while (i < buffer.length) {
      // 1) check the bunsetsu only
      val bnst = buffer(i)

      if (rng.nextFloat() <= bnst.shouldKeep) {
        val example = make(buffer, i, 1)
        val exLen = example.context.length
        if (exLen >= minLen && exLen <= maxLen) {
          result += example
        }
      } else {
        subsampled += 1
      }

      if (i > 0) {
        val prevBnst = buffer(i - 1)
        if (prevBnst.bunsetsu.depNumber == bnst.bunsetsu.number && !prevBnst.hasPunct) {
          val border = Math.max(prevBnst.shouldKeep, bnst.shouldKeep)
          if (rng.nextFloat() <= border) {
            val example = make(buffer, i - 1, 2)
            val exLen = example.context.length
            if (exLen >= minLen && exLen <= maxLen) {
              result += example
            }
          } else {
            subsampled += 1
          }
        }
      }

      i += 1
    }

    result.filter(_.cloze.size <= maxClozeSize)
  }

  def makeTfExamples(cex: Seq[SeqClozeExample], useLength: Boolean): Seq[((Int, Int), Example)] = {
    cex.map { cx =>
      val ctxLen = cx.context.length
      val shaped = if (useLength) Math.max(ctxLen + rng.nextInt(5) - 2, minLen) else minLen
      val key = keyer.keyFor(shaped, SeqHashing.hashSeq(cx.context) ^ SeqHashing.hashSeq(cx.cloze))
      key -> TFExample(
        "context" -> TFExample.i64seq(cx.context),
        "cloze" -> TFExample.i64seq(cx.cloze),
        "contextLength" -> TFExample.i64(cx.context.size),
        "clozeLength" -> TFExample.i64(cx.cloze.length)
      )
    }
  }


}
