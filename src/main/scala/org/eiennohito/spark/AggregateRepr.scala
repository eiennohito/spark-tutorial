package org.eiennohito.spark

import java.io.{BufferedReader, InputStream, InputStreamReader}
import java.nio.file.Path
import java.util.zip.GZIPInputStream

import ws.kotonoha.akane.analyzers.knp.raw.OldAndUglyKnpLexeme
import ws.kotonoha.akane.io.Charsets
import ws.kotonoha.akane.resources.FSPaths

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * @author eiennohito
  * @since 2017/01/27
  */
object AggregateRepr {
  import ws.kotonoha.akane.resources.FSPaths._
  import scala.concurrent.duration._

  implicit val ec: ExecutionContext = ExecutionContext.global

  def maybeWrapStream(path: Path, input: InputStream): InputStream = {
    path.extension match {
      case "gz" => new GZIPInputStream(input)
      case _ => input
    }
  }

  private def toPairs(f: Path): Future[Seq[(String, Long)]] = Future {

    val buffer = new ArrayBuffer[(String, Long)]()

    for {
      inp <- f.inputStream
    } {
      val wrapped = maybeWrapStream(f, inp)
      val rdr = new BufferedReader(new InputStreamReader(wrapped, Charsets.utf8))

      var line: String = null

      while ({
        line = rdr.readLine()
        line != null
      }) parseLine(buffer, line)

    }

    println(s"file $f is finished")
    buffer
  }

  private def parseLine(buffer: ArrayBuffer[(String, Long)], line: String): Unit = {
    line.split("\t") match {
      case Array(a, b) =>
        val cnt = a.toLong
        try {
          val morph = OldAndUglyKnpLexeme.fromTabFormat(b)

          morph.tags.find(s => s.startsWith("正規化代表表記:")) match {
            case Some(s) =>
              buffer += s.substring(8) -> cnt
              return
            case _ =>
          }

          buffer += morph.canonicForm() -> cnt
        } catch {
          case e: Exception =>
        }
      case _ =>
    }

  }

  def main(args: Array[String]): Unit = {
    val folder = args(0).p

    val regex = "^.*\\.gz$".r
    val files = FSPaths.find(folder, 1) { (p, _) => regex.pattern.matcher(p.getFileName.toString).matches() }.obj.toVector

    val counts = Map.empty[String, Long]

    val foldedF = Future.fold(files.map(f => toPairs(f)))(counts) { (cur, elems) =>
      var smt = cur
      elems.foreach { case (key, cnt) => smt = smt.updated(key, smt.getOrElse(key, 0L) + cnt) }
      smt
    }

    val result = Await.result(foldedF, 10.minutes)
    val ordered = result.toSeq.sortBy(-_._2)

    args(1).p.writeLines(ordered.take(1000000).map { case (a, b) => s"$a\t$b"})
  }
}
