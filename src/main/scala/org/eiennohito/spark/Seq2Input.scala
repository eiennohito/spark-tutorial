package org.eiennohito.spark

import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import java.nio.charset.StandardCharsets
import java.nio.file.Path

import org.rogach.scallop.ScallopConf
import ws.kotonoha.akane.analyzers.knp.raw.OldAngUglyKnpTable
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.utils.DelimetedIterator
import ws.kotonoha.akane.vectorization.{DictionaryVectorizer, VectKnpTree}

import scala.collection.mutable.ArrayBuffer

object Seq2Input {

  import ws.kotonoha.akane.resources.FSPaths._

  private class Seq2InpArgs(args: Seq[String]) extends ScallopConf(args) {
    val dictionary = opt[Path](required = true)
    val input = opt[Path](required = true)
    val output = opt[String](required = true)
    val fullVocab = opt[Int](default = Some(10000))
    val vocabSize = opt[Int](default = Some(10000))
    val feature = opt[String](default = Some("repr"))
    val target = opt[String](required = true)
  }

  def main(args: Array[String]): Unit = {
    val a = new Seq2InpArgs(args)
    a.verify()

    val data = DictionaryVectorizer.readTsv2(a.dictionary(), a.fullVocab(), a.vocabSize())
    val restorator = DictionaryVectorizer.restorator(data.map(_._1))
    val featurizer = VectKnpTree.feature(a.feature())
    val vectorizer = DictionaryVectorizer.fromWordFreqPairs(data, featurizer)

    val targetId = vectorizer.idFor(a.target()).get


    for {
      is <- a.input().inputStream
      os <- a.output().p.outputStream()
      isr <- new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)).res
      osw <- new PrintWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8)).res
    } {
      val parser = new KnpTabFormatParser
      var p: OldAngUglyKnpTable = null

      val context = new ArrayBuffer[Long]()
      val cloze = new ArrayBuffer[Long]()
      val di = new DelimetedIterator(isr, "EOS")

      while ({p = parser.parse(di); di.head != null}) {
        val tree = p

        context.clear()
        cloze.clear()

        context += DictionaryVectorizer.bos
        cloze += DictionaryVectorizer.bos

        val bIter = tree.bunsetsuIter
        while (bIter.hasNext) {
          val bnst = bIter.next()

          val ids = vectorizer.vectorize(bnst.lexemeIter, tokens = false)
          if (ids.contains(targetId)) {
            cloze ++= ids
            context += DictionaryVectorizer.question
          } else {
            context ++= ids
          }
        }

        context += DictionaryVectorizer.eos
        cloze += DictionaryVectorizer.eos

        restorator.restoreTo(context, osw)
        osw.append('\t')
        restorator.restoreTo(cloze, osw)
        osw.append('\n')

        if (di.head == "EOS") {
          di.next()
        }
      }
    }
  }
}
