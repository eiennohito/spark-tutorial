#!/usr/bin/env python3

import sys
import tensorflow as tf
import numpy as np
import os.path

if len(sys.argv) == 2:
    ckpt_fpath = sys.argv[1]
else:
    print('Usage: python count_ckpt_param.py path-to-ckpt')
    sys.exit(1)

snap_file = os.path.join(ckpt_fpath, "checkpoint")
if os.path.isdir(ckpt_fpath) and os.path.exists(snap_file):
    snap_state = tf.train.get_checkpoint_state(ckpt_fpath)
    ckpt_fpath = snap_state.model_checkpoint_path

# Open TensorFlow ckpt
reader = tf.train.NewCheckpointReader(ckpt_fpath)

print('\nCount the number of parameters in ckpt file(%s)' % ckpt_fpath)
param_map = reader.get_variable_to_shape_map()

params = []

total_count = 0
for k, v in param_map.items():
    if "/Adam" in k:
        continue
    temp = np.prod(v)
    total_count += temp
    params.append((temp, k, str(v)))

params.sort()

for num, name, size in params:
    print(f"{name}: {size} => {num}")

print('Total Param Count: %d' % total_count)
