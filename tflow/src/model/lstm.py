import tensorflow as tf

import lstm_cell


class LstmModel(object):
    def __init__(self, args, scope=tf.VariableScope(name="rnn", reuse=False)):
        self.input_size = args.vec_size
        cell_size = args.cell_size
        if cell_size is None:
            cell_size = self.input_size
        elif cell_size < self.input_size:
            print("cell_size (%d) must be greater or equal to input_size (%d)" % (cell_size, self.input_size))
            exit(1)

        self.cell_size = cell_size
        self.scope = scope

        self.cell = None
        self.cell_obj = lstm_cell.DynamicRnnLstm(
            self, self.scope, args.dtype
        )

        self.cell_obj.init()

    def executor(self, embed, args):
        return ModelExecutor(self, embed, args)

    def embedding(self, args):
        return Embedding(self, args)

    def loss_evaluator(self, embed, args):
        return LossEvaluator(self, embed, args)

    def projecting(self):
        return self.cell_size != self.input_size


class Embedding(object):
    def __init__(self, model, args):
        self.model = model
        self.vocabulary_size = args.vocabulary

        if args.l2_norm:
            self.emb_norm = 1.0
        else:
            self.emb_norm = None

        with tf.variable_scope(model.scope):
            self.embedding = tf.get_variable(
                name="embedding",
                shape=[self.vocabulary_size, model.input_size],
                dtype=args.dtype,
                initializer=tf.random_uniform_initializer(
                    minval=-1 / self.vocabulary_size,
                    maxval=1 / self.vocabulary_size,
                    seed=args.seed,
                    dtype=args.dtype
                )
            )

            if args.softmax_bias:
                self.softmax_bias = tf.get_variable(
                    name="softmax_bias",
                    shape=[self.vocabulary_size],
                    dtype=args.dtype,
                    initializer=tf.zeros_initializer(dtype=args.dtype)
                )
            else:
                self.softmax_bias = tf.zeros([self.vocabulary_size], args.dtype)

            if self.model.projecting():
                self.project_matrix = tf.get_variable(
                    name="projection_weights",
                    shape=[self.model.cell_size, self.model.input_size],
                    dtype=args.dtype
                )

                self.project_bias = tf.get_variable(
                    name="projection_bias",
                    shape=[self.model.input_size],
                    dtype=args.dtype,
                    initializer=tf.zeros_initializer(dtype=args.dtype)
                )

    def embed(self, indices):
        value = tf.nn.embedding_lookup(
            params=self.embedding,
            ids=indices,
            name="embed_input",
            max_norm=self.emb_norm
        )

        return value

    def softmax(self, state):
        return tf.matmul(state, self.embedding, transpose_b=True) + self.softmax_bias


class ExecArgs(object):
    def __init__(self, dtype, l2_norm, batch_size, relu, dropout, seed):
        self.dtype = dtype
        self.l2_norm = l2_norm
        self.batch_size = batch_size
        self.relu = relu
        self.dropout = dropout
        self.seed = seed


class ModelExecutor(object):
    def __init__(self, model, embed, args):
        self.model = model
        self.batch_size = args.batch_size
        self.dtype = args.dtype
        self.embed = embed
        self.relu = args.relu
        self.dropout = args.dropout
        self.seed = args.seed

    def raw_rnn_pass(self, inputs, input_len):
        return self.model.cell_obj.forward_raw(inputs, input_len)

    def rnn_forward(self, inputs, input_len):
        outputs, state = self.raw_rnn_pass(inputs, input_len)
        return self.last_state(input_len, outputs, state)

    def last_state(self, input_len, outputs, state):
        # indices = tf.stack(
        #     values=[
        #         input_len - 1,
        #         tf.range(0, self.batch_size, dtype=tf.int64)
        #     ],
        #     axis=1
        # )
        # gathered = tf.gather_nd(params=outputs, indices=indices)
        gathered = state.h

        if self.dropout is not None:
            gathered = tf.nn.dropout(gathered, self.dropout, seed=self.seed)

        if self.relu:
            gathered = tf.nn.relu(gathered)

        if self.model.projecting():
            gathered = tf.matmul(gathered, self.embed.project_matrix) + self.embed.project_bias

        return gathered


class LossEvaluator(object):
    def __init__(self, model, embed, args):
        self.softmax_sample = args.softmax_sample
        self.model = model
        self.embed = embed
        self.seed = args.seed
        self.use_full_softmax = args.softmax_sample >= (args.vocabulary / 2)
        print("using full softmax=%s" % self.use_full_softmax)

    def loss(self, summaries, labels):

        embeds = self.embed.embedding

        if self.use_full_softmax:
            logits = self.embed.softmax(summaries)
            losses = tf.nn.sparse_softmax_cross_entropy_with_logits(
                logits=logits,
                labels=tf.squeeze(labels),
                name="full_softmax"
            )
        else:
            losses = tf.nn.sampled_softmax_loss(
                weights=embeds,
                biases=self.embed.softmax_bias,
                labels=labels,
                inputs=summaries,
                num_sampled=self.softmax_sample,
                num_classes=self.embed.vocabulary_size,
                name="sparse_softmax"
            )

        return tf.reduce_mean(losses)
