import tensorflow as tf
import numpy as np


class Dictionary(object):
    def __init__(self, word2id, words, unk=0):
        self.word2id = word2id
        self.words = words
        self.unk = unk
        if self.unk == 0:
            self.unk = word2id.get('UNK', unk)

    def vectorize(self, sentence):
        words = sentence.split(' ')
        ids = [self.word2id.get(w.strip(), self.unk) for w in words]
        return ids

    def wordize(self, ids):
        words = [self.words[id] for id in ids]
        return words

    def nwords(self):
        return len(self.words)

    def __repr__(self):
        return "Dict: %d words, UNK=%d, $$$=%d" % (self.nwords(), self.word2id["UNK"], self.word2id["$$$"])


def parse_dictionary(filename, unk=0):
    cnt = 0
    vocab = []
    word2id = {}
    with open(filename, "r") as f:
        for line in f:
            if len(line) > 2:
                word = line.strip()
                word2id[word] = cnt
                vocab.append(word)
                cnt += 1

    return Dictionary(word2id, vocab, unk)


def validation_data(path, vecdict):
    data = []
    with open(path, "r") as f:
        for line in f:
            if len(line) > 10 and not line.startswith("#"):
                ids = vecdict.vectorize(line.strip())
                answer = ids[0]
                question = ids[1:]
                length = len(question)
                data.append((answer, question, length))

    answers, questions, lengths = zip(*data)
    maxlen = np.max(lengths)

    padded = [np.pad(q, [0, maxlen - len(q)], 'constant') for q in questions]

    return (
        tf.constant(np.asarray(answers)),
        tf.constant(np.asarray(padded)),
        tf.constant(np.asarray(lengths))
    )


class Validator(object):
    def __init__(self, args, model, embed, data):
        self.model = model

        self.answers, self.questions, self.lengths = data
        self.questions = tf.transpose(self.questions)
        import lstm

        batch_size = tf.shape(self.lengths)[0]
        args = lstm.ExecArgs(
            dtype=args.dtype,
            l2_norm=args.l2_norm,
            batch_size=batch_size,
            relu=args.relu,
            dropout=None,
            seed=args.seed
        )

        runner = model.executor(embed, args)
        embedded = embed.embed(self.questions)
        states = runner.rnn_forward(embedded, self.lengths)
        scores = embed.softmax(states)
        in_100 = tf.nn.in_top_k(scores, self.answers, 100)
        in_30 = tf.nn.in_top_k(scores, self.answers, 30)
        in_10 = tf.nn.in_top_k(scores, self.answers, 10)

        num_items = tf.to_float(batch_size)
        ratio_100 = tf.reduce_sum(tf.to_float(in_100)) / num_items
        ratio_30 = tf.reduce_sum(tf.to_float(in_30)) / num_items
        ratio_10 = tf.reduce_sum(tf.to_float(in_10)) / num_items

        tf.summary.scalar("valid/in_100", ratio_100)
        tf.summary.scalar("valid/in_30", ratio_30)
        tf.summary.scalar("valid/in_10", ratio_10)

        print("initialized validation")

        self.ins = [ratio_10, ratio_30, ratio_100]
