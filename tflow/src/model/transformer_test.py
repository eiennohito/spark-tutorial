from .transformer import param_swish
import tensorflow as tf
import numpy as np


class SwishTest(tf.test.TestCase):
    def test_swishgrad(self):
        with self.test_session() as sess:
            data = tf.random_uniform([10, 10], -5, 5)
            vals = param_swish(data)
            sess.run(tf.global_variables_initializer())
            theo, num = tf.test.compute_gradient(data, [10, 10], vals, [10, 10])
            self.assertAllClose(num, theo, rtol=1e-3)
