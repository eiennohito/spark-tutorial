import tensorflow as tf
import numpy as np

import lstm


class Trial(object):
    @staticmethod
    def load_files(args, dict):
        check_sents = []
        with open(args.online_check_sentences, "r") as f:
            for sent in f:
                if len(sent) > 5 and not sent.startswith("#"):
                    ids = dict.vectorize(sent)
                    check_sents.append((sent, ids))

        return check_sents

    def __init__(self, model, args):
        self.check_sents = Trial.load_files(args, model.debug.dict)
        self.batch_size = args.online_check_cnt
        self.maxn = args.online_check_maxn
        self.last_sentence = 0
        self.dict = model.debug.dict

        with tf.name_scope("online_eval"):
            # pipeline
            self.input = tf.placeholder(dtype=tf.int64, shape=[self.batch_size, None])
            self.lengths = tf.placeholder(dtype=tf.int64, shape=[self.batch_size])

            my_args = lstm.ExecArgs(
                dtype=args.dtype,
                l2_norm=args.l2_norm,
                batch_size=self.batch_size,
                relu=args.relu,
                dropout=None,
                seed=args.seed
            )
            self.runner = model.model.executor(model.embed, my_args)
            embeds = model.embed.embed(tf.transpose(self.input))
            state = self.runner.rnn_forward(embeds, self.lengths)
            l2_embeds = tf.nn.l2_normalize(model.embed.embedding, dim=1)
            scores = tf.matmul(state, l2_embeds, transpose_b=True) + model.embed.softmax_bias
            self.top_scores, self.top_idx = tf.nn.top_k(scores, k=self.maxn)

    def do_eval(self, sess):
        inputs_orig, inputs = self.next_batch()
        lens = np.asarray([len(x) for x in inputs], dtype=np.int64)
        maxlen = np.max(lens)
        inputs = np.asarray([np.pad(i, (0, maxlen - len(i)), 'constant') for i in inputs])
        scores, idxs = sess.run([self.top_scores, self.top_idx], feed_dict={
            self.input: inputs,
            self.lengths: lens
        })

        for scbatch, idxbatch, inp in zip(scores, idxs, inputs_orig):
            import sys
            sys.stdout.write(inp)
            for word, score in zip(self.dict.wordize(idxbatch), scbatch):
                sys.stdout.write("%s~%.2f " % (word, score))
            sys.stdout.write("\n")

    def next_batch(self):
        ret = []
        while len(ret) < self.batch_size:
            idx = self.last_sentence
            ret.append(self.check_sents[idx])
            self.last_sentence = (idx + 1) % len(self.check_sents)

        return zip(*ret)
