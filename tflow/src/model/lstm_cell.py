import tensorflow as tf


class ContribFusedLstmCellRnn(object):
    def __init__(self, model, scope, dtype):
        self.model = model
        self.cell = None
        self.scope = scope
        self.dtype = dtype

    def init(self):
        size = self.model.cell_size
        self.cell = tf.contrib.rnn.LSTMBlockFusedCell(size, use_peephole=True)

        _ = self.cell(
            inputs=tf.zeros([1, 1, size], dtype=self.dtype),
            scope=self.scope,
            dtype=self.dtype
        )

    def forward_raw(self, inputs, input_len):
        with tf.variable_scope(self.model.scope, reuse=True) as scope:
            return self.model.cell(
                inputs=inputs,
                sequence_length=tf.cast(input_len, tf.int32),
                dtype=self.dtype,
                scope=scope
            )


class DynamicRnnLstm(object):
    def __init__(self, model, scope, dtype):
        self.model = model
        self.cell = None
        self.scope = scope
        self.dtype = dtype

    def init(self):
        size = self.model.cell_size
        self.cell = tf.contrib.rnn.LSTMCell(
            num_units=size,
            state_is_tuple=True,
            use_peepholes=True
        )

        # put vars to correct collections
        with tf.variable_scope(self.scope) as scope:
            _ = self._core_rnn(
                inputs=tf.ones(shape=[1, 1, self.model.input_size], dtype=self.dtype),
                input_len=tf.ones(shape=[1], dtype=tf.int64),
                scope=scope
            )

    def forward_raw(self, inputs, input_len):
        with tf.variable_scope(self.model.scope, reuse=True) as scope:
            return self._core_rnn(inputs, input_len, scope)

    def _core_rnn(self, inputs, input_len, scope):
        return tf.nn.dynamic_rnn(
            cell=self.cell,
            inputs=inputs,
            sequence_length=input_len,
            dtype=self.dtype,
            time_major=True,
            scope=scope,
            swap_memory=True
        )
