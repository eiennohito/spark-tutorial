import tensorflow as tf


def decode(string_batch):
    print (string_batch)
    parsed = tf.parse_example(string_batch, {
        'words': tf.VarLenFeature(tf.int64),
        'answer': tf.FixedLenFeature([1], tf.int64),
        'len': tf.FixedLenFeature([], tf.int64)
    })

    sparse_words = parsed["words"]
    transp_words = tf.sparse_transpose(sparse_words)
    words = tf.sparse_tensor_to_dense(transp_words, validate_indices=False)
    answers = parsed["answer"]
    lengths = parsed["len"]

    return words, lengths, answers


def read_records(fname_q, args):
    opts = tf.python_io.TFRecordOptions(
        compression_type=tf.python_io.TFRecordCompressionType.GZIP
    )
    reader = tf.TFRecordReader(options=opts)

    key, out = reader.read(fname_q)

    if args.packed:
        parsed = tf.parse_single_example(out, {
            'packed': tf.VarLenFeature(tf.string)
        })
        return parsed['packed'].values
    else:
        return out


def buffer(inputs, capacity=32, shapes=None):
    dtypes = [i.dtype for i in inputs]
    if shapes is None:
        shapes = [i.shape for i in inputs]

    q = tf.PaddingFIFOQueue(capacity=capacity, dtypes=dtypes, shapes=shapes)
    qop = q.enqueue(inputs)
    tf.summary.scalar("input/preoutout_buffer", tf.cast(q.size(), tf.float32) / capacity)
    tf.train.add_queue_runner(tf.train.QueueRunner(queue=q, enqueue_ops=[qop]))
    return q.dequeue(name="output_buffer")


def maybe_parallel_read(filename_q, args):
    nthreads = args.input_threads
    shuffle_size = args.shuffle_size
    batch_size = args.batch_size
    seed = args.seed
    if nthreads == 1:
        record = read_records(filename_q, args)
        record_batch = tf.train.shuffle_batch(
            tensors=[record],
            batch_size=batch_size,
            capacity=shuffle_size * 2,
            min_after_dequeue=shuffle_size,
            seed=seed,
            enqueue_many=args.packed
        )
        return record_batch
    else:
        records = [[read_records(filename_q, args)] for _ in range(0, nthreads)]
        return tf.train.shuffle_batch_join(
            tensors_list=records,
            batch_size=batch_size,
            capacity=shuffle_size * 2,
            min_after_dequeue=shuffle_size,
            seed=seed,
            enqueue_many=args.packed
        )


def input_pipeline(files, args):
    with tf.device("/cpu:0"):
        with tf.name_scope("input_pipeline"):
            fnames = tf.train.string_input_producer(files, num_epochs=args.epochs, shuffle=False)
            record_batch = maybe_parallel_read(fnames, args)

            batch_size = args.batch_size

            batch = decode(record_batch)
            batch = buffer(batch, shapes=[[None, batch_size], [batch_size], [batch_size, 1]])

            lens = batch[1]
            tf.summary.histogram("input/length", lens)
            return batch


def fixed_input_pipeline(data, args):
    with tf.name_scope("input_pipeline"):
        ai, wi, li = data
        shuffled = tf.train.slice_input_producer(
            [wi, ai, li],
            num_epochs=args.epochs, shuffle=True, seed=args.seed, capacity=args.batch_size)
        words, answers, lengths = tf.train.batch(shuffled, batch_size=args.batch_size)
        return tf.transpose(words), lengths, answers


def lines_of_file(filename):
    res = []
    with open(filename, "rt") as f:
        for line in f:
            if len(line) > 5 and line[0] != '#':
                res.append(line.strip())

    return res
