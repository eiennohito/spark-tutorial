import tensorflow as tf
import os
from tensorflow.contrib.session_bundle import exporter


class Exporter(object):
    def __init__(self, session, model):
        self.session = session
        self.model = model
        self.saver = tf.train.Saver(
            sharded=True,
            var_list=tf.trainable_variables()
        )
        self.exporter = exporter.Exporter(self.saver)

        self.exporter.init(
            graph_def=self.session.graph.as_graph_def(),
            clear_devices=True,
            named_graph_signatures={
                'inputs': exporter.generic_signature({
                    'words': model.export_input,
                    'sizes': model.export_lens,
                    'embeds': model.export_embed_id
                }),
                'outputs': exporter.generic_signature({
                    'summaries': model.export_result,
                    'embeddings': model.export_embeddings,
                    'nearest_ids': model.export_nearest_ids,
                    'nearest_scores': model.export_nearest_scores
                })
            }
        )

    def do_export(self, step, output):

        def keep_exports(paths):
            if len(paths) == 1:
                return paths

            to_keep = sorted(paths, key=lambda x: x.export_version, reverse=True)[0:5]
            return to_keep

        exported_path = self.exporter.export(
            export_dir_base=os.path.join(output, "exports"),
            global_step_tensor=step,
            sess=self.session,
            exports_to_keep=keep_exports
        )

        print("exported model to %s" % exported_path)


