import tensorflow as tf
import numpy as np

try:
    import matplotlib.cm as mplcm
    has_mpl = True
except ImportError:
    has_mpl = False

class Imager(object):
    def __init__(self, scheme='nipy_spectral'):
        if has_mpl:
            data = mplcm.get_cmap(scheme)
            colors = data(np.linspace(0, 1, 256, dtype=np.float), bytes=True)
            self.colors = tf.constant(colors, dtype=tf.uint8)

    def to_image_norm(self, x):
        if has_mpl:
            indices = tf.cast(x * 255, dtype=tf.int32)
            return tf.nn.embedding_lookup(self.colors, indices)
        else:
            x = tf.expand_dims(x, axis=-1)
            return tf.image.convert_image_dtype(x, tf.uint8)
