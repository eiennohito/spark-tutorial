import tensorflow as tf
import pyhocon
from input.config import parse_config_args
import os
import sys
import time
import numpy as np
from model.metrics import Metrics

def _parse_batch(strings):
    with tf.device('/cpu:0'):
        dic = tf.parse_example(
            serialized=strings,
            features={
                'context': tf.VarLenFeature(tf.int64),
                'cloze': tf.VarLenFeature(tf.int64),
                'contextLength': tf.FixedLenFeature([], tf.int64),
                'clozeLength': tf.FixedLenFeature([], tf.int64)
            }
        )
        return {
            'context': tf.to_int32(tf.sparse_tensor_to_dense(tf.sparse_transpose(dic['context'], [1, 0]))),
            'cloze': tf.to_int32(tf.sparse_tensor_to_dense(tf.sparse_transpose(dic['cloze'], [1, 0]))),
            'contextLength': tf.to_int32(tf.reshape(dic['contextLength'], [-1])),
            'clozeLength': tf.to_int32(tf.reshape(dic['clozeLength'], [-1])),
        }


def input_dataset(cfg: pyhocon.ConfigTree):
    name_pat = cfg.get_string('pattern')
    files = tf.data.Dataset.list_files(
        name_pat,
        shuffle=False
    )

    def _read_file(nm):
        return tf.data.TFRecordDataset(
            nm,
            compression_type=cfg.get_string('compression', '')
        )

    return files.repeat(
        count=cfg.get_int('epochs', 1)
    ).apply(
        tf.contrib.data.parallel_interleave(
            _read_file,
            cycle_length=cfg.get_int('parallel_reads', 4),
            sloppy=True
        )
    ).shuffle(
        buffer_size=cfg.get_int('shuffle_buffer', 10000),
        seed=cfg.get_int('seed')
    ).batch(
        batch_size=cfg.get_int('batch', 16)
    ).map(
        map_func=_parse_batch,
        num_parallel_calls=cfg.get_int('parallel_parse', 4)
    ).prefetch(cfg.get_int('prefetch', 4))


class Dictionary(object):
    def __init__(self, filename):
        lines = []
        with open(filename, 'rt', encoding='utf-8') as fd:
            for line in fd:
                lines.append(line.rstrip('\n'))
        self.words = lines
        self.idx2word = tf.contrib.lookup.index_to_string_table_from_tensor(
            mapping=self.words
        )

    def size(self):
        return len(self.words)

    def lookup(self, x, name=None):
        return self.idx2word.lookup(tf.to_int64(x), name)


class ModelInput(object):
    def __init__(self, dict: Dictionary, iter: tf.data.Dataset):
        self.dictionary = dict

        self.handle = tf.placeholder(tf.string, shape=[])
        self.iterator = tf.data.Iterator.from_string_handle(
            string_handle=self.handle,
            output_types=iter.output_types,
            output_shapes=iter.output_shapes,
            output_classes=iter.output_classes
        )

        dequeed = self.iterator.get_next()

        self.context = dequeed['context']
        self.cloze = dequeed['cloze']
        self.context_len = dequeed['contextLength']
        self.cloze_len = dequeed['clozeLength']
        self.batch_size = tf.shape(self.context)[0]

    def feed_dict(self, sess, iter: tf.data.Iterator):
        string_handle = sess.run(iter.string_handle())
        return {self.handle: string_handle}


class SimpleEncoder(object):
    def __init__(self, cfg: pyhocon.ConfigTree, model, ids, lens, decoder_size, scope, reuse=tf.AUTO_REUSE):
        self.cfg = cfg
        size = cfg.get_int('size')
        with tf.variable_scope(scope, reuse=reuse):
            self.embedded_ctx = tf.nn.embedding_lookup(
                params=model.vocab_embed,
                ids=ids
            )

            self.ctx_rnn_out, self.ctx_rnn_state = tf.nn.dynamic_rnn(
                cell=Model.lstm_cell(self.cfg),
                inputs=self.embedded_ctx,
                sequence_length=lens,
                swap_memory=True,
                time_major=True,
                dtype=tf.float32
            )

            if size != decoder_size:
                self.ctx_h_proj = tf.layers.Dense(
                    units=decoder_size,
                    activation=tf.nn.selu,
                    name='to_dec/h'
                )

                final_state = self.ctx_h_proj(self.ctx_rnn_state.h)

                self.output = tf.nn.rnn_cell.LSTMStateTuple(
                    c=final_state,
                    h=final_state
                )
            else:
                self.output = tf.nn.rnn_cell.LSTMStateTuple(
                    c=self.ctx_rnn_state.h,
                    h=self.ctx_rnn_state.h
                )


class BiDiEncoder(object):
    def __init__(self, cfg: pyhocon.ConfigTree, model, ids, lens, decoder_size, scope, reuse=tf.AUTO_REUSE):
        self.cfg = cfg
        size = cfg.get_int('size')
        full_size = size * 2
        with tf.variable_scope(scope, reuse=reuse) as sc:
            self.scope = sc
            self.embedded_ctx = tf.nn.embedding_lookup(
                params=model.vocab_embed,
                ids=ids
            )

            self.ctx_rnn_out, self.ctx_rnn_state = tf.nn.bidirectional_dynamic_rnn(
                cell_fw=Model.lstm_cell(self.cfg, name="fw/lstm"),
                cell_bw=Model.lstm_cell(self.cfg, name="bw/lstm"),
                inputs=self.embedded_ctx,
                sequence_length=lens,
                swap_memory=True,
                time_major=True,
                dtype=tf.float32,
                scope=sc
            )

            state_fw, state_bw = self.ctx_rnn_state

            self.result_h = tf.concat([state_fw.h, state_bw.h], axis=1)

            if full_size != decoder_size:
                self.ctx_h_proj = tf.layers.Dense(
                    units=decoder_size,
                    activation=tf.nn.selu,
                    name='to_dec/h'
                )

                final_state = self.ctx_h_proj(self.result_h)

                self.output = tf.nn.rnn_cell.LSTMStateTuple(
                    c=final_state,
                    h=final_state
                )
            else:
                self.output = tf.nn.rnn_cell.LSTMStateTuple(
                    c=self.result_h,
                    h=self.result_h
                )


class BiDiAttEncoder(object):
    def __init__(self, cfg: pyhocon.ConfigTree, model, ids, lens, decoder_size, scope, reuse=tf.AUTO_REUSE):
        self.cfg = cfg
        size = cfg.get_int('size')
        full_size = size * 2
        with tf.variable_scope(scope, reuse=reuse) as sc:
            self.scope = sc
            self.embedded_ctx = tf.nn.embedding_lookup(
                params=model.vocab_embed,
                ids=ids
            )

            self.rnn_out, self.ctx_rnn_state = tf.nn.bidirectional_dynamic_rnn(
                cell_fw=Model.lstm_cell(self.cfg, name="fw/lstm"),
                cell_bw=Model.lstm_cell(self.cfg, name="bw/lstm"),
                inputs=self.embedded_ctx,
                sequence_length=lens,
                swap_memory=True,
                time_major=True,
                dtype=tf.float32,
                scope=sc
            )

            states = tf.concat(self.rnn_out, axis=2)

            weight_size = cfg.get_int('w1_size', 20)

            weight_1 = tf.get_variable(
                name='weights_1',
                shape=[full_size, weight_size]
            )

            weight_2 = tf.get_variable(
                name='weights_2',
                shape=[weight_size, 1]
            )

            weight_internal = tf.nn.tanh(tf.einsum('ijk,kl->ijl', states, weight_1))
            weights = tf.einsum('ijk,kl->ijl', weight_internal, weight_2)
            weights = tf.reshape(weights, tf.shape(ids))

            weights = weights * tf.expand_dims(tf.sqrt(tf.reciprocal(tf.to_float(lens))), axis=0)
            weights = weights * tf.to_float(tf.not_equal(ids, 0))
            if cfg.get_bool('debug', False):
                tf.summary.histogram('attn/weights', weights)

            self.result = tf.einsum('tbd,tb->bd', states, weights)

            if full_size != decoder_size:
                self.ctx_c_proj = tf.layers.Dense(
                    units=decoder_size,
                    activation=tf.nn.selu,
                    name='to_dec'
                )

                self.result = self.ctx_c_proj(self.result)

            self.output = tf.nn.rnn_cell.LSTMStateTuple(
                c=self.result,
                h=self.result
            )


class SelfAttnEncoder(object):
    def __init__(self, cfg: pyhocon.ConfigTree, model, ids, lens, decoder_size, scope, reuse=tf.AUTO_REUSE):
        self.cfg = cfg
        size = cfg.get_int('size')
        self.num_attn = cfg.get_int('attn_number', 8)
        size2 = cfg.get_int('size2', size // self.num_attn)
        size3 = cfg.get_int('size3', size // self.num_attn)
        key_size = cfg.get_int('key_size', size // self.num_attn)
        max_length = cfg.get_int('max_length')
        num_units = model.embed_size
        num_layers = cfg.get_int('num_layers', 2)
        pemb_size = cfg.get_int('pos_embed', None)
        debug = cfg.get_bool('debug', False)
        keep_tform = cfg.get_float('keep_tform', 0.8)
        keep_out = cfg.get_float('keep_out', 0.7)
        normalize = cfg.get_bool('normalize', True)
        keep_layer = cfg.get_float('keep_layer', 0.9)
        activation = cfg.get_string('activation', 'selu')

        import model.transformer as mtf

        with tf.variable_scope(scope, reuse=reuse) as sc:
            self.scope = sc
            self.embedded_ctx = tf.nn.embedding_lookup(
                params=model.vocab_embed,
                ids=ids
            )

            xids = tf.transpose(ids, [1, 0])

            embeds = tf.nn.embedding_lookup(
                params=model.vocab_embed,
                ids=xids
            )

            xids_shape = tf.shape(xids)
            batch_size = xids_shape[0]
            batch_mlen = xids_shape[1]

            if pemb_size is None:
                self.positional = mtf.PositionalEmbedding(num_units, max_length, scale=num_units)
                self.pembs = self.positional.embeds(batch_mlen, batch_size, scale=num_units)
                self.input = embeds + self.pembs
                self.input_size = num_units
            else:
                self.positional = mtf.PositionalEmbedding(pemb_size, max_length)
                self.pembs = self.positional.embeds(batch_mlen, batch_size)
                self.input = tf.concat([embeds, self.pembs], axis=2)
                self.input_size = num_units + pemb_size

            xinput = self.input

            # if normalize:
            #     xinput = tf.layers.dense(
            #         inputs=xinput,
            #         units=self.input_size,
            #         activation=tf.nn.selu,
            #         use_bias=True
            #     )
            #
            #     xinput = mtf.layer_norm(xinput)

            import input.imager as imgr
            imger = imgr.Imager()

            len_mask = tf.to_float(tf.equal(xids, 0)) * -1e6
            len_mask = tf.reshape(len_mask, [batch_size, 1, 1, batch_mlen])
            xinput = tf.nn.dropout(xinput, keep_layer)

            for i in range(num_layers):
                unit = mtf.TransformerUnit(
                    input=xinput,
                    number_attn=self.num_attn,
                    inner_dim=size2,
                    key_dim=key_size,
                    proj_dim=size3,
                    name=f'tf_{i}',
                    attn_keep=keep_tform,
                    mask=len_mask,
                    normalize=normalize,
                    keep_layer=keep_layer,
                    activation=activation
                )
                xinput = unit.output

                if debug:
                    tf.summary.image(f'tf_{i}_attn', imger.to_image_norm(unit.attn.Attn[0]), self.num_attn)
                    tf.summary.histogram(f'tf_{i}_attn_raw', unit.attn.AttnWeights)

            self.final = mtf.FinalSelfAttention(
                input=xinput,
                number_attn=self.num_attn,
                inner_dim=size,
                key_dim=key_size,
                output=decoder_size,
                lens=lens,
                name='tf_out',
                keep_prob=keep_out,
                out_bias=True,
                out_act=tf.nn.tanh
            )

            if debug:
                tf.summary.histogram('tf_out_attn_raw', self.final.AttnWeights)
                tf.summary.image('tf_out_attn', imger.to_image_norm(self.final.Attn))

            final_state = tf.nn.dropout(self.final.output, keep_layer)

            self.output = tf.nn.rnn_cell.LSTMStateTuple(
                h=final_state,
                c=final_state
            )


class TwoBiDiEncoderConcat(object):
    def __init__(self, cfg: pyhocon.ConfigTree, model, ids, lens, decoder_size, scope, reuse=tf.AUTO_REUSE):
        self.cfg = cfg
        size = cfg.get_int('size')
        size2 = cfg.get_int('size2', size)
        full_size = size * 2 + size2 * 2
        with tf.variable_scope(scope, reuse=reuse) as sc:
            self.scope = sc
            self.embedded_ctx = tf.nn.embedding_lookup(
                params=model.vocab_embed,
                ids=ids
            )

            self.ctx_rnn_out1, self.ctx_rnn_state1 = tf.nn.bidirectional_dynamic_rnn(
                cell_fw=Model.lstm_cell2(size, name="fw/lstm1"),
                cell_bw=Model.lstm_cell2(size, name="bw/lstm1"),
                inputs=self.embedded_ctx,
                sequence_length=lens,
                swap_memory=True,
                time_major=True,
                dtype=tf.float32,
                scope=sc
            )

            ofw, obw = self.ctx_rnn_out1

            in2 = tf.concat([ofw, obw, self.embedded_ctx], axis=2)

            self.ctx_rnn_out2, self.ctx_rnn_state2 = tf.nn.bidirectional_dynamic_rnn(
                cell_fw=Model.lstm_cell2(size2, name="fw/lstm2"),
                cell_bw=Model.lstm_cell2(size2, name="bw/lstm2"),
                inputs=in2,
                sequence_length=lens,
                swap_memory=True,
                time_major=True,
                dtype=tf.float32,
                scope=sc
            )

            state_fw1, state_bw1 = self.ctx_rnn_state1
            state_fw2, state_bw2 = self.ctx_rnn_state2

            self.result_h = tf.concat([state_fw1.h, state_bw1.h, state_fw2.h, state_bw2.h], axis=1)

            if full_size != decoder_size:
                self.ctx_h_proj = tf.layers.Dense(
                    units=decoder_size,
                    activation=tf.nn.selu,
                    name='to_dec/h'
                )

                proj_h = self.ctx_h_proj(self.result_h)
                self.output = tf.nn.rnn_cell.LSTMStateTuple(
                    c=proj_h,
                    h=proj_h
                )
            else:
                self.output = tf.nn.rnn_cell.LSTMStateTuple(
                    c=self.result_h,
                    h=self.result_h
                )


class TriConvDecoder(object):
    def pad(self, x):
        return tf.pad(
            tensor=x,
            paddings=[[0, 0], [1, 1], [0, 0], [0, 0]]
        )

    def __init__(self, cfg: pyhocon.ConfigTree, model, ids, lens, decoder_size, scope, reuse=tf.AUTO_REUSE):
        self.cfg = cfg
        end_size = cfg.get_int('size')
        start_size = cfg.get_int('size2', model.embed_size)

        with tf.variable_scope(scope, reuse=reuse) as sc:
            self.scope = sc
            self.embedded_ctx = tf.nn.embedding_lookup(
                params=model.vocab_embed,
                ids=ids
            )

            self.filter_0 = tf.get_variable(
                name='conv_f0',
                shape=[3, model.embed_size, 1, start_size],
                initializer=tf.random_normal_initializer(
                    stddev=1 / start_size
                )
            )

            xids = tf.transpose(ids, [1, 0])
            input = tf.expand_dims(
                tf.nn.embedding_lookup(
                    params=model.vocab_embed,
                    ids=xids
                ),
                axis=3
            )

            input_mask = tf.to_float(xids != 0)

            input = input * input_mask

            state = tf.nn.conv2d(
                input=self.pad(input),
                filter=self.filter_0,
                strides=[1, 1, 1, 1],
                padding='VALID'
            )

            shape = tf.concat([tf.shape(state)[0:2], [start_size, 1]], axis=0)
            state = tf.reshape(state, shape, name="rsp_conv_a")

            nrows = int(np.ceil(np.log2(cfg.get_int('max_length'))))
            rate = (end_size - start_size) / nrows
            prev_dim = start_size

            for i in range(nrows):
                dim = int(start_size + rate * (i + 1))

                filt = tf.get_variable(
                    name=f'filter_{i}',
                    shape=[3, prev_dim, 1, dim],
                    initializer=tf.random_normal_initializer(
                        stddev=1 / dim
                    )
                )

                state = tf.nn.selu(state)

                tf.summary.histogram(f'in_{i}', state)

                state = tf.nn.conv2d(
                    input=self.pad(state),
                    filter=filt,
                    strides=[1, 2, 1, 1],
                    padding='VALID'
                )

                shape = tf.concat([tf.shape(state)[0:2], [dim, 1]], axis=0)
                state = tf.reshape(state, shape, name=f'conv_rsp_{i}')
                prev_dim = dim

            tf.assert_equal(tf.shape(state)[1], 1)

            state = tf.squeeze(state)
            state.set_shape([None, end_size])

            if end_size != decoder_size:
                self.ctx_proj = tf.layers.Dense(
                    units=decoder_size,
                    activation=tf.nn.selu,
                    name='to_dec'
                )

                state = self.ctx_proj(state)

            self.output = tf.nn.rnn_cell.LSTMStateTuple(
                c=state,
                h=state
            )


class StateDecoder(object):
    def __init__(self, cfg: pyhocon.ConfigTree, model, state, train_ids=None, decode_len=None, name='decoder'):
        self.cfg = cfg
        self.state = state

        with tf.variable_scope(cfg.get_string('name', name), reuse=tf.AUTO_REUSE) as sc:
            self.scope = sc
            if train_ids is not None:
                self.inputs = tf.nn.embedding_lookup(
                    params=model.vocab_embed,
                    ids=train_ids[:-1, :]  # ignore last symbol
                )

                self.helper = tf.contrib.seq2seq.TrainingHelper(
                    inputs=self.inputs,
                    sequence_length=tf.cast(decode_len - 1, dtype=tf.int32),
                    time_major=True
                )
            else:
                self.helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
                    embedding=model.vocab_embed,
                    start_tokens=tf.tile([cfg.get_int('bos_id')], tf.shape(state)[0]),
                    end_token=cfg.get_int('eos_id')
                )

            self.rnn_cell = Model.lstm_cell(cfg)

            self.decoder = tf.contrib.seq2seq.BasicDecoder(
                cell=self.rnn_cell,
                helper=self.helper,
                initial_state=self.state,
                output_layer=model.decoder_proj
            )

            self.max_seq_len = max_seq_len = cfg.get_int('max_length') - 1

            self.outputs, self.final_state, self.lengths = tf.contrib.seq2seq.dynamic_decode(
                decoder=self.decoder,
                output_time_major=True,
                impute_finished=train_ids is not None,
                maximum_iterations=max_seq_len,
                scope=sc
            )

            if train_ids is not None:
                labels = train_ids[1:] # first one will be BOSk

                sample_size = cfg.get_int('sample_size', None)
                if sample_size is None:
                    self.logits = tf.einsum('ijk,lk->ijl', self.outputs.rnn_output, model.vocab_embed)
                    self.raw_loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
                        labels=labels,
                        logits=self.logits
                    )
                else:
                    logits = tf.reshape(self.outputs.rnn_output, [-1, model.embed_size])
                    raw_loss = tf.nn.sampled_softmax_loss(
                        weights=model.vocab_embed,
                        biases=tf.constant(0.0, shape=[model.input.dictionary.size()]),
                        labels=tf.reshape(labels, [-1, 1]),
                        inputs=logits,
                        num_sampled=sample_size,
                        num_classes=model.input.dictionary.size()
                    )

                    self.raw_loss = tf.reshape(raw_loss, tf.shape(labels))

                self.mask_eos = tf.equal(labels, 3)
                self.mask_aux = (labels >= 4) & (labels < 100)

                content_mask = cfg.get_int('mask_content', None)
                if content_mask is not None:
                    self.scaled_labels = tf.minimum(tf.to_float(labels) / content_mask, 1.0)
                    self.mask = tf.accumulate_n([
                        tf.to_float(self.mask_eos),
                        0.03 * tf.to_float(self.mask_aux),
                        self.scaled_labels
                    ])
                else:
                    self.mask_content = tf.greater_equal(labels, 100)
                    self.mask = tf.accumulate_n([
                        1.5 * tf.to_float(self.mask_eos),
                        0.1 * tf.to_float(self.mask_aux),
                        tf.to_float(self.mask_content)
                    ])

                self.loss = tf.reduce_mean(self.raw_loss * self.mask)
                self.xloss = self.loss * tf.reciprocal(tf.reduce_mean(self.mask))
            else:
                self.xloss = self.loss = tf.constant(0)


class Model(object):

    @staticmethod
    def lstm_cell(cfg: pyhocon.ConfigTree, name='lstm_cell'):
        return Model.lstm_cell2(cfg.get_int('size', 20), name)

    @staticmethod
    def lstm_cell2(size: int, name='lstm_cell'):
        # if tf.test.is_built_with_cuda() and tf.test.is_gpu_available(cuda_only=True) and cfg.get_bool('cudnn', True):
        #     return tf.contrib.cudnn_rnn.CudnnLSTM(
        #         num_units=cfg.get_int('size', 20),
        #         input_mode='auto_select',
        #         num_layers=1
        #     )
        # else:
        #     return tf.contrib.cudnn_rnn.CudnnCompatibleLSTMCell(
        #         num_units=cfg.get_int('size', 20),
        #         use_peephole=True
        #     )
        return tf.contrib.rnn.LSTMBlockCell(
            num_units=size,
            use_peephole=True,
            name=name
        )

    @staticmethod
    def encoder(cfg: pyhocon.ConfigTree, cfg_name, scope, model, input_ids, input_len, decoder_size,
                reuse=tf.AUTO_REUSE):
        subcfg = cfg.get_config(cfg_name)
        enctype = subcfg.get_string('type', 'basic')

        print("Using", enctype, "for", scope, file=sys.stderr)

        if enctype == 'basic':
            encoder = SimpleEncoder
        elif enctype == 'bidi':
            encoder = BiDiEncoder
        elif enctype == '2bidic':
            encoder = TwoBiDiEncoderConcat
        elif enctype == 'conv':
            encoder = TriConvDecoder
        elif enctype == 'bidiattn':
            encoder = BiDiAttEncoder
        elif enctype == 'attn':
            encoder = SelfAttnEncoder
        else:
            raise NotImplementedError("encoder type is not supported:", enctype)

        return encoder(
            cfg=subcfg,
            model=model,
            ids=input_ids,
            lens=input_len,
            decoder_size=decoder_size,
            scope=scope,
            reuse=reuse
        )

    def __init__(self, input: ModelInput, cfg: pyhocon.ConfigTree):
        self.input = input
        self.cfg = cfg

        self.embed_size = cfg.get_int('vocab.embed_size')
        decoder_size = cfg.get_int('decoder.size')
        cloze_size = cfg.get_int('lstm.cloze.size')

        self.global_step = tf.get_variable(
            'global_step',
            shape=[],
            dtype=tf.int64,
            trainable=False
        )

        self.vocab_embed = tf.get_variable(
            name='vocab_embed',
            shape=[input.dictionary.size(), self.embed_size],
            dtype=tf.float32,
            initializer=tf.random_normal_initializer(
                dtype=tf.float32,
                stddev=1.0 / np.sqrt(self.embed_size),
                seed=cfg.get_int('seed') + hash('vocab_embed')
            )
        )

        self.ctx_enc = Model.encoder(
            cfg=cfg,
            cfg_name="lstm.context",
            scope="ctx",
            model=self,
            input_ids=self.input.context,
            input_len=self.input.context_len,
            decoder_size=decoder_size
        )

        with tf.variable_scope('clz'):
            self.embedded_clz = tf.nn.embedding_lookup(
                params=self.vocab_embed,
                ids=self.input.cloze
            )

            self.embedded_clz = tf.nn.dropout(
                self.embedded_clz,
                keep_prob=cfg.get_float('lstm.cloze.embed.dropout', 0.5)
            )

            self.clz_rnn_out, self.clz_rnn_state = tf.nn.dynamic_rnn(
                cell=Model.lstm_cell(self.cfg.get_config('lstm.cloze')),
                inputs=self.embedded_clz,
                sequence_length=self.input.cloze_len,
                swap_memory=True,
                time_major=True,
                dtype=tf.float32
            )

            clz_out_dropout = cfg.get_float('lstm.cloze.output.dropout', 0.5)
            self.clz_rnn_state = tf.nn.rnn_cell.LSTMStateTuple(
                c=tf.nn.dropout(self.clz_rnn_state.c, keep_prob=clz_out_dropout),
                h=tf.nn.dropout(self.clz_rnn_state.h, keep_prob=clz_out_dropout)
            )

            if cloze_size != decoder_size:
                self.clz_h_proj = tf.layers.Dense(
                    units=decoder_size,
                    activation=tf.nn.selu,
                    name='clz2dec/h'
                )

                h_proj = self.clz_h_proj(self.clz_rnn_state.h)

                self.clz_dec_input = tf.nn.rnn_cell.LSTMStateTuple(
                    c=h_proj,
                    h=h_proj
                )
            else:
                self.clz_dec_input = tf.nn.rnn_cell.LSTMStateTuple(
                    c=self.clz_rnn_state.h,
                    h=self.clz_rnn_state.h
                )

            clz_out_scale_end = cfg.get_int('lstm.cloze.output.scale.end', 0)

            if clz_out_scale_end != 0:
                clz_out_scale_start = cfg.get_int('lstm.cloze.output.scale.start', 0)
                clz_out_scale_min = cfg.get_float('lstm.cloze.output.scale.min', 0)
                clz_out_scale_max = cfg.get_float('lstm.cloze.output.scale.max', 1)
                out_scale_ratio = (clz_out_scale_max - clz_out_scale_min) / (clz_out_scale_end - clz_out_scale_start)
                self.clz_scale_sigma = tf.train.piecewise_constant(
                    x=self.global_step,
                    boundaries=[clz_out_scale_start, clz_out_scale_end],
                    values=[
                        0.0,
                        clz_out_scale_min + out_scale_ratio * tf.to_float(self.global_step - clz_out_scale_start),
                        clz_out_scale_max
                    ]
                )

                self.clz_scale = tf.exp(tf.random_normal(
                    shape=[tf.shape(self.clz_rnn_state.c)[0], 1],
                    stddev=self.clz_scale_sigma
                ))

                self.clz_dec_input = tf.nn.rnn_cell.LSTMStateTuple(
                    c=self.clz_dec_input.c * self.clz_scale,
                    h=self.clz_dec_input.h * self.clz_scale
                )
            else:
                self.clz_scale = tf.constant(1, dtype=tf.float32)

        if self.embed_size != decoder_size:
            self.decoder_proj = tf.layers.Dense(
                units=self.embed_size,
                activation=tf.nn.selu
            )
        else:
            self.decoder_proj = None

        self.decoder_layer = DecoderLayer(self)

    @property
    def ctx_dec_input(self):
        return self.ctx_enc.output


class DecoderLayer(tf.layers.Layer):
    def __init__(self, mdl: Model, **kwargs):
        super().__init__(**kwargs)
        self.mdl = mdl

    def call(self, inputs, **kwargs):
        if self.mdl.decoder_proj is not None:
            inputs = self.mdl.decoder_proj(inputs)
        input_shape = inputs.shape
        if len(input_shape) == 2:
            return tf.matmul(inputs, self.mdl.vocab_embed, transpose_b=True)
        elif len(input_shape) == 3:
            return tf.einsum('abc,dc->abd', inputs, self.mdl.vocab_embed)
        else:
            raise NotImplementedError()

    def compute_output_shape(self, input_shape):
        if len(input_shape) == 2:
            return tf.TensorShape([input_shape[0], self.mdl.vocab_embed.shape[0]])
        elif len(input_shape) == 3:
            return tf.TensorShape([input_shape[0], input_shape[1], self.mdl.vocab_embed.shape[0]])
        else:
            raise NotImplementedError()


class NoAdv(object):
    def __init__(self, step, cfg: pyhocon.ConfigTree, in_x, in_y):
        self.cfg = cfg

        in_x = in_x.h
        in_y = in_y.h

        self.regular_y = in_y
        self.stopped_y = tf.stop_gradient(in_y)
        self.regular_x = in_x
        self.stopped_x = tf.stop_gradient(in_x)

        self.x_lens = tf.reduce_sum(tf.square(in_x), axis=1)
        self.y_lens = tf.reduce_sum(tf.square(in_y), axis=1)

        x_ndims = tf.shape(in_x)[1]
        y_ndims = tf.shape(in_y)[1]

        self.x_len_loss = tf.reduce_mean(
            tf.log(tf.maximum(self.x_lens - tf.to_float(x_ndims), 1.0))
        )

        self.y_len_loss = tf.reduce_mean(
            tf.log(tf.maximum(self.y_lens - tf.to_float(y_ndims), 1.0))
        )

        self.losses = []

        len_pw = cfg.get_float('len_pw', 1e-3)
        if len_pw != 0:
            self.losses.append(self.x_len_loss * len_pw)
            self.losses.append(self.y_len_loss * len_pw)

        self.losses = [
            self.x_len_loss,
            self.y_len_loss
        ]

        slope_start = cfg.get_int('slope_start', None)
        slope_end = cfg.get_int('slope_end', None)
        if slope_start is not None and slope_end is not None:
            self.loss_mplier = tf.train.piecewise_constant(
                x=step,
                boundaries=[slope_start, slope_end],
                values=[
                    0.0,
                    tf.to_float(step - slope_start) / (slope_end - slope_start),
                    1.0
                ]
            )
        else:
            self.loss_mplier = tf.constant(1.0)


class AdvNorm(NoAdv):
    def __init__(self, step, cfg: pyhocon.ConfigTree, in_x, in_y):
        super().__init__(step, cfg, in_x, in_y)
        layer_cfg = cfg.get_list('layers')
        layer_cfg.append(1)
        layer_cfg.insert(0, self.stopped_x.shape[-1])

        self.layers = []

        with tf.variable_scope('advnet', reuse=tf.AUTO_REUSE):
            for i in range(1, len(layer_cfg)):
                weight = tf.get_variable(
                    name=f"w_{i}",
                    shape=[layer_cfg[i - 1], layer_cfg[i]],
                    dtype=tf.float32,
                    initializer=tf.random_normal_initializer(
                        stddev=1.0 / layer_cfg[i]
                    )
                )
                bias = tf.get_variable(
                    name=f'b_{i}',
                    shape=[layer_cfg[i]],
                    dtype=tf.float32,
                    initializer=tf.zeros_initializer()
                )
                self.layers.append((weight, bias))

        x_adv = self.stopped_x
        x_gen = self.regular_x
        y_adv = self.stopped_y
        y_gen = self.regular_y

        in_size = tf.shape(self.stopped_x)[0]
        self.alpha = tf.random_uniform(
            shape=[in_size, 1],
            minval=0,
            maxval=1
        )
        self.between = self.stopped_y + self.alpha * (self.stopped_x - self.stopped_y)

        self.w_reg = []

        def fn(x):
            y = tf.nn.softplus(2 * x + 2)
            return 0.5 * y - 1

        between = self.between

        for w, b in self.layers:
            x_adv = tf.matmul(fn(x_adv), w) + b
            x_gen = tf.matmul(fn(x_gen), tf.stop_gradient(w)) + tf.stop_gradient(b)
            y_adv = tf.matmul(fn(y_adv), w) + b
            y_gen = tf.matmul(fn(y_gen), tf.stop_gradient(w)) + tf.stop_gradient(b)
            between = tf.matmul(fn(between), w) + b
            self.w_reg.append(tf.log(tf.nn.l2_loss(w) + 1) * 1.0E-5)

        self.x_adv = x_adv
        self.x_gen = x_gen
        self.y_adv = y_adv
        self.y_gen = y_gen
        self.between_logits = between
        self.between_grad = tf.gradients(self.between_logits, self.between)[0]
        self.grad_squares = tf.reduce_sum(tf.square(self.between_grad), axis=1)
        self.grad_diffs = tf.square(tf.sqrt(self.grad_squares) - 1)
        self.grad_loss = tf.reduce_mean(self.grad_diffs)

        def l2loss(x):
            return x * x

        self.prob_x = tf.nn.sigmoid(x_adv)
        self.prob_y = tf.nn.sigmoid(y_gen)
        self.loss_raw_xadv = l2loss(self.prob_x)
        self.loss_raw_xgen = l2loss(1 - tf.nn.sigmoid(x_gen))
        self.loss_raw_yadv = l2loss(1 - tf.nn.sigmoid(y_adv))
        self.loss_raw_ygen = l2loss(self.prob_y)

        self.loss_xadv = l2loss(tf.nn.sigmoid(tf.reduce_mean(x_adv)))
        self.loss_xgen = tf.reduce_mean(self.loss_raw_xgen)
        self.loss_yadv = l2loss(1 - tf.nn.sigmoid(tf.reduce_mean(y_adv)))
        self.loss_ygen = tf.reduce_mean(self.loss_raw_ygen)
        self.loss_x_flag = tf.reduce_mean(self.prob_x) >= 0.5
        self.loss_y_flag = tf.reduce_mean(self.prob_y) >= 0.5

        self.norm_x_raw = tf.log(tf.maximum(l2loss(x_adv) - 9, 1))
        self.norm_y_raw = tf.log(tf.maximum(l2loss(y_adv) - 9, 1))
        self.regul = tf.reduce_mean(self.norm_x_raw) + tf.reduce_mean(self.norm_y_raw)

        fake_weight = cfg.get_float('fake_weight', 0.1)
        real_weight = cfg.get_float('real_weight', 0.01)
        adv_weight = cfg.get_float('adv_weight', 1)

        self.losses += [
                           self.loss_xadv * adv_weight,
                           self.loss_yadv * adv_weight,
                           self.loss_xgen * self.loss_mplier * fake_weight,
                           self.loss_ygen * self.loss_mplier * real_weight,
                           self.regul,
                           self.grad_loss
                       ] + self.w_reg

        tf.summary.scalar('loss/adv/clz_adv', self.loss_xadv)
        tf.summary.scalar('loss/adv/ctx_adv', self.loss_yadv)
        tf.summary.scalar('loss/adv/reg', self.regul + tf.accumulate_n(self.w_reg))
        tf.summary.histogram('dist/clz_prob', self.prob_x)
        tf.summary.histogram('dist/ctx_prob', self.prob_y)
        tf.summary.histogram('dist/grad_log', tf.log(self.grad_squares))


class AdvWass(NoAdv):
    def __init__(self, step, cfg: pyhocon.ConfigTree, fake_in, real_in):
        super().__init__(step, cfg, fake_in, real_in)
        self.cfg = cfg

        layer_cfg = cfg.get_list('layers')
        layer_cfg.append(1)
        layer_cfg.insert(0, self.stopped_x.shape[-1])

        self.layers = []

        in_size = tf.shape(self.stopped_x)[0]
        self.alpha = tf.random_uniform(
            shape=[in_size, 1],
            minval=0,
            maxval=1
        )

        self.between = self.stopped_y + self.alpha * (self.stopped_x - self.stopped_y)

        with tf.variable_scope('advnet', reuse=tf.AUTO_REUSE):
            for i in range(1, len(layer_cfg)):
                weight = tf.get_variable(
                    name=f"w_{i}",
                    shape=[layer_cfg[i - 1], layer_cfg[i]],
                    dtype=tf.float32,
                    initializer=tf.random_normal_initializer(
                        stddev=1.0 / layer_cfg[i]
                    )
                )
                bias = tf.get_variable(
                    name=f'b_{i}',
                    shape=[layer_cfg[i]],
                    dtype=tf.float32,
                    initializer=tf.zeros_initializer()
                )
                self.layers.append((weight, bias))

        fake_adv = self.stopped_x
        fake_gen = self.regular_x
        real_adv = self.stopped_y
        real_gen = self.regular_y
        between = self.between

        self.w_reg = []

        def fn(x):
            y = tf.nn.softplus(2 * x + 2)
            return 0.5 * y - 1

        for w, b in self.layers:
            fake_adv = tf.matmul(fn(fake_adv), w) + b
            fake_gen = tf.matmul(fn(fake_gen), tf.stop_gradient(w)) + tf.stop_gradient(b)
            real_adv = tf.matmul(fn(real_adv), w) + b
            real_gen = tf.matmul(fn(real_gen), tf.stop_gradient(w)) + tf.stop_gradient(b)
            between = tf.matmul(fn(between), w) + b
            self.w_reg.append(1.0E-5 * tf.nn.l2_loss(w))

        self.x_adv = fake_adv
        self.x_gen = fake_gen
        self.y_adv = real_adv
        self.y_gen = real_gen
        self.between_logits = between
        self.between_grad = tf.gradients(self.between_logits, self.between)[0]
        self.grad_squares = tf.reduce_sum(tf.square(self.between_grad), axis=1)
        one_sided = cfg.get_bool('one_sided', False)
        self.grad_pentalties_raw = tf.maximum(self.grad_squares, 1.0)
        if one_sided:
            self.grad_loss = tf.reduce_mean(self.grad_pentalties_raw - 1)
        else:
            grad_diffs = tf.sqrt(self.grad_squares + 1e-6) - 1
            grad_diffs = tf.square(grad_diffs)
            self.grad_loss = tf.reduce_mean(grad_diffs)

        grad_vis = tf.log(tf.sqrt(self.grad_squares + 1e-6))

        self.grad_pentalties_log = tf.log(self.grad_pentalties_raw)

        self.fake_loss = tf.reduce_mean(-fake_gen)
        self.real_loss = tf.reduce_mean(real_gen)
        self.adv_diff = fake_adv - real_adv
        self.adv_loss = tf.reduce_mean(fake_adv) - tf.reduce_mean(real_adv)

        self.mean_grad_log = tf.reduce_mean(self.grad_pentalties_log)
        self.grad_mplier = tf.stop_gradient(tf.exp(-self.mean_grad_log))
        self.grad_mplier2 = tf.stop_gradient(tf.exp(-self.mean_grad_log * self.mean_grad_log))

        fake_weight = cfg.get_float('fake_weight', 0.1)
        real_weight = cfg.get_float('real_weight', 0.001)
        adv_weight = cfg.get_float('adv_weight', 0.1)

        self.losses += [
            fake_weight * self.loss_mplier * self.grad_mplier2 * self.fake_loss,
            real_weight * self.loss_mplier * self.grad_mplier2 * self.real_loss,
            adv_weight * self.grad_mplier * self.adv_loss,
            self.grad_loss
        ]
        self.losses += self.w_reg

        tf.summary.scalar('loss/wgan/fake', self.fake_loss)
        tf.summary.scalar('loss/wgan/real', self.real_loss)
        tf.summary.scalar('loss/wgan/adv', self.adv_loss)
        tf.summary.scalar('loss/wgan/grad', self.grad_loss)
        tf.summary.histogram('dist/wgan/fake', fake_adv)
        tf.summary.histogram('dist/wgan/real', real_adv)
        tf.summary.histogram('dist/wgan/adv', self.adv_diff)
        tf.summary.histogram('dist/wgan/grad', grad_vis)


class Trainer(object):
    def __init__(self, cfg: pyhocon.ConfigTree, mdl: Model, dec1: StateDecoder, dec2: StateDecoder, adv: NoAdv):
        self.model = mdl
        self.full_loss = tf.accumulate_n([
                                             10 * dec1.loss,
                                             dec2.loss] + adv.losses)
        steps = cfg.get_int('optimizer.lrate.steps', 1000)
        decay = cfg.get_float('optimizer.lrate.decay', 0.95)
        self.lrate = tf.train.piecewise_constant(
            x=mdl.global_step,
            boundaries=[100],
            values=[
                tf.to_float(mdl.global_step) * tf.to_float(1E-3 / 100),
                tf.train.exponential_decay(1.0E-3, mdl.global_step, steps, decay, staircase=True)
            ]
        )
        self.trainer = tf.train.AdamOptimizer(
            learning_rate=self.lrate
        )

        self.train_op = self.trainer.minimize(
            loss=self.full_loss,
            global_step=mdl.global_step
        )

        # gvs = self.trainer.compute_gradients(self.full_loss)
        # capped_gvs = [(tf.clip_by_value(grad, -0.001, 0.001), var) for grad, var in gvs]
        #
        # for grad, var in gvs:
        #     tf.summary.histogram('grad/' + var.name, grad)
        #
        # self.capped_train_op = self.trainer.apply_gradients(
        #     capped_gvs,
        #     global_step=self.global_step
        # )

    @property
    def global_step(self):
        return self.model.global_step


class Summaries(object):
    def __init__(self, input, model, ctx_dec, clz_dec, adv: NoAdv, trainer):
        tf.summary.scalar('loss/s2s/ctx2clz', ctx_dec.loss)
        tf.summary.scalar('loss/s2s/clz2clz', clz_dec.loss)
        tf.summary.scalar('loss/s2sx/ctx2clz', ctx_dec.xloss)
        tf.summary.scalar('loss/s2sx/clz2clz', clz_dec.xloss)
        tf.summary.scalar('loss/full', trainer.full_loss)
        tf.summary.scalar('loss/mplier', adv.loss_mplier)
        tf.summary.scalar('loss/rate', trainer.lrate)

        self.euclid_dist = tf.norm(adv.stopped_x - adv.stopped_y, axis=1)
        tf.summary.histogram('dist/ctx_clz/euclid', self.euclid_dist)
        self.dotprods = tf.einsum(
            'ij,ij->i',
            tf.nn.l2_normalize(adv.stopped_x, axis=1),
            tf.nn.l2_normalize(adv.stopped_y, axis=1)
        )
        self.x_lengths = tf.einsum('ij,ij->i', adv.stopped_x, adv.stopped_x)
        self.y_lengths = tf.einsum('ij,ij->i', adv.stopped_y, adv.stopped_y)
        tf.summary.histogram('dist/ctx/len', tf.sqrt(self.y_lengths))
        tf.summary.histogram('dist/clz/len', tf.sqrt(self.x_lengths))
        tf.summary.histogram('dist/ctx_clz/cosine', self.dotprods)
        tf.summary.histogram('len/ctx', input.context_len)
        tf.summary.histogram('len/clz', input.cloze_len)
        tf.summary.scalar('misc/mask', tf.reduce_mean(ctx_dec.mask))
        tf.summary.histogram('misc/clz_scale', model.clz_scale)

        if tf.test.is_gpu_available():
            tf.summary.scalar('memory/max_used', tf.contrib.memory_stats.MaxBytesInUse())


class Eval(object):
    def __init__(self, xcfg: pyhocon.ConfigTree, mdl: Model, dec: StateDecoder):
        self.cfg = xcfg
        cfg = xcfg.get_config('eval', None)
        if cfg is None:
            self.no_eval = True
            return
        else:
            self.no_eval = False

        self.model = mdl
        self.input_ref = mdl.input.handle
        self.batch_size = tf.shape(mdl.input.context_len)

        self.helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
            embedding=mdl.vocab_embed,
            start_tokens=tf.tile([cfg.get_int('bos_id')], self.batch_size),
            end_token=cfg.get_int('eos_id')
        )

        beam_width = cfg.get_int('beam', 5)
        state_c = tf.contrib.seq2seq.tile_batch(mdl.ctx_dec_input.c, beam_width)
        state_h = tf.contrib.seq2seq.tile_batch(mdl.ctx_dec_input.h, beam_width)

        self.init_state = tf.nn.rnn_cell.LSTMStateTuple(
            state_c,
            state_h
        )

        self.decoder = tf.contrib.seq2seq.BeamSearchDecoder(
            cell=dec.rnn_cell,
            embedding=mdl.vocab_embed,
            start_tokens=tf.tile([cfg.get_int('bos_id')], self.batch_size),
            end_token=cfg.get_int('eos_id'),
            initial_state=self.init_state,
            beam_width=beam_width,
            output_layer=mdl.decoder_layer
        )

        self.outputs, self.final_state, self.lengths = tf.contrib.seq2seq.dynamic_decode(
            decoder=self.decoder,
            output_time_major=False,
            impute_finished=False,
            maximum_iterations=dec.max_seq_len,
            scope=dec.scope
        )

        self.beam_predicted = self.outputs.predicted_ids

        self.predicted_top1 = self.beam_predicted[:, :, 0]
        cloze = tf.transpose(tf.cast(mdl.input.cloze, tf.int32), [1, 0])
        self.matched_top1_raw = self.predicted_top1 == cloze[1:]
        self.input_mask = tf.transpose(mdl.input.cloze > 0, [1, 0])
        self.matched_top1_masked = self.matched_top1_raw & self.input_mask
        self.matched_top1_counts = tf.reduce_sum(tf.cast(self.matched_top1_masked, tf.int32), axis=1)
        self.expected_top1_counts = mdl.input.cloze_len - 1

        self.beam_by_batch = tf.reshape(
            self.beam_predicted,
            tf.concat([tf.shape(self.beam_predicted)[0:1], [-1]], axis=0)
        )
        bad_constant = tf.tile(
            input=tf.constant([0, 1, 2, 3], dtype=tf.int32, shape=[1, 4]),
            multiples=tf.concat([tf.shape(self.beam_predicted)[0:1], [1]], axis=0)
        )
        self.cloze_set = tf.sets.set_difference(cloze, bad_constant)
        self.matching_words = tf.sets.set_intersection(self.beam_by_batch, self.cloze_set)
        self.beam_counts = tf.sets.set_size(self.matching_words)
        self.input_counts = tf.sets.set_size(self.cloze_set)

        self.beam_plus = self.outputs.beam_search_decoder_output.predicted_ids
        self.beam_plus_by_batch = tf.reshape(
            self.beam_plus,
            tf.concat([tf.shape(self.beam_plus)[0:1], [-1]], axis=0)
        )
        self.beam_plus_matching_words = tf.sets.set_intersection(self.beam_plus_by_batch, self.cloze_set)
        self.beam_plus_counts = tf.sets.set_size(self.beam_plus_matching_words)

        self.metrics = Metrics('eval')

        acc_0 = tf.cast(self.beam_plus_counts, tf.float32) / tf.cast(self.input_counts, tf.float32)
        acc_0.set_shape([None])
        self.bplus_microavg_met = self.metrics.mean(
            values=acc_0,
            scope='beamplus/acc/micro'
        )
        self.bplus_macroavg_met = self.metrics.accuracy(
            labels=self.input_counts,
            predictions=self.beam_plus_counts,
            scope='beamplus/acc/macro'
        )
        self.metrics.scalar('eval/beamplus_acc/micro_avg', self.bplus_microavg_met)
        self.metrics.scalar('eval/beamplus_acc/macro_avg', self.bplus_macroavg_met)
        acc = tf.cast(self.beam_counts, tf.float32) / tf.cast(self.input_counts, tf.float32)
        acc.set_shape([None])
        self.beam_microavg_met = self.metrics.mean(
            values=acc,
            scope='beam/acc/macro'
        )
        self.beam_macroavg_met = self.metrics.accuracy(
            labels=self.input_counts,
            predictions=self.beam_counts,
            scope='beam/acc/macro'
        )
        self.metrics.scalar('eval/beam_acc/micro_avg', self.beam_microavg_met)
        self.metrics.scalar('eval/beam_acc/macro_avg', self.beam_macroavg_met)

        def _pad(x):
            return tf.pad(
                x,
                [[0, dec.max_seq_len - tf.shape(x)[0]], [0, 0]]
            )

        vis_concat = tf.concat(
            [
                _pad(tf.expand_dims(mdl.input.cloze[1:, 0], axis=-1)),
                _pad(self.beam_predicted[0]),
                _pad(self.beam_plus[0])
            ],
            axis=1
        )

        self.first_scal = self.metrics.text('eval/first_txt', mdl.input.dictionary.lookup(vis_concat))

        self.reset_metrics = self.metrics.reset()
        self.update_metrics = self.metrics.update()
        self.report = self.metrics.summary()

        self.files = tf.matching_files(cfg.get_string('pattern'))
        self.pipeline = tf.data.TFRecordDataset(
            filenames=self.files,
            compression_type=cfg.get('compression', ''),
            num_parallel_reads=1
        ).batch(
            batch_size=cfg.get_int('batch', 16)
        ).map(
            map_func=_parse_batch
        ).prefetch(4)

        self.iterator = self.pipeline.make_initializable_iterator()

    def run(self, sess: tf.Session):
        start = time.time()
        if self.no_eval:
            return

        sess.run([self.iterator.initializer, self.reset_metrics])
        feed_dict = {self.input_ref: sess.run(self.iterator.string_handle())}

        try:
            while True:
                sess.run(self.update_metrics, feed_dict=feed_dict)
        except tf.errors.OutOfRangeError:
            pass
        print("Done eval in %.3f secs" % (time.time() - start))

    def summaries(self, sess: tf.Session, global_step, writer: tf.summary.FileWriter):
        handle, _ = sess.run([self.iterator.string_handle(), self.iterator.initializer])
        objs = sess.run(self.report, feed_dict={self.input_ref: handle})
        writer.add_summary(objs, global_step)
        writer.flush()


class Ctx2Cloze(object):
    def __init__(self, cfg: pyhocon.ConfigTree):
        self.cfg = cfg
        self.snap_dir = cfg.get_string('snapshot_full')
        with tf.Graph().as_default() as g:
            self.graph = g
            tf.set_random_seed(cfg.get_int('seed'))
            self.train_pipe = input_dataset(cfg.get_config('input.train'))

            t2cfg = cfg.get_config('input.train2', None)
            if t2cfg is not None:
                self.train_pipe2 = input_dataset(t2cfg)
            else:
                self.train_pipe2 = None

            self.dic_obj = Dictionary(cfg.get_string('dictionary.file'))
            inp = self.inp = ModelInput(self.dic_obj, self.train_pipe)
            mdl = self.mdl = Model(inp, cfg)
            self.train_ctx_dec = StateDecoder(cfg.get_config('decoder'), mdl, mdl.ctx_dec_input, inp.cloze,
                                              inp.cloze_len)
            self.train_clz_dec = StateDecoder(cfg.get_config('decoder'), mdl, mdl.clz_dec_input, inp.cloze,
                                              inp.cloze_len)
            advtype = cfg.get_string('advnet.type', 'none')
            if advtype == 'norm':
                self.adv = AdvNorm(mdl.global_step, cfg.get_config('advnet'), mdl.clz_dec_input, mdl.ctx_dec_input)
            elif advtype == 'wass':
                self.adv = AdvWass(mdl.global_step, cfg.get_config('advnet'), mdl.clz_dec_input, mdl.ctx_dec_input)
            else:
                self.adv = NoAdv(mdl.global_step, cfg.get_config('advnet'), mdl.clz_dec_input, mdl.ctx_dec_input)
            print("Using advnet=", advtype, self.adv)
            self.eval = Eval(cfg, mdl, self.train_ctx_dec)
            self.trainer = Trainer(cfg, mdl, self.train_ctx_dec, self.train_clz_dec, self.adv)

            self.summaries = Summaries(inp, mdl, self.train_ctx_dec, self.train_clz_dec, self.adv, self.trainer)

            self.sm = tf.train.SessionManager(
                local_init_op=tf.local_variables_initializer()
            )

            self.saver = tf.train.Saver()

            self.sess = self.sm.prepare_session(
                master=cfg.get_string('master.name', ''),
                init_op=[tf.global_variables_initializer()],
                saver=self.saver,
                checkpoint_dir=self.snap_dir
            )

            self.sess.run(tf.tables_initializer())

            self.summwr = tf.summary.FileWriter(
                logdir=self.snap_dir,
                session=self.sess
            )

    def train(self):
        with self.graph.as_default():
            self._train()

    def _train(self):
        os.makedirs(self.snap_dir, exist_ok=True)
        with open(f'{self.snap_dir}/dump.conf', 'wt', encoding='utf-8') as conf_fd:
            conf_fd.write(pyhocon.HOCONConverter.to_hocon(self.cfg))

        # Register Embedding for projector
        from tensorboard.plugins import projector
        conf = projector.ProjectorConfig()
        embedding = conf.embeddings.add()
        embedding.tensor_name = self.mdl.vocab_embed.name
        embedding.metadata_path = self.cfg.get('dictionary.file')
        projector.visualize_embeddings(self.summwr, conf)

        train_feed = self.inp.feed_dict(self.sess, self.train_pipe.make_one_shot_iterator())

        t2run_per = self.cfg.get_int('input.train2.freq', 0)
        if self.train_pipe2 is not None:
            train_feed2_iter = self.train_pipe2.make_one_shot_iterator()
            train_feed2 = {self.inp.handle: self.sess.run(train_feed2_iter.string_handle())}
            print("Using second input pipeline per", t2run_per, file=sys.stderr)
        else:
            train_feed2 = None
            t2run_per = 0

        last_snap = time.monotonic()
        last_summary = last_snap
        last_eval = last_snap

        summary_secs = self.cfg.get_float('save.summary', 30)
        snashot_secs = self.cfg.get_float('save.snapshot', 10 * 60)
        eval_secs = self.cfg.get_float('eval.time', 3600)

        print(f"Saving summary per {summary_secs} secs and snapshots per {snashot_secs} secs")

        summary_op = tf.summary.merge_all()
        step = 0

        try:
            while True:
                cur_time = time.monotonic()

                launch = {'train': self.trainer.train_op, 'step': self.trainer.global_step}

                # if step < 1000:
                #     launch['train'] = self.trainer.capped_train_op

                normal_run = True

                if t2run_per != 0 and step % t2run_per == 0:
                    normal_run = False
                    feed = train_feed2
                else:
                    feed = train_feed

                if normal_run and (cur_time - last_summary) > summary_secs:
                    launch['summary'] = summary_op
                    last_summary = cur_time

                if normal_run and (cur_time - last_snap) > snashot_secs:
                    launch['loss'] = self.trainer.full_loss

                result = self.sess.run(launch, feed_dict=feed)

                summary_val = result.get('summary', None)
                step = result.get('step')

                if summary_val is not None:
                    self.summwr.add_summary(summary_val, step)

                if normal_run and (cur_time - last_snap) > snashot_secs:
                    last_snap = cur_time
                    print(f'saving snapshots at step {step} - loss: {result["loss"]}')
                    self.saver.save(self.sess, self.snap_dir + "/snap", step)

                if (cur_time - last_eval) > eval_secs:
                    self.eval.run(self.sess)
                    self.eval.summaries(self.sess, step, self.summwr)
                    last_eval = cur_time

        except tf.errors.OutOfRangeError:
            print("End of input: saving")
            self.saver.save(self.sess, self.snap_dir + "/snap", step)

    def infer_ctx2cloze(self):
        with self.graph.as_default():
            return Ctx2ClozeSampling(self)

    def infer_ctxcloze_cosine(self):
        with self.graph.as_default():
            return Ctx2ClozeCosine(self)


class Ctx2ClozeSampling(object):
    def __init__(self, c2c: Ctx2Cloze):
        self.c2c = c2c
        cfg = c2c.cfg
        mdl = c2c.mdl
        dec = c2c.train_ctx_dec

        self.batch_size = tf.shape(mdl.input.context_len)

        temp = cfg.get_float('sample_temp', None)
        if temp is None:
            self.helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
                embedding=mdl.vocab_embed,
                start_tokens=tf.tile([cfg.get_int('bos_id')], self.batch_size),
                end_token=cfg.get_int('eos_id')
            )
        else:
            self.helper = tf.contrib.seq2seq.SampleEmbeddingHelper(
                embedding=mdl.vocab_embed,
                start_tokens=tf.tile([cfg.get_int('bos_id')], self.batch_size),
                end_token=cfg.get_int('eos_id'),
                softmax_temperature=temp,
                seed=cfg.get_int('seed')
            )

        beam_width = cfg.get_int('beam', 5)

        self.input_type = cfg.get_string('input_type', 'ctx')

        if self.input_type == 'cloze':
            input_state = mdl.clz_dec_input
        else:
            input_state = mdl.ctx_dec_input

        state_c = tf.contrib.seq2seq.tile_batch(input_state.c, beam_width)
        state_h = tf.contrib.seq2seq.tile_batch(input_state.h, beam_width)

        self.init_state = tf.nn.rnn_cell.LSTMStateTuple(
            state_c,
            state_h
        )

        self.decoder = tf.contrib.seq2seq.BeamSearchDecoder(
            cell=dec.rnn_cell,
            embedding=mdl.vocab_embed,
            start_tokens=tf.tile([cfg.get_int('bos_id')], self.batch_size),
            end_token=cfg.get_int('eos_id'),
            initial_state=self.init_state,
            beam_width=beam_width,
            output_layer=mdl.decoder_layer
        )

        self.outputs, self.final_state, self.lengths = tf.contrib.seq2seq.dynamic_decode(
            decoder=self.decoder,
            output_time_major=False,
            impute_finished=False,
            maximum_iterations=dec.max_seq_len,
            scope=dec.scope
        )

        self.lookup = tf.contrib.lookup.index_table_from_tensor(
            mapping=c2c.dic_obj.words,
            default_value=4
        )

        c2c.sess.run(self.lookup.init)

        def _convert_data(context_full, cloze_full):
            with tf.device('/cpu:0'):
                context = tf.string_split(tf.reshape(context_full, [1]), skip_empty=False)
                cloze = tf.string_split(tf.reshape(cloze_full, [1]), skip_empty=False)
                context = self.lookup.lookup(context.values)
                cloze = self.lookup.lookup(cloze.values)
                ctxLen = tf.shape(context)[0]
                clzLen = tf.shape(cloze)[0]
            return {
                'context': tf.to_int32(tf.reshape(context, [-1])),
                'cloze': tf.to_int32(tf.reshape(cloze, [-1])),
                'contextLength': ctxLen,
                'clozeLength': clzLen
            }

        def _transpose(x):
            return {
                'context': tf.transpose(x['context']),
                'cloze': tf.transpose(x['cloze']),
                'contextLength': x['contextLength'],
                'clozeLength': x['clozeLength']
            }

        self.dataset = tf.contrib.data.CsvDataset(
            filenames=tf.matching_files(cfg.get_string('files')),
            field_delim='\t',
            select_cols=[0, 1],
            record_defaults=[
                tf.string,
                tf.string
            ]
        ).map(
            map_func=_convert_data
        ).padded_batch(
            batch_size=cfg.get_int('batch', 32),
            padded_shapes={
                'context': [None],
                'cloze': [None],
                'contextLength': [],
                'clozeLength': []
            }
        ).map(
            map_func=_transpose
        )

        self.iterator = self.dataset.make_initializable_iterator()

    def run(self):
        with self.c2c.graph.as_default():
            try:
                self._run()
            except tf.errors.OutOfRangeError:
                return

    def _run(self):
        sess = self.c2c.sess

        sess.run(self.iterator.initializer)
        handle = sess.run(self.iterator.string_handle())
        feed_dict = {self.c2c.inp.handle: handle}

        if self.input_type == 'cloze':
            input_tens = self.c2c.inp.cloze
        else:
            input_tens = self.c2c.inp.context

        fetches = {
            'input': self.c2c.dic_obj.lookup(input_tens),
            'output': self.c2c.dic_obj.lookup(self.outputs.predicted_ids)
        }

        # print(sess.run(tf.shape_n([self.c2c.inp.context_len, self.c2c.inp.context]), feed_dict=feed_dict))

        while True:
            res = sess.run(fetches, feed_dict=feed_dict)
            self.print_values(res)

    def print_values(self, res):
        inputs = res['input']
        outputs = res['output']

        for i in range(inputs.shape[1]):
            input_data = inputs[:, i]
            print('##', i, ' '.join(x.decode('utf-8') for x in input_data))

            output_data = outputs[i]
            for j in range(output_data.shape[1]):
                output = output_data[:, j]
                print(' '.join(x.decode('utf-8') for x in output))


class Ctx2ClozeCosine(object):
    def __init__(self, c2c: Ctx2Cloze):
        self.c2c = c2c
        cfg = c2c.cfg

        self.lookup = tf.contrib.lookup.index_table_from_tensor(
            mapping=c2c.dic_obj.words,
            default_value=4
        )

        c2c.sess.run(self.lookup.init)

        def _convert_data(context_full, cloze_full):
            with tf.device('/cpu:0'):
                context = tf.string_split(tf.reshape(context_full, [1]), skip_empty=False)
                cloze = tf.string_split(tf.reshape(cloze_full, [1]), skip_empty=False)
                context = self.lookup.lookup(context.values)
                cloze = self.lookup.lookup(cloze.values)
                ctxLen = tf.shape(context)[0]
                clzLen = tf.shape(cloze)[0]
            return {
                'context': tf.to_int32(tf.reshape(context, [-1])),
                'cloze': tf.to_int32(tf.reshape(cloze, [-1])),
                'contextLength': ctxLen,
                'clozeLength': clzLen
            }

        def _transpose(x):
            return {
                'context': tf.transpose(x['context']),
                'cloze': tf.transpose(x['cloze']),
                'contextLength': x['contextLength'],
                'clozeLength': x['clozeLength']
            }

        self.dataset = tf.contrib.data.CsvDataset(
            filenames=tf.matching_files(cfg.get_string('files')),
            field_delim='\t',
            select_cols=[0, 1],
            record_defaults=[
                tf.string,
                tf.string
            ]
        ).map(
            map_func=_convert_data
        ).padded_batch(
            batch_size=cfg.get_int('batch', 32),
            padded_shapes={
                'context': [None],
                'cloze': [None],
                'contextLength': [],
                'clozeLength': []
            }
        ).map(
            map_func=_transpose
        )

        self.iterator = self.dataset.make_initializable_iterator()

        self.clozes = []
        self.contexts = []
        self.cosine = []

    def run(self):
        with self.c2c.graph.as_default():
            try:
                self._run()
            except tf.errors.OutOfRangeError:
                self.print_values()

    def _run(self):
        sess = self.c2c.sess

        self.clozes = []
        self.contexts = []
        self.cosine = []

        sess.run(self.iterator.initializer)
        handle = sess.run(self.iterator.string_handle())
        feed_dict = {self.c2c.inp.handle: handle}

        inp = self.c2c.inp

        fetches = {
            'context': self.c2c.dic_obj.lookup(inp.context),
            'cloze': self.c2c.dic_obj.lookup(inp.cloze),
            'cosine': self.c2c.summaries.dotprods
        }

        # print(sess.run(tf.shape_n([self.c2c.inp.context_len, self.c2c.inp.context]), feed_dict=feed_dict))

        while True:
            res = sess.run(fetches, feed_dict=feed_dict)
            self.accumulate_values(res)

    def print_values(self):
        order = np.argsort(self.cosine)

        for i in order:
            ctx = self.contexts[i]
            clz = self.clozes[i]
            cosine = self.cosine[i]
            print("%.3f\t%s\t%s" % (cosine, ctx, clz))

    def accumulate_values(self, res):
        ctx = res['context']
        clz = res['cloze']
        dists = res['cosine']

        for i in range(ctx.shape[1]):
            ctx_data = ctx[:, i]
            clz_data = clz[:, i]
            cos = dists[i]

            self.contexts.append(' '.join(x.decode('utf-8') for x in ctx_data))
            self.clozes.append(' '.join(x.decode('utf-8') for x in clz_data))
            self.cosine.append(cos)


def main():
    cfg = parse_config_args()
    c2c = Ctx2Cloze(cfg)

    infer = cfg.get_string('infer', None)
    if infer == 'text':
        inf = c2c.infer_ctx2cloze()
        inf.run()
    elif infer == 'dist':
        inf = c2c.infer_ctxcloze_cosine()
        inf.run()
    else:
        c2c.train()


if __name__ == '__main__':
    np.set_printoptions(
        precision=3,
        linewidth=150
    )
    main()
