#!/usr/bin/env python3

import sys

with open(sys.argv[1], encoding='utf-8') as f:
    nid = 1
    cst = 0
    for line in f:
        if len(line) < 1:
            continue

        line = line.rstrip()
        if line[0] == '#':
            print(line)
            continue
        elif line[0] == '+' or line[0] == '*':
            continue
        elif line == "EOS":
            cst = 0
            nid = 1
            print("EOS")
            continue

        parts = line.split(' ')

        cen = cst + len(parts[0]) - 1

        jppparts = [
            "-",
            nid,
            nid - 1,
            cst,
            cen,
            parts[0],
            "*"  # no daihyo
        ]

        jppparts.extend(parts[1:11])
        jppparts.append("ランク:1")

        print("\t".join(str(x) for x in jppparts))
        cst = cen + 1
        nid += 1
