import tensorflow as tf
import sys

def shuffle_data(inputs, lower_bound=5000, capacity=None, seed=None, shapes=None):
    if capacity is None:
        capacity = lower_bound * 2

    dtypes = []
    for i in inputs:
        dtypes.append(i.dtype)

    q = tf.RandomShuffleQueue(capacity, lower_bound, dtypes, seed=seed, shapes=shapes)
    qop = q.enqueue(inputs)
    tf.train.add_queue_runner(tf.train.QueueRunner(queue=q, enqueue_ops=[qop]))
    return q.dequeue_up_to(5)

filename = tf.constant(sys.argv[1])
deq = tf.train.string_input_producer([filename])
opts = tf.python_io.TFRecordOptions(
    tf.python_io.TFRecordCompressionType.GZIP
)
reader = tf.TFRecordReader(options=opts)
key, data = reader.read(deq)
keys, datas = tf.train.shuffle_batch([key, data], 8, 10000, 8000)

parsed = tf.parse_example(datas, {
    'words': tf.VarLenFeature(tf.int64),
    'answer': tf.FixedLenFeature([1], tf.int64),
    'len': tf.FixedLenFeature([1], tf.int64)
})

sentSparse = parsed["words"]
answer = parsed["answer"]

complete = tf.sparse_tensor_to_dense(sentSparse, validate_indices=False)

#sent = sentSparse.values

#sx, sy = shuffle_data([sent, answer], seed=0xdeadbeef)

#theinput = tf.train.batch([sx, tf.shape(sx), sy], 8, dynamic_pad=True, shapes=[[None], [1], [1]])


init = tf.global_variables_initializer()

with tf.Session() as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord, sess=sess)
    sess.run(init)

    try:
        # while not coord.should_stop():
        #     sess.run(training_op)
        print(sess.run([complete, parsed["len"]]))
    except tf.errors.OutOfRangeError:
        print("Done training!")
    finally:
        coord.request_stop()

    coord.join(threads)
