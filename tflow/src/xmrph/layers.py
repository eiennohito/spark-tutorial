import model.transformer as trf
from pyhocon import ConfigTree as Config
import tensorflow as tf

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .model import MorphModel


class Layer(object):
    def __init__(self, cfg: Config, model: 'MorphModel', input, name):
        self.cfg = cfg
        self.input = input
        self.model = model
        self.name = name
        self.debug = cfg.get_bool('debug', model.debug)
        self.length_handler = model.len_handler
        self.concat_handled = False

    def dropout_prob(self, model, cfg, name):
        if model.infer:
            return 1.0
        return cfg.get_float(name, 1.0)


class TransformerLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        tagging = isinstance(input, list)

        if tagging:
            self.output = []

            for i, x in enumerate(input):
                impl = self.make_transformer(cfg, f't{i}', model)
                self.output.append(impl.output)

            tagwise = cfg.get_int('tagwise', None)

            if tagwise is not None:
                xinput = tf.concat(self.output, axis=-1)

                tagwise_act = cfg.get_string('tagwise_activation', 'selu')
                tagwise_bias = cfg.get_bool('tagwise_bias', True)
                tagwise_act = trf.tf_activation(tagwise_act)
                self.tagwise_in = tf.layers.dense(
                    inputs=xinput,
                    units=tagwise,
                    activation=tagwise_act,
                    use_bias=tagwise_bias,
                    name='tagwise_in'
                )

                tagwise_dims = [x.shape[-1] for x in input]
                tagwise_alldims = sum(tagwise_dims)
                self.tagwise_out = tf.layers.dense(
                    inputs=self.tagwise_in,
                    units=tagwise_alldims,
                    activation=tagwise_act,
                    use_bias=tagwise_bias,
                    name='tagwise_out'
                )

                tagwise_splitted = tf.split(self.tagwise_out, tagwise_dims, axis=2)

                self.output = [(x + y) for x, y in zip(self.output, tagwise_splitted)]
        else:
            self.impl = self.make_transformer(cfg, name, model, input)
            self.output = self.impl.output

    def make_transformer(self, cfg: Config, name, model, input):
        d = model.data
        dim = [d.batch_size, 1, 1, self.length_handler.batch_mlen]
        mask = tf.reshape(self.length_handler.length_softmax_mask, dim)
        impl = trf.TransformerUnit(
            input=input,
            number_attn=(cfg.get_int('num_heads', 8)),
            inner_dim=cfg.get_int('inner', 16),
            key_dim=cfg.get_int('key', 16),
            proj_dim=cfg.get_int('proj', 16),
            attn_keep=self.dropout_prob(model, cfg, 'attn_keep'),
            mask=mask,
            normalize=cfg.get_bool('normalize', True),
            keep_layer=self.dropout_prob(model, cfg, 'keep_layer'),
            activation=cfg.get_string('activation', 'selu'),
            oactivation=cfg.get_string('oactivation', 'selu'),
            ak_bias=cfg.get_bool('ak_bias', False),
            ak_act=cfg.get_string('ak_act', None),
            av_bias=cfg.get_bool('av_bias', False),
            av_act=cfg.get_string('av_act', None),
            ao_bias=cfg.get_bool('ao_bias', False),
            ao_act=cfg.get_string('ao_act', None),
            gating=cfg.get_bool('gate', False)
        )

        if self.debug:
            import input.imager as imgr
            imger = imgr.Imager()
            tf.summary.histogram(f'raw_attn', impl.attn.AttnWeights, family=name)
            num_heads = cfg.get_int('num_heads', 8)
            attn0 = impl.attn.Attn[0]
            if impl.gating:
                tf.summary.histogram('gate', impl.gate_values, family=name)
                gate0 = impl.gate_values[0]
                mlen = trf.mlen(input)
                gate0x = tf.reshape(gate0, [1, mlen, 1])
                gate0x = tf.tile(gate0x, [num_heads, 1, 1])
                attn0 = tf.concat([gate0x, attn0], axis=-1)
            tf.summary.image(f'attn', imger.to_image_norm(attn0), max_outputs=(
                num_heads), family=name)

        return impl


class DenseLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        split = cfg.get_bool('split', False)
        size = cfg.get_int('size', input.shape[-1].value)

        if split:
            size *= model.num_taggers

        self.impl = tf.layers.Dense(
            units=size,
            activation=trf.tf_activation(cfg.get_string('activation', 'selu')),
            use_bias=cfg.get_bool('bias', True),
            name=name
        )

        self.output = self.impl(input)

        if split:
            self.output = tf.split(self.output, model.num_taggers, axis=2)


class DoubleLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        self.output = tf.concat([input, input], axis=-1)
        if cfg.get_bool('keep_length', False):
            self.output *= 2.0 ** -0.5


class RnnLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        self.size = cfg.get_int('size', input.shape[-1].value)
        self.bidi = cfg.get_bool('bidi', True)

        if self.bidi:
            cell_size = self.size // 2

            self.rnn_out, self.rnn_state = tf.nn.bidirectional_dynamic_rnn(
                cell_fw=self.cell(cell_size + self.size % 2, "fwd"),
                cell_bw=self.cell(cell_size, "bwd"),
                inputs=input,
                sequence_length=self.length_handler.length,
                swap_memory=True,
                time_major=False,
                dtype=input.dtype,
                scope=tf.get_variable_scope()
            )

            self.output = tf.concat(self.rnn_out, 2)
        else:
            self.rnn_out, self.rnn_state = tf.nn.dynamic_rnn(
                cell=self.cell(self.size),
                inputs=input,
                sequence_length=self.length_handler.length,
                swap_memory=True,
                time_major=False,
                dtype=input.dtype,
                scope=tf.get_variable_scope()
            )

            self.output = self.rnn_out

        if cfg.get_bool('residual', False):
            insize = input.shape[-1].value

            if insize != self.size:
                projected = tf.layers.dense(
                    inputs=self.output,
                    units=insize,
                    activation=tf.nn.selu
                )
            else:
                projected = self.output

            self.output = input + projected

    def cell(self, size, name='cell'):
        ctype = self.cfg.get_string('cell', 'lstm')

        if ctype == 'lstm':
            return tf.contrib.rnn.LSTMBlockCell(
                num_units=size,
                use_peephole=self.cfg.get_bool('lstm_peephole', True),
                name=name
            )
        elif ctype == 'gru':
            return tf.contrib.rnn.GRUBlockCell(
                num_units=size,
                name=name
            )
        elif ctype == 'lnlstm':
            return tf.contrib.rnn.LayerNormBasicLSTMCell(
                num_units=size,
                layer_norm=self.cfg.get_bool('use_norm', True),
                dropout_keep_prob=self.dropout_prob(self.model, self.cfg, 'keep_layer')
            )
        else:
            raise Exception("Unknown cell type:", ctype)


class ConvLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        widths = cfg.get_list('widths')
        sizes = cfg.get_list('sizes')

        out_size = sum(sizes)

        assert out_size > 0
        d = model.data

        reshaped_input = tf.reshape(input, [d.batch_size, self.length_handler.batch_mlen, 1, -1])
        idim = input.shape[-1].value

        outs = []

        for w, s, i in zip(widths, sizes, range(len(sizes))):
            assert w % 2 == 1

            filter = tf.get_variable(
                name=f'filter_{i}',
                shape=[w, 1, idim, s],
                dtype=input.dtype,
                initializer=tf.random_normal_initializer(
                    dtype=input.dtype
                )
            )

            npad = w // 2
            xinput = tf.pad(reshaped_input, [[0, 0], [npad, npad], [0, 0], [0, 0]])

            xout = tf.nn.conv2d(
                input=xinput,
                filter=filter,
                strides=[1, 1, 1, 1],
                padding='VALID',
                name=f'conv_{i}'
            )

            xout = tf.reshape(xout, [d.batch_size, self.length_handler.batch_mlen, s])

            outs.append(xout)

        if len(outs) == 1:
            self.output = outs[0]
        else:
            self.output = tf.concat(outs, axis=2)

        act = trf.tf_activation(cfg.get_string('activation', None))
        if act is not None:
            self.output = act(self.output)

        with tf.variable_scope(name):
            if cfg.get_bool('residual', False):
                if idim != out_size:
                    projected = tf.layers.dense(
                        inputs=self.output,
                        units=idim,
                        activation=tf.nn.selu
                    )
                else:
                    projected = self.output

                self.output = input + projected


class PosEmbeddingLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        size = cfg.get_int('size', None)

        if size is None:
            size = input.shape[-1].value

            self.pemb = trf.PositionalEmbedding(size, model.max_length)
            embeds = self.pemb.embeds(self.length_handler.batch_mlen, model.data.batch_size)
            self.output = input + embeds
        else:
            cur_size = input.shape[-1].value
            self.pemb = trf.PositionalEmbedding(size, model.max_length)
            embeds = self.pemb.embeds(self.length_handler.batch_mlen, model.data.batch_size)
            pad = size - cur_size
            if pad < 0:
                raise Exception("Invalid positional embedding size:", name, size, cur_size)
            if pad > 0:
                input = tf.pad(input, [[0, 0], [0, 0], [pad, 0]])
            self.output = input + embeds


class NoopLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        self.output = input


class SegmentTokensLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        self.target = cfg.get_string('target')
        self.tag = model.taggers.get(self.target, None)
        if self.tag is None:
            raise KeyError("Tagger with name", self.target, "was not yet created")
        tag_preds = tf.equal(self.tag.answers, 1)

        # Make value mask
        handler = self.length_handler
        smask = tf.sequence_mask(handler.length - 2, maxlen=handler.batch_mlen - 1)
        zeros = tf.zeros([model.data.batch_size, 1], dtype=tf.bool)
        smask = tf.concat([zeros, smask], axis=1)
        filled = tf.logical_or(tf.logical_not(smask), tag_preds)
        self.ones = tf.to_int32(filled)
        self.guessed_mask = tf.logical_and(smask, tag_preds)
        self.num_words = tf.reduce_sum(tf.to_int32(self.guessed_mask), axis=-1)
        self.num_tokens = self.num_words + 2
        self.input_shape = tf.shape(input)[:2]

        full_mask = tf.sequence_mask(handler.length)
        self.full_indices = tf.where(full_mask)
        self.full_indices = tf.to_int32(self.full_indices)
        gathered_indicators = tf.gather_nd(self.ones, self.full_indices)

        self.token_ids = tf.cumsum(gathered_indicators, axis=0, exclusive=False) - 1
        presegment = tf.reshape(input, [handler.batch_mlen * model.data.batch_size, -1])
        flat_indices = tf.multiply(self.full_indices, [handler.batch_mlen, 1])
        flat_indices = tf.reduce_sum(flat_indices, axis=1)
        segmented_raw = tf.sparse_segment_mean(presegment, flat_indices, self.token_ids)

        small_mask = tf.sequence_mask(self.num_tokens)
        self.small_indices = tf.to_int32(tf.where(small_mask))

        oshape = tf.concat([tf.shape(small_mask), [input.shape[-1].value]], axis=0)
        self.output = tf.scatter_nd(self.small_indices, segmented_raw, oshape)

        # Implement length handling
        self.length_handler = self
        self.length = self.num_tokens
        self.batch_mlen = oshape[1]
        self.len_bool_mask = tf.sequence_mask(self.length)
        over_len = tf.logical_not(self.len_bool_mask)
        self.length_softmax_mask = -1e9 * tf.to_float(over_len)

        # self.output = tf.Print(self.input, [
        #     self.ones,
        #     gathered_indicators,
        #     self.token_ids,
        #     self.num_tokens,
        #     tf.shape(segmented_raw),
        #     oshape
        # ], first_n=-1, summarize=10000)

        self.previous = model.len_handler

    def scatter(self, x):
        # Input is B x L-out x ...
        # step 1: gather data to flat array
        flat_x = tf.gather_nd(x, self.small_indices)
        # step 2: additionally mutliply data
        flat_unsegmented = tf.gather(flat_x, self.token_ids)
        # step 3: construct output (of initial size)
        oshape = tf.concat([self.input_shape, tf.shape(x)[2:]], axis=0)
        y = tf.scatter_nd(self.full_indices, flat_unsegmented, oshape)
        return self.previous.scatter(y)


class SelfAttentionSegment(trf.BaseSelfAttention):
    def __init__(self, i_all, i_st, number_attn, inner_dim, key_dim, out_dim, name=None, keep_prob=1.0, mask=None,
                 k_bias=False, k_act=None, v_bias=False, v_act=None, o_bias=False, o_act=None):
        super().__init__(i_st, i_all, i_all, number_attn, inner_dim, key_dim, name, keep_prob,
                         k_bias, k_act, v_bias, v_act)
        with tf.variable_scope(name, default_name='attn'):
            if mask is not None:
                self.AttnWeightsMasked = self.AttnWeights + mask
            else:
                self.AttnWeightsMasked = self.AttnWeights
            self.Attn = tf.nn.softmax(self.AttnWeightsMasked, axis=2)

            self.Attended = tf.einsum('bhas,bahd->bshd', self.Attn, self.Vx)
            self.Ax = tf.reshape(self.Attended, [self.batch_size, trf.mlen(i_st), number_attn * inner_dim])

            self.output = tf.layers.dense(
                inputs=self.Ax,
                units=out_dim,
                use_bias=o_bias, activation=trf.tf_activation(o_act),
                name='O'
            )


class TransformerSegmentLayer(Layer):
    def __init__(self, cfg: Config, model, input, name):
        super().__init__(cfg, model, input, name)
        self.concat_handled = True

        self.target = cfg.get_string('target')
        self.tag = model.taggers.get(self.target, None)
        if self.tag is None:
            raise KeyError("Tagger with name", self.target, "was not yet created")
        tag_hits = tf.equal(self.tag.answers, 1)

        # Make value mask
        handler = self.length_handler

        # Mask for EOS tags (always get them)
        # We append mask for BOS (always 0), so -2
        emask_1 = tf.sequence_mask(handler.length - 2, maxlen=handler.batch_mlen - 1)
        emask_2 = tf.sequence_mask(handler.length - 1, maxlen=handler.batch_mlen - 1)
        endmask = tf.logical_xor(emask_1, emask_2)
        begmask = tf.ones([model.data.batch_size, 1], dtype=tf.bool)
        sentmask = tf.concat([begmask, endmask], axis=1)
        full_mask = tf.sequence_mask(handler.length)
        masked_hits = tf.logical_and(tag_hits, full_mask)  # discard invalid
        masked_hits = tf.logical_or(masked_hits, sentmask)  # fix BOS/EOS

        self.guessed_mask = masked_hits
        self.ones = tf.to_int32(masked_hits)
        self.num_tokens = tf.reduce_sum(self.ones, axis=-1)
        self.max_tokens = tf.reduce_max(self.num_tokens)

        self.full_indices = tf.where(full_mask)
        self.full_indices = tf.to_int32(self.full_indices)
        gathered_indicators = tf.gather_nd(self.ones, self.full_indices)
        self.token_ids = tf.cumsum(gathered_indicators, axis=0, exclusive=False) - 1

        guessed_idxes = tf.where(self.guessed_mask)
        guessed_idxes = tf.to_int32(guessed_idxes)
        beg_vecs = tf.gather_nd(input, guessed_idxes)

        small_mask = tf.sequence_mask(self.num_tokens)
        self.small_indices = tf.where(small_mask)

        input_dim = input.shape[2].value
        shaped_starts = tf.scatter_nd(
            indices=self.small_indices,
            updates=beg_vecs,
            shape=tf.to_int64([model.data.batch_size, self.max_tokens, input_dim]),
        )

        shaped_starts.set_shape([None, None, input_dim])

        attn_mask = tf.reshape(handler.length_softmax_mask, [model.data.batch_size, 1, handler.batch_mlen, 1])

        out_dim = cfg.get_int('size', input_dim)
        self.impl = SelfAttentionSegment(
            i_all=input,
            i_st=shaped_starts,
            number_attn=cfg.get_int('num_heads', 4),
            inner_dim=cfg.get_int('inner', 16),
            key_dim=cfg.get_int('key', 16),
            out_dim=out_dim,
            keep_prob=self.dropout_prob(model, cfg, 'attn_keep'),
            mask=attn_mask,
            k_bias=cfg.get_bool('ak_bias', False),
            k_act=cfg.get_string('ak_act', None),
            v_bias=cfg.get_bool('av_bias', False),
            v_act=cfg.get_string('av_act', None),
            o_bias=cfg.get_bool('ao_bias', False),
            o_act=cfg.get_string('ao_act', None),
            name='attn'
        )

        if self.debug:
            import input.imager as imgr
            imger = imgr.Imager()
            tf.summary.histogram(f'raw_attn', self.impl.AttnWeights, family=name)
            tf.summary.image(f'attn', imger.to_image_norm(self.impl.Attn[0]), max_outputs=(
                cfg.get_int('num_heads', 4)), family=name)

        output = self.impl.output

        mode = cfg.get_string('mode', None)
        proj_dim = cfg.get_int('proj', None)

        if proj_dim is not None:
            proj_h = tf.layers.dense(
                output,
                proj_dim,
                activation=trf.tf_activation(cfg.get_string('activation', 'relu')),
                name='proj_in'
            )
            if mode == 'resid' and input_dim != out_dim:
                proj_odim = input_dim
            else:
                proj_odim = out_dim
            output = tf.layers.dense(
                proj_h,
                proj_odim,
                activation=trf.tf_activation(cfg.get_string('oactivation', 'relu')),
                name='proj_out'
            )

        layer_kprob = self.dropout_prob(model, cfg, 'keep_layer')
        lnorm = cfg.get_bool('normalize', True)
        output = tf.nn.dropout(output, layer_kprob)

        if mode is None:
            raise RuntimeError('You must specify mode for TransformSegmentLayer')
        elif mode == 'concat':
            if lnorm:
                output = trf.layer_norm(output)
            self.output = tf.concat([shaped_starts, output], axis=2)
        elif mode == 'resid':
            if input_dim != out_dim and proj_dim is not None:
                output = tf.layers.dense(output, input_dim, name='out_proj')
            self.output = shaped_starts + output
            if lnorm:
                self.output = trf.layer_norm(self.output)
        elif mode == 'noop':
            if lnorm:
                output = trf.layer_norm(output)
            self.output = output
        else:
            raise RuntimeError("Invalid mode:", mode)

        # Implement length handling
        self.length_handler = self
        self.length = self.num_tokens
        self.batch_mlen = self.max_tokens
        self.len_bool_mask = tf.sequence_mask(self.length)
        over_len = tf.logical_not(self.len_bool_mask)
        self.length_softmax_mask = -1e9 * tf.to_float(over_len)

        self.input_shape = tf.shape(input)[:2]

        # self.output = tf.Print(self.input, [
        #     self.ones,
        #     gathered_indicators,
        #     self.token_ids,
        #     self.num_tokens,
        #     tf.shape(segmented_raw),
        #     oshape
        # ], first_n=-1, summarize=10000)

        self.previous = model.len_handler

    def scatter(self, x):
        # Input is B x L-out x ...
        # step 1: gather data to flat array
        flat_x = tf.gather_nd(x, self.small_indices)
        # step 2: additionally mutliply data
        flat_unsegmented = tf.gather(flat_x, self.token_ids)
        # step 3: construct output (of initial size)
        oshape = tf.concat([self.input_shape, tf.shape(x)[2:]], axis=0)
        y = tf.scatter_nd(self.full_indices, flat_unsegmented, oshape)
        y.set_shape([None, None] + x.shape.as_list()[2:])
        return self.previous.scatter(y)


class Layers(object):
    registry = {
        'tform': TransformerLayer,
        'dense': DenseLayer,
        'double': DoubleLayer,
        'rnn': RnnLayer,
        'pemb': PosEmbeddingLayer,
        'conv': ConvLayer,
        'noop': NoopLayer,
        'segment': SegmentTokensLayer,
        'tfseg': TransformerSegmentLayer,
    }

    @staticmethod
    def for_name(name):
        return Layers.registry[name]
