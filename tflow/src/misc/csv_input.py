import tensorflow as tf

tf.enable_eager_execution()

table = tf.contrib.lookup.index_table_from_file("...")


def make_boundaries(start, end, increase):
    value = int(start)
    result = [value]
    while value < end:
        value = int(value * increase)
        result.append(value)
    return result


def make_batch_sizes(num_data, boundaries):
    result = []
    for x in boundaries:
        result.append(int(num_data / x) + 1)
    result.append(1)
    return result


def read_files(files) -> tf.data.Dataset:
    files_dataset = tf.data.Dataset.from_tensor_slices(files)
    files_dataset = files_dataset.repeat(1)

    def csv_dataset(fname):
        return tf.data.experimental.CsvDataset(
            filenames=fname,
            record_defaults=[tf.string, tf.string],
            field_delim='\t'
        )

    lines = files_dataset.apply(
        tf.data.experimental.parallel_interleave(
            map_func=csv_dataset,
            cycle_length=4,
            sloppy=True
        )
    )

    def to_ids(data):
        s1 = tf.strings.split(tf.reshape(data[0], [1], sep=' '))
        i1 = table.lookup(s1.values)
        s2 = tf.strings.split(tf.reshape(data[1], [1], sep=' '))
        i2 = table.lookup(s2.values)
        return i1, i2

    lines = lines.shuffle(buffer_size=10000)
    lines = lines.map(to_ids)
    boundaries = make_boundaries(10, 500, 1.2)
    sizes = make_batch_sizes(60000, boundaries)
    lines = lines.apply(
        tf.data.experimental.bucket_by_sequence_length(
            element_length_func=lambda x: tf.shape(x[0])[0] + tf.shape(x[1])[0],
            bucket_boundaries=boundaries,
            bucket_batch_sizes=sizes,
            padded_shapes=([None], [None]),
            padding_values=(0, 0)
        )
    )
    lines = lines.prefetch(2)
    return lines


for a, b in read_files([]):
    pass
