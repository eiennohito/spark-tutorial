import os
import sys


def process_line(comment, line):
    if len(line) <= 5:
        print(comment)
        print(line)


def process(path, fname):
    fullpath = os.path.join(path, fname)
    with open(fullpath, 'rt', encoding='utf-8') as fin:
        comment = ""
        for line in fin:
            line = line.rstrip('\n')
            if line.startswith("#"):
                comment = line
                continue
            process_line(comment, line)
            comment = ""


def main(start):
    for path, subdirs, files in os.walk(start):
        for f in files:
            process(path, f)


if __name__ == '__main__':
    main(sys.argv[1])
