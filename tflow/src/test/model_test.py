from model.lstm import LstmModel

import tensorflow as tf


class KWDict(object):
    def __init__(self, dict):
        self._dict = dict

    def __getattr__(self, item):
        return self._dict[item]


def mydict(**kwargs):
    return KWDict(kwargs)


def testargs(vec_size=10, cell_size=None, dtype=tf.float32, vocabulary=500, seed=1, l2_norm=None, softmax_bias=False,
             batch_size=1, relu=False, dropout=None, softmax_sample=1):
    return mydict(vec_size=vec_size, cell_size=cell_size, dtype=dtype, vocabulary=vocabulary, l2_norm=l2_norm,
                  softmax_bias=softmax_bias, seed=seed, batch_size=batch_size, relu=relu, dropout=dropout,
                  softmax_sample=softmax_sample)


class LstmModelTest(tf.test.TestCase):
    def testModelIsCreated(self):
        model = LstmModel(mydict(
            vec_size=10,
            cell_size=None,
            dtype=tf.float32
        ))
        self.assertIsNotNone(model)

    def testEmbeddings(self):
        args = testargs()
        model = LstmModel(args)
        embed = model.embedding(args)

        inputs = tf.constant(
            value=[[0, 1, 5], [2, 3, 6]],
            dtype=tf.int64
        )

        embeds = embed.embed(inputs)
        self.assertEqual(embeds.shape, tf.TensorShape([2, 3, 10]))

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            ret = tf.reduce_sum(tf.square(embeds), axis=2)
            equals = ret > 0.01
            self.assertTrue(tf.reduce_all(equals).eval())

    def testRnn(self):
        args = testargs(batch_size=3)

        inputs = tf.constant(
            value=[
                [1, 6, 5, 0],
                [2, 3, 0, 0],
                [5, 9, 12, 3]
            ],
            dtype=tf.int64
        )

        inputs.set_shape([3, None])

        lengths = tf.constant(
            value=[3, 2, 4],
            dtype=tf.int64
        )

        tposed = tf.transpose(inputs)

        model = LstmModel(args, scope=tf.VariableScope(name="lstmtest", reuse=False))
        embed = model.embedding(args)
        rnn = model.executor(embed, args)

        emb = embed.embed(tposed)
        outs0 = rnn.raw_rnn_pass(emb, lengths)
        outs = rnn.rnn_forward(emb, lengths)
        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            clean, out = sess.run([outs, outs0])
            outs, state = out
            # print (state, outs)
            h = state.h
            self.assertAllClose(h[0], outs[2, 0])
            self.assertAllClose(h[1], outs[1, 1])
            self.assertAllClose(h[2], outs[3, 2])
            self.assertTrue(((clean[0] - outs[2, 0]) == 0).all())
            self.assertTrue(((clean[1] - outs[1, 1]) == 0).all())
            self.assertTrue(((clean[2] - outs[3, 2]) == 0).all())

    def testTrain(self):
        args = testargs(batch_size=2)

        inputs = tf.constant(
            value=[[1, 6, 5], [2, 3, 0]],
            dtype=tf.int64
        )

        inputs.set_shape([2, None])

        lengths = tf.constant(
            value=[3, 2],
            dtype=tf.int64
        )

        labels = tf.constant(
            value=[[20], [50]],
            dtype=tf.int64
        )

        model = LstmModel(args, scope=tf.VariableScope(name="lstmtest", reuse=False))
        embed = model.embedding(args)
        rnn = model.executor(embed, args)
        train = model.loss_evaluator(embed, args)

        tposed = tf.transpose(inputs)
        emb = embed.embed(tposed)
        outs = rnn.rnn_forward(emb, lengths)
        loss = train.loss(outs, labels)

        with self.test_session() as sess:
            sess.run(tf.global_variables_initializer())
            res = sess.run(loss)
            self.assertNotEqual(res, 0.0)


if __name__ == "__main__":
    tf.test.main()
