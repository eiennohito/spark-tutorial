import argparse
import tensorflow as tf
import os
import numpy as np
import re

import matplotlib.pyplot as plt
import scipy.stats

def parse_args():
    p = argparse.ArgumentParser()
    p.add_argument('--input')
    p.add_argument('--target', type=int, default=250_000_000)
    p.add_argument('--window', type=int, default=10_000_000)
    return p.parse_args()

def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
    return m, h

class LineProcessor(object):
    def __init__(self, args):

        point = args.target
        offset = args.window // 2

        self.start = point - offset
        self.end = point + offset
        self.items = {}

    def process(self, checkpt_dir, match):
        v1 = match.group(1)
        v2 = match.group(2)
        v3 = match.group(3)

        values = []
        for fn in os.listdir(checkpt_dir):
            fullname = os.path.join(checkpt_dir, fn)
            if fn.startswith('events.out.tfevents.') and os.path.isfile(fullname):
                try:
                    for e in tf.train.summary_iterator(fullname):
                        if self.start <= e.step <= self.end:
                            for v in e.summary.value:
                                if v.tag == 'kyoto-test/seg/f1':
                                    values.append(v.simple_value)
                except tf.errors.DataLossError:
                    continue
        if values:
            self.items[(f"{v3}-{v1}", v2)] = mean_confidence_interval(values)


def main(args):
    lp = LineProcessor(args)
    for fn in os.listdir(args.input):
        fullname = os.path.join(args.input, fn)
        m = re.match(r'tfe-(\d+)-64-(\d+)-(\d+)', fn)
        if m:
            lp.process(fullname, m)

    print(lp.items)
    #lp.plot()


if __name__ == '__main__':
    main(parse_args())
