import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker
from matplotlib.ticker import FuncFormatter

data = {('128-16', '64'): (0.9879905760288239, 0.00018814224417526817),
        ('64-4', '64'): (0.9794694185256958, 0.0002377776792879733),
        ('256-16', '64'): (0.9904538180146899, 0.00016177603488191661),
        ('64-16', '64'): (0.9853132622582572, 0.0002222197573027259),
        ('128-16', '128'): (0.9894875798906598, 0.00019659359853371628),
        ('128-4', '32'): (0.9814253383212619, 0.00027265237410236125),
        ('256-16', '16'): (0.9868811922413963, 0.00020378951216383102),
        ('128-16', '16'): (0.9858424067497253, 0.00018569673100050593),
        ('256-16', '128'): (0.9907972812652588, 9.079201145048281e-05),
        ('256-4', '32'): (0.9843247979879379, 0.00021243210802423913),
        ('128-16', '32'): (0.9867752432823181, 0.0001781051133588744),
        ('256-4', '16'): (0.9790114611387253, 0.0002634999434138455),
        ('64-16', '32'): (0.9820697531104088, 0.00027497524643206643),
        ('128-4', '16'): (0.978188176949819, 0.0002801177833822849),
        ('256-16', '32'): (0.9882687462700738, 0.0001934955302160709),
        ('64-16', '16'): (0.979167810508183, 0.00037840277335784766),
        ('256-4', '128'): (0.9883074301939744, 0.00016071642284725595),
        ('128-4', '64'): (0.9853605230649313, 0.00020676383223730918),
        ('128-4', '128'): (0.9850183129310608, 0.0003006699106543537),
        ('64-4', '16'): (0.9729039924485343, 0.00046154327708242506),
        ('64-4', '32'): (0.9766743704676628, 0.00026432479868126053),
        ('256-4', '64'): (0.9863915351720957, 0.00012057943034988812)}

colors = ["#478622",
          "#8242b3",
          "#b45915"]

# font = {'family' : 'Rounded Mgen+ 2p',
#         'weight' : 'normal',
#         'size'   : 14}

# matplotlib.rc('font', **font)

keys = set()
for k, _ in data.keys():
    keys.add(k)

for k in sorted(keys, key=lambda x: tuple(map(int, x.split('-')))):
    xs = []
    ys = []
    for ko, x in data.keys():
        if ko == k:
            y, yci = data[(ko, x)]
            xs.append(int(x))
            ys.append(float(y))

    idxs = np.argsort(xs)
    xs = np.asarray(xs)[idxs]
    ys = np.asarray(ys)[idxs]
    if '-4' in k:
        linestyle = 'dotted'
    else:
        linestyle = 'solid'
    if '64-' in k:
        color = colors[0]
        marker = "X"
    elif '128-' in k:
        color = colors[1]
        marker = "p"
    elif '256-' in k:
        color = colors[2]
        marker = "v"
    else:
        color = None
    plt.plot(xs, ys, label=k, linestyle=linestyle, color=color, linewidth=3, marker=marker, markersize=8)

plt.legend(loc='lower right', title='Shr.dim - hid.dim')

plt.axhline(0.984, color='red', label='JUMAN')

plt.ylim((0.97, 0.995))
plt.xscale('log', basex=2)
plt.gca().xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda x, y: '{0:.1f}'.format(100 * x)))
# plt.xticks([16, 32, 64, 128], ['16', '32', '64', '128'])
plt.text(0.02, 0.95, 'Seg F1', transform=plt.gca().transAxes)

plt.xlabel('Embedding dimension')
plt.tight_layout(0)
#plt.show()
plt.savefig('/Users/eiennohito/dev/tex/2018/xmorph-naacl-2019/graphs/bus-hs-emb.eps')
