from time import time
import os

import tensorflow as tf
import model.input as inp
import model.lstm as mdl
import model.debug_output as dbg
import model.valid as val
from model.export import Exporter
from string import Formatter


def argparser():
    import argparse

    parser = argparse.ArgumentParser(description="LSTM word guesser")

    parser.add_argument('--input', type=str, help="file which contains list of output files", required=True)
    parser.add_argument('--output', type=str, help="output directory for snapshots and model", required=True)
    parser.add_argument('--words', type=int, help="size of vocabulary", dest='vocabulary', required=True)
    parser.add_argument('--debug_input', action='store_true', help="treat input as file with text data")
    parser.add_argument('--batch_size', type=int, default=16, help="batch size")
    parser.add_argument('--export_batch', type=int, default=32, help="batch size for export")
    parser.add_argument('--export_only', action='store_true', help="only export graph, do not train")
    parser.add_argument('--shuffle_size', type=int, default=500, help="size of shuffling queue")
    parser.add_argument('--seed', type=int, default=0xdeadbeef, help="seed of rng")
    parser.add_argument('--vec_size', type=int, default=100, help="embedding size")
    parser.add_argument('--cell_size', type=int, default=None, help="lstm state/hidden size, if None then vec_size")
    parser.add_argument('--save_interval', type=int, default=10000, help="snapshot model each interval steps")
    parser.add_argument('--summary_interval', type=int, default=20, help="save summary each N steps")
    parser.add_argument('--dictionary', type=str, default=None,
                        help="dictionary file for tensorflow embedding visualizer")
    parser.add_argument('--input_threads', type=int, default=1)
    parser.add_argument('--softmax_sample', type=int, default=1000)
    parser.add_argument('--epochs', type=int, default=1)
    parser.add_argument('--lrate', type=float, default=0.001)
    parser.add_argument('--decay_rate', type=float, default=0.96)
    parser.add_argument('--decay_steps', type=int, default=3000)
    parser.add_argument('--log_devices', action='store_true')
    parser.add_argument('--debug_tensors', action='store_true')
    parser.add_argument('--non_packed', action='store_false', dest='packed')
    parser.add_argument('--num_gpus', type=int, default=0)
    parser.add_argument('--dtype', action='store', default=tf.float32)
    parser.add_argument('--restore', type=str, default=None, help='restore training from specified checkpoint')
    parser.add_argument('--online_check_cnt', type=int, default=1)
    parser.add_argument('--online_check_sentences', type=str, default=None)
    parser.add_argument('--online_check_maxn', type=int, default=20)
    parser.add_argument('--l2_norm', action='store_true', default=False)
    parser.add_argument('--optimizer', action='store', default='adagrad',
                        choices=['sgd', 'adagrad', 'adadelta', 'adam', 'rmsprop'])
    parser.add_argument('--softmax_bias', action='store_true', default=False)
    parser.add_argument('--validation', type=str, default=None)
    parser.add_argument('--relu', action='store_true', default=False)
    parser.add_argument('--dropout', type=float, default=None)
    parser.add_argument('--xla', action='store_true')

    return parser


class Debug(object):
    def __init__(self, args):
        self.dict_file = args.dictionary
        self._dict = None

        self.valid_path = args.validation
        self.validator = None

    @property
    def dict(self):
        if self._dict is None:
            self._dict = val.parse_dictionary(self.dict_file)
            print("loaded dictionary: %s" % self._dict)
        return self._dict

    def initialize(self, args, model):
        if self.valid_path is not None:
            data = val.validation_data(self.valid_path, self.dict)
            print ("loaded validation data: %s sentences" % data[1])
            self.validator = val.Validator(args, model.model, model.embed, data)

    def status_enabled(self):
        return self.valid_path is not None

    def status_fmt_string(self):
        return "[{top_10:.3f}/{top_30:.3f}/{top_100:.3f}]"

    def status_fmt_tensors(self):
        return self.validator.ins

    def status_fmt_unpack(self, dic, data):
        dic["top_10"] = data[0]
        dic["top_30"] = data[1]
        dic["top_100"] = data[2]


class LstmModel(object):
    def __init__(self, args):
        self.debug = Debug(args)
        self.debug_tensors = args.debug_tensors

        self.model = mdl.LstmModel(args)
        self.embed = self.model.embedding(args)
        self.loss_eval = self.model.loss_evaluator(self.embed, args)
        self.runner = self.model.executor(self.embed, args)

        export_args = mdl.ExecArgs(
            batch_size=args.export_batch,
            dtype=args.dtype,
            l2_norm=args.l2_norm,
            dropout=None,
            relu=args.relu,
            seed=args.seed
        )
        self.export_runner = self.model.executor(self.embed, export_args)

        self.export_input = tf.placeholder(dtype=tf.int64, shape=[None, args.export_batch])
        self.export_lens = tf.placeholder(dtype=tf.int64, shape=[args.export_batch])
        self.export_embed_id = tf.placeholder(dtype=tf.int64, shape=[None])
        self.export_embeddings = self.embed.embed(self.export_embed_id)
        embs = self.embed.embed(self.export_input)
        self.export_result = self.export_runner.rnn_forward(embs, self.export_lens)
        # result is batch X vec_size, embedding is vocabulary X vec_size
        scores = self.embed.softmax(self.export_result)
        top_scores, top_ids = tf.nn.top_k(scores, k=100)
        self.export_nearest_ids = top_ids
        self.export_nearest_scores = top_scores

    def infer(self, input_tuple):
        data, size, answer = input_tuple
        embedded = self.embed.embed(data)
        return self.runner.rnn_forward(embedded, size)

    def compute_loss(self, input_tuple):
        sentence_summary = self.infer(input_tuple)

        _, size, answer = input_tuple

        if self.debug_tensors:
            tf.summary.histogram("debug/sent_summary", sentence_summary)
            rows = tf.square(tf.reduce_sum(sentence_summary, axis=1))
            tf.summary.histogram("debug/sent_summary_rows", rows)

        mbatch_loss = self.loss_eval.loss(sentence_summary, answer)

        tf.summary.scalar("train/mbatch_loss", mbatch_loss)
        return mbatch_loss

    def initialize(self, args):
        self.debug.initialize(args, self)


def trainer(optimizer, loss, step, lrate):
    opt = optimizer(learning_rate=lrate)
    tf.summary.scalar("train/step", step)
    tf.summary.scalar("train/rate", lrate)
    return opt.minimize(loss, global_step=step)


class ConsoleReporter(object):
    def __init__(self, batch_size, plugins, first_step=1):
        self.batch_size = batch_size
        self.start_time = time()
        self.last_step = first_step
        self.plugins = [p for p in plugins if p.status_enabled()]

        parts = ["{0:d}: processed {1:d}|{2:d} items in {3:.2f} secs, speed {4:.2f}|{5:.2f} item/sec, loss={6:.3e}"]
        for p in plugins:
            parts.append(p.status_fmt_string())

        self.message_template = " ".join(parts)

    def plugin_tensors(self):
        return [p.status_fmt_tensors() for p in self.plugins]

    def report(self, step, loss, num_tokens, plugin_data, summary=None):
        time_end = time()
        steps = step - self.last_step
        duration = time_end - self.start_time
        self.start_time = time_end
        self.last_step = step
        cnt = steps * self.batch_size
        speed = cnt / duration
        speed2 = speed * num_tokens / self.batch_size

        if summary is not None:
            sents_sec = summary.value.add()
            sents_sec.tag = "train/examples_per_sec"
            sents_sec.simple_value = speed

            tokens_sec = summary.value.add()
            tokens_sec.tag = "train/tokens_per_sec"
            tokens_sec.simple_value = speed2

            token_speed = summary.value.add()
            token_speed.tag = "train/time_for_token"
            token_speed.simple_value = speed2 / num_tokens / self.batch_size

        fmt = Formatter()

        dic = {

        }
        for plug, data in zip(self.plugins, plugin_data):
            plug.status_fmt_unpack(dic, data)

        result = fmt.vformat(
            format_string=self.message_template,
            args=[step, cnt, num_tokens, duration, speed, speed2, loss],
            kwargs=dic
        )
        print(result)

    def reset_time(self):
        self.start_time = time()


class Trainer(object):
    @staticmethod
    def compute_optimizer(name):
        if name == 'rmsprop':
            return tf.train.RMSPropOptimizer
        elif name == 'sgd':
            return tf.train.GradientDescentOptimizer
        elif name == 'adagrad':
            return tf.train.AdagradOptimizer
        elif name == 'adadelta':
            return tf.train.AdadeltaOptimizer
        elif name == 'adam':
            return tf.train.AdamOptimizer
        elif name == 'momentum':
            return tf.train.MomentumOptimizer
        else:
            print("unknown optimizer: %s" % name)
            return tf.train.GradientDescentOptimizer

    def __init__(self, args, model, ngpu, input):

        self.optimizer = Trainer.compute_optimizer(args.optimizer)

        def do_init():
            self.loss = model.compute_loss(input)
            self.step_var = tf.Variable(1, name='global_step_%d' % ngpu, trainable=False)
            self.learn_rate = tf.train.exponential_decay(args.lrate, self.step_var, args.decay_steps, args.decay_rate)
            self.training_op = trainer(self.optimizer, self.loss, self.step_var, self.learn_rate)

        if ngpu == -1:
            do_init()
        else:
            with tf.device("/gpu:%d" % ngpu):
                do_init()


if __name__ == "__main__":
    tf.logging.set_verbosity(tf.logging.DEBUG)

    parser = argparser()
    args = parser.parse_args()

    if args.debug_tensors:
        args.output = os.path.join(args.output, str(time()))

    model = LstmModel(args)

    if not args.debug_input:
        data_filenames = inp.lines_of_file(args.input)
        data = inp.input_pipeline(data_filenames, args)
    else:
        input_data = val.validation_data(args.input, model.debug.dict)
        data = inp.fixed_input_pipeline(input_data, args)

    trainers = None
    if args.num_gpus == 0:
        trainers = [(Trainer(args, model, -1, data))]
    elif args.num_gpus > 0:
        trainers = [Trainer(args, model, n, data) for n in range(0, args.num_gpus)]

    first = trainers[0]

    checker = None
    if args.dictionary is not None and args.online_check_sentences is not None:
        checker = dbg.Trial(model, args)

    model.initialize(args)

    # Before starting, initialize the variables.  We will 'run' this first.
    init = tf.global_variables_initializer()

    summary_writer = tf.summary.FileWriter(args.output)

    if args.dictionary is not None:
        from tensorflow.contrib.tensorboard.plugins import projector

        conf = projector.ProjectorConfig()

        embedding = conf.embeddings.add()
        embedding.tensor_name = model.embed.embedding.name
        embedding.metadata_path = args.dictionary

        projector.visualize_embeddings(summary_writer, conf)

    gpuopts = tf.GPUOptions(
        allow_growth=args.debug_tensors
    )

    sess_conf = tf.ConfigProto(
        log_device_placement=args.log_devices,
        gpu_options=gpuopts,
        allow_soft_placement=True
    )

    if args.xla:
        print("Using XLA!")
        sess_conf.graph_options.optimizer_options.global_jit_level = tf.OptimizerOptions.ON_1

    print("Starting learning: summary/snap: %d/%d" % (args.summary_interval, args.save_interval))

    save_path = os.path.join(args.output, "tensors")

    # Launch the graph.
    with tf.Session(config=sess_conf) as sess:
        summary_writer.add_graph(sess.graph)
        coord = tf.train.Coordinator()
        sess.run([init, tf.local_variables_initializer()])

        threads = tf.train.start_queue_runners(coord=coord, sess=sess)

        exporter = Exporter(sess, model)

        saver = tf.train.Saver()
        summaries = tf.summary.merge_all()
        step_sizes = tf.reduce_sum(data[1])

        step = 1


        def do_restore(checkpoint):
            print("Restoring execution from: %s" % checkpoint)
            print("=======================")
            saver.restore(sess, checkpoint)

            # input = tf.constant([0, 1, 500, 1200])
            # output = model.embed.embed(input)
            # print(sess.run(output))
            #
            # exit()


            mystep = sess.run(first.step_var)
            print("restored learning from step %d" % mystep)
            return mystep


        if (args.restore is not None) and os.path.exists(args.restore + ".meta"):
            print("=======================")
            print("RESTORE IS USED FROM COMMAND LINE!")
            step = do_restore(args.restore)
        else:
            latest = tf.train.latest_checkpoint(args.output)
            if latest is not None:
                print("=======================")
                print("Found a checkpoint!")
                step = do_restore(latest)

        reporter = ConsoleReporter(
            batch_size=args.batch_size * len(trainers),
            plugins=[model.debug],
            first_step=step)
        try:
            main_op = [t.training_op for t in trainers]

            print([n.name for n in tf.global_variables()])
            report_plugins = reporter.plugin_tensors()

            while not coord.should_stop():
                if step % args.summary_interval == 0:

                    run_opts = tf.RunOptions()
                    run_meta = tf.RunMetadata()

                    if step % args.save_interval == 0:
                        run_opts.trace_level = tf.RunOptions.FULL_TRACE  # FULL_TRACE
                        run_opts.output_partition_graphs = True

                    _, summ, lval, cnts, rep_data = sess.run(
                        fetches=[first.training_op, summaries, first.loss, step_sizes, report_plugins],
                        options=run_opts,
                        run_metadata=run_meta
                    )
                    summ2 = tf.Summary()
                    summ2.ParseFromString(summ)
                    reporter.report(step, lval, cnts, rep_data, summary=summ2)
                    summary_writer.add_summary(summ2, step)

                    if step % args.save_interval == 0:
                        summary_writer.add_run_metadata(run_meta, "debug_run_at_%s" % step, step)

                    if checker is not None:
                        checker.do_eval(sess)
                else:
                    sess.run(main_op)

                if step % args.save_interval == 0:
                    path = saver.save(sess, save_path, global_step=step)
                    print("saved snapshot to %s" % path)

                    exporter.do_export(first.step_var, args.output)

                    reporter.reset_time()

                step += 1

        except tf.errors.OutOfRangeError:
            print("Done training!")
        finally:
            coord.request_stop()

        coord.join(threads)

        saver.save(sess, save_path, global_step=step)
