import os
import os.path as p
import shutil as sh
import sys

from timeit import default_timer as timer
from datetime import timedelta


class Progress(object):
    def __init__(self):
        self.start = timer()

    def interpolate(self, cnt, total):
        now = timer()
        progress = float(cnt) / total
        dur = now - self.start
        total_time = dur / progress
        return timedelta(seconds=total_time - dur)


def collect_files(base):
    result = []
    dirs = []
    for dirname, subdirs, files in os.walk(base, followlinks=False):
        for f in files:
            result.append(p.relpath(p.join(dirname, f), base))
        dirs.append(p.relpath(dirname, base))
    return (result, dirs)


def distribute(files, locs):
    locidx = 0
    loccnt = len(locs)
    res = []
    for f in files:
        res.append((f, locs[locidx % loccnt]))
        locidx += 1
    return res


def mkdirp(path, mode=0o777):
    if not p.exists(path):
        head, tail = p.split(path)
        if len(tail) > 0 and not p.exists(head):
            mkdirp(head, mode)
        os.mkdir(path, mode)


def make_dirs(locs, dirs, loc):
    for l in locs:
        mkdirp(p.join(l, loc))
        for d in dirs:
            mkdirp(p.join(l, loc, d))


def do_copy(base, dnames, dpath):
    prog = Progress()
    cnt = 1
    total = len(dnames)
    for f, dst in dnames:
        sh.copy2(p.join(base, f), p.join(dst, dpath, f))  # do actual copy
        if cnt % 100 == 0:
            time = prog.interpolate(cnt, total)
            print("copied %d/%d files, %s left" % (cnt, total, time))
        cnt += 1


def do_symlinks(dnames, dpath, target):
    for f, dst in dnames:
        file = p.join(dst, dpath, f)
        symlink = p.join(target, f)
        os.symlink(file, symlink)


if __name__ == "__main__":
    args = sys.argv
    src = args[1]
    dests = args[2].split(',')
    core = dests[0]
    local = ".local"

    print("distributing content of %s to %s" % (src, dests))

    print("collecting filenames...")
    files, dirs = collect_files(src)
    print("Done: %d files" % len(files))

    dfiles = distribute(files, dests)
    make_dirs(dests, dirs, local)

    print("Copying:")
    do_copy(src, dfiles, local)
    print("Copy done.")

    make_dirs([core], dirs, "")
    print("Making symlinks...")
    do_symlinks(dfiles, local, core)
    print("All processing is done")
