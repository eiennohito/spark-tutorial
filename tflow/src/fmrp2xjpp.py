#!/usr/bin/env python3

import sys

with open(sys.argv[1], encoding='utf-8') as f:
    for line in f:
        if len(line) < 1:
            continue

        line = line.rstrip()
        hidx = line.index('#')
        if hidx > 0:
            print(line[hidx:])
            line = line[:hidx].rstrip()

        nid = 1
        cst = 0

        for token in line.split(' '):
            parts = token.split('_')

            cen = cst + len(parts[0]) - 1

            jppparts = [
                "-",
                nid,
                nid - 1,
                cst,
                cen,
                parts[0],
                "*",  # no daihyo,
                parts[1],  # rd
                parts[2],  # dict
                parts[3],  # pos
                "0",  # posid
                parts[4],  # subpos
                "0",  # subposid
                parts[5],  # ctype
                "0",  # ctypeid
                parts[6],  # cform
                "0",  # cformid
                "ランク:1",  # comments
            ]

            print("\t".join(str(x) for x in jppparts))
            cst = cen + 1
            nid += 1
        print("EOS")
