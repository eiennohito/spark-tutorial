package ws.eiennohito

import ws.eiennohito.math.VectorOps

/**
  * @author eiennohito
  * @since 2017/02/01
  */
object KnnUtil {
  import breeze.linalg._

  def normOf(mat: DenseMatrix[Float]): DenseMatrix[Float] = {
    val data = new Array[Float](mat.cols * mat.rows)
    VectorOps.normalize2d(mat.data, mat.rows, mat.cols, data)
    new DenseMatrix[Float](mat.rows, mat.cols, data, mat.offset, mat.majorStride, mat.isTranspose)
  }

  def normVec(vec: DenseVector[Float]): DenseVector[Float] = {
    val copy = vec.copy
    VectorOps.normalize(copy.data, 0, copy.activeSize)
    copy
  }


  def topN(data: DenseMatrix[Float], query: DenseVector[Float], n: Int): (IndexedSeq[Int], DenseVector[Float]) = {
    val prod = query.t * data
    val indices = argtopk(prod.inner, n)
    val values = prod.inner(indices)
    (indices, values.toDenseVector)
  }


}
