package ws.eiennohito

import breeze.linalg.DenseMatrix
import org.tensorflow.framework.tensor.TensorProto
import org.tensorflow.framework.tensor_shape.TensorShapeProto
import org.tensorflow.framework.tensor_shape.TensorShapeProto.Dim
import org.tensorflow.framework.types.DataType
import org.tensorflow.serving.model.ModelSpec
import org.tensorflow.serving.predict.{PredictRequest, PredictResponse}
import org.tensorflow.serving.prediction_service.PredictionServiceGrpc.PredictionService
import ws.kotonoha.akane.analyzers.knp.{LexemeApi, TableApi}
import ws.kotonoha.akane.vectorization.DictionaryVectorizer

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{ExecutionContext, Future}

/**
  * @author eiennohito
  * @since 2017/01/31
  */

trait SentenceVectorizer {
  def idFor(lex: LexemeApi): Seq[Long]
  def placeholder: Long
}

case class MarkedMorpheme(data: LexemeApi, ids: Seq[Long])

case class MaskedSentence(
  sentence: Seq[MarkedMorpheme],
  mask: Seq[Int]
) {
  val length: Int = sentence.view.map(_.ids.length).sum
}

object MatrixOps {
  def put(mat: DenseMatrix[Long], xstart: Int, y: Int, longs: Seq[Long]): Unit = {
    var start = xstart
    val iter = longs.iterator
    while (iter.hasNext) {
      mat(start, y) = iter.next()
      start += 1
    }
  }
}

class SentenceSummarizerClient(svc: PredictionService, val batchSize: Int, placeholder: Long) {

  def makeWords(sentences: Seq[MaskedSentence]) = {
    val maxLen = sentences.map(_.length).max + 2
    val data = new Array[Long](batchSize * maxLen)
    val mat = new DenseMatrix[Long](maxLen, batchSize, data, 0, batchSize, true)

    val sents = sentences.iterator
    var sentIdx = 0
    while (sents.hasNext) {
      val sent = sents.next()

      var lexTfIdx = 1
      var lexOrigIdx = 0
      val lexIter = sent.sentence.iterator
      while (lexIter.hasNext) {
        val lex = lexIter.next()

        val toPut = if (sent.mask.contains(lexOrigIdx)) {
          lex.ids.updated(0, placeholder)
        } else {
          lex.ids
        }

        MatrixOps.put(mat, lexTfIdx, sentIdx, toPut)

        lexOrigIdx += 1
        lexTfIdx += lex.ids.length
      }

      mat(0, sentIdx) = 2
      mat(lexTfIdx, sentIdx) = 3

      sentIdx += 1
    }

    println(mat)


    TensorProto(
      dtype = DataType.DT_INT64,
      tensorShape = Some(TensorShapeProto(dim = Seq(Dim(maxLen), Dim(batchSize)))),
      int64Val = data
    )
  }

  def makeSizes(sentences: Seq[MaskedSentence]) = {
    TensorProto(
      dtype = DataType.DT_INT64,
      tensorShape = Some(TensorShapeProto(dim = Seq(Dim(batchSize)))),
      int64Val = sentences.map(_.length.toLong + 2).padTo(batchSize, 2L)
    )
  }

  val emptyInt64Tensor = TensorProto(
    dtype = DataType.DT_INT64,
    tensorShape = Some(TensorShapeProto(dim = Seq(Dim(0))))
  )

  def summarize(sentences: Seq[MaskedSentence])(implicit ec: ExecutionContext): Future[DenseMatrix[Float]] = {
    val req = PredictRequest(
      inputs = Map(
        "words" -> makeWords(sentences),
        "sizes" -> makeSizes(sentences),
        "embeds" -> emptyInt64Tensor
      ),
      modelSpec = Some(ModelSpec(name = "default")),
      outputFilter = Seq("summaries")
    )
    svc.predict(req).map { resp =>
      val sentLen = sentences.length
      parseTensor(resp, sentLen, "summaries")
    }
  }

  private def parseTensor(resp: PredictResponse, sentLen: Int, name: String): DenseMatrix[Float] = {
    val tensor = resp.outputs(name)
    val vecLen = tensor.tensorShape.get.dim(1).size.toInt
    val data = tensor.floatVal.slice(0, sentLen * vecLen).toArray
    new DenseMatrix[Float](
      rows = vecLen,
      cols = sentLen,
      data = data,
      offset = 0,
      majorStride = vecLen,
      isTranspose = false
    )
  }

  private def parseIntTensor(resp: PredictResponse, sentLen: Int, name: String): DenseMatrix[Int] = {
    val tensor = resp.outputs(name)
    val vecLen = tensor.tensorShape.get.dim(1).size.toInt
    val data = tensor.intVal.slice(0, sentLen * vecLen).toArray
    new DenseMatrix[Int](
      rows = vecLen,
      cols = sentLen,
      data = data,
      offset = 0,
      majorStride = vecLen,
      isTranspose = false
    )
  }

  def tensor(data: Seq[Long]) = {
    TensorProto(
      dtype = DataType.DT_INT64,
      tensorShape = Some(TensorShapeProto(Dim(data.length) :: Nil)),
      int64Val = data
    )
  }

  def convertScores(ids: DenseMatrix[Int], scores: DenseMatrix[Float]) = {
    val ret = new ArrayBuffer[Seq[NearestItem]]()
    var col = 0
    val totalItems = ids.cols
    val subitemCount = ids.rows
    while (col < totalItems) {
      val items = new ArrayBuffer[NearestItem](subitemCount)

      var row = 0
      while (row < subitemCount) {

        val itemId = ids(row, col)
        val itemScore = scores(row, col)

        items += NearestItem(itemId, itemScore)

        row += 1
      }

      ret += items
      col += 1
    }
    ret
  }

  def parseResponse(resp: PredictResponse, reqSentsLen: Int, embedLen: Int) = {
    val summaries = parseTensor(resp, reqSentsLen, "summaries")
    val embeds = parseTensor(resp, embedLen, "embeddings")
    val scores = parseTensor(resp, reqSentsLen, "nearest_scores")
    val ids = parseIntTensor(resp, reqSentsLen, "nearest_ids")

    val scoreObjects = convertScores(ids, scores)

    SummaryPlusScores(
      representations = summaries,
      embeddings = embeds,
      nearest = scoreObjects
    )
  }

  def summaryAndScores(sentences: Seq[MaskedSentence], embeds: Seq[Long])(implicit ec: ExecutionContext) = {
    val req = PredictRequest(
      inputs = Map(
        "words" -> makeWords(sentences),
        "sizes" -> makeSizes(sentences),
        "embeds" -> tensor(embeds)
      ),
      modelSpec = Some(ModelSpec(name = "default"))
    )

    svc.predict(req).map { resp =>
      println("response here!")
      parseResponse(resp, sentences.length, embeds.length)
    }
  }
}

case class NearestItem(id: Long, score: Float)

case class SummaryPlusScores(
  representations: DenseMatrix[Float],
  embeddings: DenseMatrix[Float],
  nearest: IndexedSeq[Seq[NearestItem]]
)


class DictionaryVectorizerWrapper(dvec: DictionaryVectorizer) extends SentenceVectorizer {
  override def idFor(lex: LexemeApi): Seq[Long] = dvec.vecOne(lex)
  override def placeholder: Long = 1
}
