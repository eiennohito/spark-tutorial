package ws.eiennohito.math

import breeze.linalg.{DenseMatrix, DenseVector, SparseVector}
import breeze.storage.Storage

import scala.annotation.strictfp

/**
  * @author eiennohito
  * @since 2014-02-18
  */
object VectorOps {

  def scale(arr: Array[Double], offset: Int, length: Int, x: Double): Unit = {
    var i = offset
    val e = offset + length
    assume(i >= 0, s"offset should be positive, was $i")
    assume(e <= arr.length, s"end should not be more than length, $e ${arr.length}")
    while (i < e) {
      arr(i) *= x
      i += 1
    }
  }

  def sparseDenseDot(sparseData: Array[Float], indices: Array[Int], denseData: Array[Float]) = {
    var i = 0
    val len = sparseData.length min indices.length
    var res = 0.0f
    while (i < len) {
      res += sparseData(i) * denseData(indices(i))
      i += 1
    }
    res
  }


  def copyScaled(input: Array[Float], off: Int, len: Int, output: Array[Float], outOff: Int, scale: Float): Unit = {
    var i = 0
    while (i < len) {
      output(outOff + i) = input(off + i) * scale
      i += 1
    }
  }

  def normalize2d(input: Array[Float], rows: Int, cols: Int, output: Array[Float]): Unit = {
    var col = 0
    while (col < cols) {
      val offset = col * rows
      val colNorm = norm2(input, offset, rows)
      copyScaled(input, offset, rows, output, offset, 1.0f / colNorm)
      col += 1
    }
  }

  def norm2(array: Array[Float]): Float = {
    norm2(array, 0, array.length)
  }

  def norm2(array: Array[Float], off: Int, len: Int): Float = {
    Math.sqrt(arrayDot(array, off, len, array, off, len)).toFloat
  }

  def norm2(array: Array[Double]): Double = {
    norm2(array, 0, array.length)
  }

  def norm2(array: Array[Double], offset: Int, length: Int): Double = {
    Math.sqrt(norm2sq(array, offset, length))
  }

  def norm2sq(array: Array[Double], offset: Int, length: Int): Double = {
    var i = offset
    val end = array.length min (offset + length)

    var res = 0.0
    while (i < end) {
      val fp = array(i)
      res += fp * fp
      i += 1
    }

    res
  }

  def add(res: Array[Float], input: Array[Float]): Unit = {
    val len = res.length min input.length
    var i = 0
    while (i < len) {
      res(i) += input(i)
      i += 1
    }
  }

  def arrayDot(a1: Array[Float], a2: Array[Float]): Float = {
    arrayDot(a1, 0, a1.length, a2, 0, a2.length)
  }

  def arrayDot(
    a1: Array[Float], off1: Int, len1: Int,
    a2: Array[Float], off2: Int, len2: Int
  ): Float = {
    var res: Float = 0
    val len = len1 min len2
    var i = 0
    while (i < len) {
      res += a1(off1 + i) * a2(off2 + i)
      i += 1
    }
    res
  }

  def arrayDot(a1: Array[Double], a2: Array[Double]) = {
    var res: Double = 0
    val len = a1.length min a2.length
    var i = 0
    while (i < len) {
      res += a1(i) * a2(i)
      i += 1
    }
    res
  }

  def normalize(array: Array[Float]): Unit = {
    normalize(array, 0, array.length)
  }

  def normalize(array: Array[Float], offset: Int, length: Int): Unit = {
    val norm = norm2(array)
    if (norm < 1e-6) return
    val invLen = 1 / norm
    var i = 0
    while (i < length) {
      array(i + offset) *= invLen
      i += 1
    }
  }

  def normailze(array: Array[Double], scale: Double = 1.0): Unit = {
    normailze(array, array.length, scale)
  }

  def normailze(array: Array[Double], alen: Int, scale: Double): Unit = {
    val norm = norm2(array)
    if (norm < 1e-6) return
    val invLen = scale / norm
    var i = 0
    while (i < alen) {
      array(i) *= invLen
      i += 1
    }
  }

  def scaleSparse(sv: SparseVector[Float], data: Array[Float]) = {
    val sd = sv.data
    val si = sv.index

    var i = 0
    val end = sv.activeSize
    while (i < end) {
      sd(i) *= data(si(i))
      i += 1
    }
    sv
  }

  def interpolate(v1: DenseVector[Float], r1: Float, v2: DenseVector[Float], r2: Float): DenseVector[Float] = {
    val res = v1 :* r1
    res += (v2 :* r2)
    res
  }

  def interpolate(v1: SparseVector[Float], r1: Float, v2: SparseVector[Float], r2: Float) = {
    val i1 = v1.index
    val d1 = v1.data
    val l1 = v1.activeSize
    val i2 = v2.index
    val d2 = v2.data
    val l2 = v2.activeSize

    val resIdx = new Array[Int](l1 + l2)
    val resData = new Array[Float](l1 + l2)

    var i, j, k = 0
    while (i < l1 && j < l2) {
      val xi1 = i1(i)
      val xi2 = i2(j)
      if (xi1 == xi2) {
        resIdx(k) = xi1
        resData(k) = r1 * d1(i) + r2 * d2(j)
        i += 1
        j += 1
        k += 1
      } else if (xi1 < xi2) {
        resIdx(k) = xi1
        resData(k) = d1(i) * r1
        i += 1
        k += 1
      } else {
        resIdx(k) = xi2
        resData(k) = d2(j) * r2
        j += 1
        k += 1
      }
    }

    if (i < l1) {
      System.arraycopy(i1, i, resIdx, k, l1 - i)
      while (i < l1) {
        resData(k) = d1(i) * r1
        i += 1
        k += 1
      }
    } else if (j < l2) {
      System.arraycopy(i2, j, resIdx, k, l2 - j)
      while (j < l2) {
        resData(k) = d2(j) * r2
        j += 1
        k += 1
      }
    }

    new SparseVector[Float](resIdx, resData, k, v1.length max v2.length)
  }

  def square(m: Storage[Double]): Unit = {
    var i = 0
    val size = m.activeSize
    val data = m.data
    while (i < size) {
      val x = data(i)
      data(i) = x * x
      i += 1
    }
  }

  def square(m: DenseMatrix[Double]): Unit = {
    var i = 0
    val size = m.activeSize
    val data = m.data
    while (i < size) {
      val x = data(i)
      data(i) = x * x
      i += 1
    }
  }
}
