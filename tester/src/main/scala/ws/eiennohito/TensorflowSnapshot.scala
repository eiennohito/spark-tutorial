package ws.eiennohito

import java.io.{Closeable, RandomAccessFile}
import java.nio.channels.FileChannel
import java.nio.channels.FileChannel.MapMode
import java.nio.file.{Path, Paths}
import java.nio.{ByteBuffer, ByteOrder, FloatBuffer}

import breeze.linalg.DenseMatrix
import com.trueaccord.scalapb.{GeneratedMessage, GeneratedMessageCompanion, Message}
import org.iq80.leveldb.table.{BytewiseComparator, FileChannelTable}
import org.tensorflow.framework.meta_graph.MetaGraphDef
import org.tensorflow.framework.types.DataType
import org.tensorflow.util.tensor_bundle.BundleHeaderProto.Endianness
import org.tensorflow.util.tensor_bundle.{BundleEntryProto, BundleHeaderProto}
import ws.kotonoha.akane.io.Charsets


/**
  * @author eiennohito
  * @since 2017/02/01
  */
class TensorflowSnapshot(
  graphDef: MetaGraphDef,
  headerProto: BundleHeaderProto,
  entries: Map[String, BundleEntryProto],
  base: String
) extends Closeable {

  assert(headerProto.numShards == 1, s"numShards > 1 are not supported, was ${headerProto.numShards}")

  private val dataFile = new RandomAccessFile(base + ".data-00000-of-00001", "r")

  private val channel = dataFile.getChannel

  private val byteOrder = headerProto.endianness match {
    case Endianness.BIG => ByteOrder.BIG_ENDIAN
    case Endianness.LITTLE => ByteOrder.LITTLE_ENDIAN
    case o => throw new Exception(s"Unknown order: $o")
  }

  def tensorView(name: String) = {
    val entryProto = entries(name)
    TensorView.make(name, entryProto, channel, byteOrder)
  }

  override def close() = {
    dataFile.close()
  }
}


class TensorView(name: String, data: ByteBuffer, datatype: DataType, shape: Array[Int]) {

  def floats(): TensorView.Floats = {
    assert(datatype == DataType.DT_FLOAT)
    new TensorView.Floats(data.asFloatBuffer(), shape)
  }

}

object TensorView {
  def make(name: String, entryProto: BundleEntryProto, chan: FileChannel, order: ByteOrder) = {
    val mmap = chan.map(MapMode.READ_ONLY, entryProto.offset, entryProto.size)
    mmap.order(order)
    new TensorView(
      name,
      mmap,
      entryProto.dtype,
      entryProto.shape.get.dim.map(_.size.toInt).toArray
    )
  }

  class Floats(data: FloatBuffer, shape: Array[Int]) {

    def gather(input: DenseMatrix[Float], indices: TraversableOnce[Int]): Unit = ???

    def toMatrix(): DenseMatrix[Float] = {
      assert(shape.length == 2)
      //tensorflow indices are col-major-ish
      val array = new Array[Float](shape(0) * shape(1))
      data.get(array) //read from mmaped file

      new DenseMatrix[Float](
        cols = shape(0),
        rows = shape(1),
        data = array,
        offset = 0,
        majorStride = shape(1),
        isTranspose = false
      )
    }

  }

}


object TensorflowSnapshot {

  def parseObj[T <: Message[T] with GeneratedMessage](file: Path)(implicit meta: GeneratedMessageCompanion[T]): Option[T] = {
    import ws.kotonoha.akane.resources.FSPaths._

    try {
      val o = file.inputStream.map { i => meta.parseFrom(i) }
      Some(o.obj)
    } catch {
      case e: Exception =>
        e.printStackTrace()
        None
    }
  }

  def read(base: String): TensorflowSnapshot = {
    val graphMeta = parseObj[MetaGraphDef](Paths.get(base + ".meta")).get
    val bundlePath = Paths.get(base + ".index")
    val raf = new RandomAccessFile(bundlePath.toFile, "r")
    val table = new FileChannelTable(
      bundlePath.toString,
      raf.getChannel,
      new BytewiseComparator,
      true
    )
    val iter = table.iterator()

    val hdr = BundleHeaderProto.parseFrom(iter.next().getValue.getBytes)

    val entries = Map.newBuilder[String, BundleEntryProto]
    while (iter.hasNext) {
      val o = iter.next()
      val nameSlice = o.getKey
      val name = new String(nameSlice.getBytes, Charsets.utf8)
      val valBytes = o.getValue
      val bundle = BundleEntryProto.parseFrom(valBytes.getBytes)
      entries += name -> bundle
    }

    raf.close()

    new TensorflowSnapshot(graphMeta, hdr, entries.result(), base)
  }
}
