package ws.eiennohito

/**
  * @author eiennohito
  * @since 2017/01/31
  */
object TryReadTF {

  def main(args: Array[String]): Unit = {
    val base = args(0)

    val reader = TensorflowSnapshot.read(base)

    val embs = reader.tensorView("rnn/embedding")
    val matrix = embs.floats().toMatrix()

    println(matrix(0, ::).inner)
    println(matrix(1, ::).inner)
    println(matrix(500, ::).inner)
    println(matrix(1200, ::).inner)

    val vec = matrix(1200, ::).inner
    println(s"${vec.offset} ${vec.length} ${vec.stride}")

    reader.close()
  }
}
