package ws.eiennohito

import java.io.{BufferedReader, InputStreamReader}
import java.nio.file.Path
import java.util

import breeze.linalg.{DenseMatrix, DenseVector}
import io.grpc.ManagedChannelBuilder
import org.tensorflow.serving.prediction_service.PredictionServiceGrpc.PredictionServiceStub
import ws.eiennohito.math.VectorOps
import ws.kotonoha.akane.analyzers.knp.{LexemeApi, TableApi}
import ws.kotonoha.akane.analyzers.knp.raw.OldAngUglyKnpTable
import ws.kotonoha.akane.io.Charsets
import ws.kotonoha.akane.parser.KnpTabFormatParser
import ws.kotonoha.akane.utils.DelimetedIterator
import ws.kotonoha.akane.vectorization.{CompressionSupport, DictionaryVectorizer, VectKnpTree}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{Await, Future}

/**
  * @author eiennohito
  * @since 2017/02/09
  */
object SentenceRater {
  import ws.kotonoha.akane.resources.FSPaths._

  def readTrees(treeFile: Path) = {
    val p = new KnpTabFormatParser
    val o = for {
      is <- treeFile.inputStream
    } yield {
      val rdr = new BufferedReader(new InputStreamReader(CompressionSupport.wrapStream(is, treeFile), Charsets.utf8))
      val iter = new DelimetedIterator(rdr, "EOS")


      val res = new ArrayBuffer[OldAngUglyKnpTable]()

      while (iter.hasNext) {
        res += p.parse(iter)
        iter.next() //drop EOS
      }

      res
    }
    o.obj
  }

  def maskMorphs(s: TableApi, vec: DictionaryVectorizer): Seq[MarkedMorpheme] = {
    val iter = s.lexemeIter.map { l =>
      MarkedMorpheme(l, vec.vecOne(l))
    }
    iter.toVector
  }

  def maskQuery(s: TableApi, q: String, f: LexemeApi => String) = {
    s.lexemeIter.zipWithIndex.collectFirst {
      case (lex, idx) if f(lex) == q => idx
    }.toList
  }

  def computeScores(data: DenseMatrix[Float], emb: DenseMatrix[Float], result: DenseVector[Float], offset: Int): Unit = {
    val dataCopy = new DenseMatrix[Float](data.rows, data.cols)
    VectorOps.normalize2d(data.data, data.rows, data.cols, dataCopy.data)
    val embCopy = emb.copy
    VectorOps.normalize(embCopy.data, embCopy.offset, embCopy.rows)

    val scores: DenseMatrix[Float] = embCopy.t * dataCopy

    var i = 0
    val len = scores.cols
    while (i < len) {
      result.data(offset + i) = scores.data(i)
      i += 1
    }
  }

  def main(args: Array[String]): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global

    val dic = args(0).p
    val treeFile = args(1).p
    val query = args(2)
    val serverAddr = args(3)

    val trees = readTrees(treeFile)
    val dictionary = DictionaryVectorizer.readPairsFromTsv(dic, 200000)
    val feature = VectKnpTree.feature("norm")
    val vect = DictionaryVectorizer.fromWordFreqPairs(dictionary, feature)
    val queryId = vect.idFor(query)

    println(s"query id=$queryId")

    val sentIds = trees.map(t => MaskedSentence(maskMorphs(t, vect), maskQuery(t, query, feature)))

    val batchSize = 32

    val rest = DictionaryVectorizer.restorator(dictionary.map(_._1))


    val sb = ManagedChannelBuilder.forTarget(serverAddr)
    sb.directExecutor()
    sb.usePlaintext(true)
    val chan = sb.build()
    try {
      val svc = new PredictionServiceStub(chan)
      val vec = new SentenceSummarizerClient(svc, batchSize, DictionaryVectorizer.question)
      import scala.concurrent.duration._

      val sentdata = sentIds.take(batchSize)
      val allsents = sentIds.drop(batchSize).grouped(batchSize)

      val firstFuture = vec.summaryAndScores(sentdata, queryId.map(_.toLong).toSeq)
      val restFutures = allsents.map(s => vec.summaryAndScores(s, Nil)).toList

      val res = Await.result(Future.sequence(firstFuture :: restFutures), 1.minute)
      val head = res.head

      val allNearest = res.flatMap(_.nearest)

      val embs = head.embeddings

      val score = new DenseVector[Float](sentIds.length)

      res.zipWithIndex.foreach { case (sps, i) =>
          computeScores(sps.representations, embs, score, i*batchSize)
      }

      val indices = Array.range(0, sentIds.length)
      val sortedIdx = indices.sortBy(i => -score(i))

      val objs = sentIds.zip(allNearest)


      var i = 0

      while (i < sentIds.length) {
        val realIdx = sortedIdx(i)
        val (sent, near) = objs(realIdx)
        val sentScore = score(realIdx)
        val toklen = sent.sentence.map(_.ids.length).sum
        println(f"============================= $sentScore%.3f [$toklen]")
        println(sent.sentence.map(_.data.surface).mkString)
        println(sent.sentence.map(m => rest.restore(m.ids)).mkString(" "))
        for (n <- near) {
          val data = rest.restoreOne(n.id)
          print(f"$data~${n.score}%.2f ")
        }
        println()

        i += 1
      }

    } finally {
      chan.shutdownNow()
    }
  }
}
