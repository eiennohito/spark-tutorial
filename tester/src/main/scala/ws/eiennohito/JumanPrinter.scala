package ws.eiennohito

import ws.kotonoha.akane.analyzers.juman.{JumanConfig, JumanSentence, JumanSequence, JumanSubprocess}
import ws.kotonoha.akane.parser.JumanPosSet

/**
  * @author eiennohito
  * @since 2017/05/11
  */
object JumanPrinter {

  val jp = JumanPosSet.default

  def formatEntry(x: JumanSentence): Unit = {
    val data = x.morphemes.map { l =>
      val pi = l.posInfo
      val pos = jp.pos(pi.pos)
      val sub = pos.subtypes(pi.subpos)
      val ctype = jp.conjugatons(pi.category)
      val cform = ctype.conjugations(pi.conjugation)
      s"${l.surface}_${l.reading}_${l.baseform}_${pos.name}_${sub.name}_${ctype.name}_${cform.name}"
    }

    println(data.mkString(" "))
  }

  def main(args: Array[String]): Unit = {
    val juman = JumanSubprocess.create(JumanConfig())

    while (true) {
      val line = Console.in.readLine()

      if (line.length > 0) {
        juman.analyzeSync(line) match {
          case scala.util.Success(x) =>
            formatEntry(x)
          case _ => return
        }
      } else return
    }
  }
}
