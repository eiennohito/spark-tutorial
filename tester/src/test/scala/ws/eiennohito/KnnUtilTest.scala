package ws.eiennohito

import breeze.linalg.{DenseMatrix, DenseVector}
import org.scalatest.{FreeSpec, Matchers}

/**
  * @author eiennohito
  * @since 2017/02/01
  */
class KnnUtilTest extends FreeSpec with Matchers {
  "KnnUtil" - {
    "normOf works" in {
      val mat1 = DenseMatrix.apply(
        1.0f, -2.0f, 3.0f,
        4.0f, 5.0f, 6.0f
      ).reshape(3, 2)

      val normed = KnnUtil.normOf(mat1)

      breeze.linalg.norm(normed(::, 0)) shouldBe 1.0 +- 0.01
      breeze.linalg.norm(normed(::, 1)) shouldBe 1.0 +- 0.01
    }

    "topN works" in {
      val mat1 = DenseMatrix.apply(
        1.0f, -2.0f, 3.0f,
        4.0f, 5.0f, 6.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, 1.0f, 1.0f
      ).reshape(3, 4)

      val query = DenseVector[Float](1.0f, 1.0f, 1.0f, 1.0f)

      val (idxes, vals) = KnnUtil.topN(mat1, query, 2)

      idxes shouldBe Seq(2, 0)
    }
  }
}
