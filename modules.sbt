
lazy val scalaPbVersion = "0.7.0"

def pbScala(grpc: Boolean = false): Seq[Setting[_]] = {
  Def.settings(
    PB.targets in Compile := Seq(
      scalapb.gen(grpc = grpc) -> (sourceManaged in Compile).value
    ),
    PB.protocVersion := "-v351",
    libraryDependencies ++= Seq(
      "com.thesamet.scalapb" %% "scalapb-runtime" % scalaPbVersion % "protobuf",
      "com.google.protobuf" % "protobuf-java" % "3.1.0"
    )
  )
}

def protoIncludes(files: Project*) = {
  val paths = files.map(f => f.base / "src" / "main" / "protobuf")
  Seq(PB.includePaths in Compile ++= paths)
}

lazy val commonSettings = Def.settings(
  scalaVersion := "2.11.8",
  resolvers += "kyouni" at "http://lotus.kuee.kyoto-u.ac.jp/nexus/content/groups/public/",
  libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % Test
)

lazy val akaneKnp = Def.settings(
  libraryDependencies += "ws.kotonoha" %% "akane-knp" % "0.2-SNAPSHOT"
)

lazy val sparkCore = Def.settings(
  libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.1" % Provided
)

lazy val `spark-usage` = (project in file("."))
    .dependsOn(`tensorflow-spark`, testerLib)
    .settings(
      commonSettings,
      akaneKnp,
      sparkCore
    )
  .settings(
    libraryDependencies += "org.rogach" %% "scallop" % "3.1.5"
  )

lazy val `tensorflow-core` = (project in file("scalapb-tensorflow/core"))
  .settings(pbScala(), commonSettings)

lazy val `tensorflow-example` = (project in file("scalapb-tensorflow/example"))
  .settings(pbScala(), commonSettings)

lazy val `tensorflow-spark` = (project in file("scalapb-tensorflow/spark"))
  .settings(pbScala(), commonSettings, sparkCore)
  .dependsOn(`tensorflow-example`, `tensorflow-core`)

lazy val `tensorflow-serving` = (project in file("scalapb-tensorflow/serving"))
  .settings(
    pbScala(grpc = true), protoIncludes(`tensorflow-core`),
    commonSettings,
    libraryDependencies ++= Seq(
      "io.grpc" % "grpc-netty" % "1.4.0",
      "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalaPbVersion
    )
  ).dependsOn(`tensorflow-core`)

lazy val testerLib = (project in file("tester-lib"))
  .settings(commonSettings, akaneKnp)
  .dependsOn(`tensorflow-example`)

lazy val modelTester = (project in file("tester"))
  .settings(commonSettings, akaneKnp)
  .dependsOn(`tensorflow-serving`, testerLib)
  .settings(
    libraryDependencies ++= Seq(
      "org.iq80.leveldb" % "leveldb" % "0.9",
      "org.scalanlp" %% "breeze" % "0.12"
    )
   )

lazy val modelWebTester = (project in file("tester-web"))
  .settings(commonSettings)
  .dependsOn(modelTester)
  .enablePlugins(PlayScala)
  .settings(
    libraryDependencies ++= Seq(
      "ws.kotonoha" %% "akane-knp-akka" % "0.2-SNAPSHOT",
      "ws.kotonoha" %% "akane-legacy" % "0.2-SNAPSHOT",
      "com.typesafe.akka" %% "akka-slf4j" % "2.4.11",
      "ch.qos.logback" % "logback-classic" % "1.1.9",
      "ch.qos.logback" % "logback-core" % "1.1.9"
    )
  )
